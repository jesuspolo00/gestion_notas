<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
| 
| rango de permisos
| 1 == padre
| 2 == docente
| 3 == contador
| 4 == secretaria
| 5 == admin
*/
// nivel==cursos

Route::get('/login', function () {
    return view('login.login');
});


/*                USUARIO AUTENTIFICADO               */
Route::middleware(['auth'])->group(function () {
    //INICIO
    Route::get('/',[
        'as'=>'inicio',
        'uses' => 'HomeController@index'
    ]);
    Route::middleware(['permisos:4'])->group(function () {
        Route::post('/finperiodo',[
            'as'=>'finalizar.periodo',
            'uses' => 'HomeController@finalizar_periodo'
        ]);
        Route::post('/finaño',[
            'as'=>'finalizar.año',
            'uses' => 'HomeController@finalizar_año'
        ]);
        
    });
    Route::middleware(['permisos:4.2.1'])->group(function () {
        /*         estudiantes               */
        Route::get('/estudiantes/buscar',[
            'as'=>'estudiantes.buscar',
            'uses' => 'EstudiantesController@index'
        ]);
        Route::post('/estudiantes/listar',[
            'as'=>'estudiantes.listar',
            'uses' => 'EstudiantesController@listar'
        ]);
        Route::middleware(['permisos:4'])->group(function () {
            Route::get('/estudiantes/registrar', [
                'as'=>'estudiantes.registrar',
                'uses' => 'EstudiantesController@create'
            ]);
                Route::post('/estudiantes/store', [
                'as'=>'estudiantes.store',
                'uses' => 'EstudiantesController@store'
                ]);
                //en caso de que el registro del estudiante no se complete ▼
                Route::get('/estudiantes/incompletos', [
                    'as'=>'estudiantes.incompletos',
                    'uses' => 'EstudiantesController@incompletos'
                ]);
                    Route::get('/estudiantes/incompletos/{id}/verificar', [
                        'as'=>'estudiantes.incompletos.verificar',
                        'uses' => 'EstudiantesController@incompletos_verificar'
                    ]);
            Route::get('/estudiantes/{id}/editar', [
                'as'=>'estudiantes.editar',
                'uses' => 'EstudiantesController@edit'
            ]);
            Route::put('/estudiantes/{id}', [
                'as'=>'estudiantes.actualizar',
                'uses' => 'EstudiantesController@update'
            ]);    
            Route::get('/estudiantes/{id}/eliminar', [
                'as'=>'estudiantes.eliminar',
                'uses' => 'EstudiantesController@destroy'
            ]); 
        });
        Route::get('/estudiantes/{id}/perfil', [
            'as'=>'estudiantes.perfil',
            'uses' => 'EstudiantesController@perfil'
        ]);
            Route::get('/estudiantes/{id}/acudiente', [
                'as'=>'estudiantes.acudiente',
                'uses' => 'AcudienteController@editar'
            ]);
        Route::middleware(['permisos:1'])->group(function () {
            Route::get('/estudiantes/{id}/boletin', [
                'as'=>'estudiantes.boletin',
                'uses' => 'EstudiantesController@ver_boletin'
            ]);
        });
        Route::middleware(['permisos:4.2'])->group(function () {
            Route::post('/estudiantes/{curso}/calificar/{estudiante}', [
                'as'=>'estudiantes.calificar',
                'uses' => 'EstudiantesController@calificar'
            ]);
            Route::post('/estudiantes/{id}/ausencia', [
                'as'=>'estudiantes.ausencia',
                'uses' => 'EstudiantesController@ausencia'
            ]);
        });
    });
    Route::middleware(['permisos:4'])->group(function () {
    //boletin
    Route::get('/boletin/descargar/{id}',[
        'as'=>'boletin.descargar',
        'uses' => 'boletinController@descargar_boletin'
    ]);
    Route::post('/boletin/generar/{id}',[
        'as'=>'boletin.generar',
        'uses' => 'boletinController@guardar_boletin'
    ]);        
    Route::get('/boletin/{id}',[
        'as'=>'boletin',
        'uses' => 'boletinController@index'
    ]);
    /* reportes */
    Route::get('/reportes/buscar',[
        'as'=>'reportes.buscar',
        'uses' => 'reporteController@individual'
    ]);
    Route::get('/reportes/descargar',[
        'as'=>'reportes.descargar',
        'uses' => 'reporteController@descargar_boletin'
    ]);

    });
    //////////////////////////////
    /* matriculas */
    Route::middleware(['permisos:4'])->group(function () {
        Route::get('/matricula/{id}/registrar',[
            'as'=>'matricula.registrar',
            'uses' => 'MatriculaController@create'
        ]);
            Route::post('/matricula/store',[
            'as'=>'matricula.store',
            'uses' => 'MatriculaController@store'
            ]);
            Route::post('/matricula/precio',[
            'as'=>'matricula.precio',
            'uses' => 'MatriculaController@precio'
            ]);
        Route::get('/configurar/matriculas',[
            'as'=>'configurar.matricula',
            'uses' => 'MatriculaController@matricula'
        ]);
        Route::post('/configurar/matriculas/habilitar/{old}',[
            'as'=>'matricula.habilitar',
            'uses' => 'MatriculaController@matricula_habilitar'
        ]);
    });
    ///////////////////////////////
    /*    acudiente      */
    Route::middleware(['permisos:4'])->group(function () {
        Route::get('/acudiente/{id}/registrar', [
            'as'=>'acudiente.registrar',
            'uses' => 'AcudienteController@create'
        ]);
            Route::post('/acudiente/seleccionar/{id}/{acudiente}', [
            'as'=>'acudiente.seleccionar',
            'uses' => 'AcudienteController@seleccionar'
            ]);
            Route::post('/acudiente/seleccion/{id}/{acudiente}', [
            'as'=>'acudiente.seleccion',
            'uses' => 'AcudienteController@acudiente_seleccion'
            ]);
            Route::post('/acudiente/store', [
            'as'=>'acudiente.store',
            'uses' => 'AcudienteController@store'
            ]);
            Route::post('/acudiente/{id}/update/{acudiente}', [
            'as'=>'acudiente.update',
            'uses' => 'AcudienteController@update'
            ]);
    });
    /* nivel === cursos */
    /////////////////////////////////
    /*        nivel              */
    Route::middleware(['permisos:4.2'])->group(function () {
        Route::get('/nivel/buscar',[
            'as'=>'cursos.buscar',
            'uses' => 'cursoController@index'
        ]);
        Route::get('/nivel/horario',[
            'as'=>'cursos.horario',
            'uses' => 'horarioController@cursos'
        ]);
        Route::get('/nivel/{id}/perfil',[
            'as'=>'cursos.perfil',
            'uses' => 'cursoController@perfil'
        ]);
            
        Route::post('/nivel/{id}/cerrar',[
            'as'=>'nivel.cerrar',
            'uses' => 'cursoController@cerrar_periodo'
        ]);
        Route::get('/nivel/categoria',[
            'as'=>'nivel.categoria',
            'uses' => 'cursoController@nivel_categoria'
        ]);
            Route::post('/nivelcat/agregar',[
                'as'=>'nivelCat.agregar',
                'uses' => 'cursoController@nivelCat_agregar'
            ]);
            Route::post('/nivelcat/{id}/editar',[
                'as'=>'nivelCat.editar',
                'uses' => 'cursoController@nivelCat_editar'
            ]);
            Route::get('/nivelcat/{id}/eliminar',[
                'as'=>'nivelCat.eliminar',
                'uses' => 'cursoController@nivelCat_eliminar'
            ]);
    });
   
    //////////////////////////////////
    /*          dimenciones         */
    Route::middleware(['permisos:4'])->group(function () {
    Route::get('/dimenciones',
    [
    'as'=>'dimencion',
    'uses' => 'dimencionController@dimencion'
    ]);
        Route::post('/dimenciones/agregar',
        [
        'as'=>'dimencion.agregar',
        'uses' => 'dimencionController@agregar_dimencion'
        ]);
        Route::get('/dimenciones/{id}/eliminar',
        [
        'as'=>'dimencion.eliminar',
        'uses' => 'dimencionController@eliminar_dimencion'
        ]);
    Route::get('/subdimencion',
    [
    'as'=>'subdimencion',
    'uses' => 'dimencionController@subdimencion'
    ]);
        Route::post('/subdimenciones/{id}/agregar',
        [
        'as'=>'subdimencion.agregar',
        'uses' => 'dimencionController@agregar_subdimencion'
        ]);
        Route::get('/subdimenciones/{id}/eliminar',
        [
        'as'=>'subdimencion.eliminar',
        'uses' => 'dimencionController@eliminar_subdimencion'
        ]);
    Route::get('/competencias',
    [
    'as'=>'competencias',
    'uses' => 'dimencionController@competencias'
    ]);
        Route::post('/competencias/{id}/agregar',
        [
        'as'=>'competencias.agregar',
        'uses' => 'dimencionController@agregar_competencias'
        ]);
        Route::get('/competencias/{id}/eliminar',
        [
        'as'=>'competencias.eliminar',
        'uses' => 'dimencionController@eliminar_competencias'
        ]);
    Route::get('/subcompetencias',
    [
    'as'=>'subcompetencias',
    'uses' => 'dimencionController@subcompetencias'
    ]);
        Route::post('/subcompetencias/{id}/agregar',
        [
        'as'=>'subcompetencias.agregar',
        'uses' => 'dimencionController@agregar_subcompetencias'
        ]);
        Route::get('/subcompetencias/{id}/eliminar',
        [
        'as'=>'subcompetencias.eliminar',
        'uses' => 'dimencionController@eliminar_subcompetencias'
        ]);
    Route::get('/materias', [
        'as'=>'materias',
        'uses' => 'dimencionController@materias'
    ]);
        Route::post('/materias/{id}/agregar',
        [
        'as'=>'materias.agregar',
        'uses' => 'dimencionController@agregar_materias'
        ]);
        Route::get('/materias/{id}/eliminar',
        [
        'as'=>'materias.eliminar',
        'uses' => 'dimencionController@eliminar_materias'
        ]);
    /* ruta ajax en Dimenciones*/
    Route::get('/subdimencion/listar/{id}',
    [
    'as'=>'subdimencion.listar',
    'uses' => 'dimencionController@subdimencion_listar'
    ]);
    Route::get('/competencias/listar/{id}',
    [
    'as'=>'compentencias.listar',
    'uses' => 'dimencionController@competencias_listar'
    ]);
    Route::get('/subcompetencias/listar/{id}',
    [
    'as'=>'subcompentencias.listar',
    'uses' => 'dimencionController@subcompetencias_listar'
    ]);
    Route::get('/materias/listar/{id}',
    [
    'as'=>'materias.listar',
    'uses' => 'dimencionController@materias_listar'
    ]);
    });
    ///////////////////////////////
    /*          profesor         */
    Route::middleware(['permisos:4'])->group(function () {
        Route::get('/profesores/buscar',[
            'as'=>'profesores.buscar',
            'uses' => 'profesorController@index'
        ]);
        Route::get('/profesores/registrar',[
            'as'=>'profesores.registrar',
            'uses' => 'profesorController@create'
        ]);
            Route::post('/profesores/store', [
            'as'=>'profesores.store',
            'uses' => 'profesorController@store'
            ]);
        Route::get('/profesores/{id}/editar', [
            'as'=>'profesores.editar',
            'uses' => 'profesorController@edit'
        ]);
        Route::put('/profesores/{id}', [
            'as'=>'profesores.actualizar',
            'uses' => 'profesorController@update'
        ]);    
        Route::get('/profesores/{id}/eliminar', [
            'as'=>'profesores.eliminar',
            'uses' => 'profesorController@destroy'
        ]); 
        Route::get('/profesores/{id}/perfil', [
            'as'=>'profesores.perfil',
            'uses' => 'profesorController@perfil'
        ]);
        // AJAX profesor 
        Route::get('/profesores/ajax/{nombre}', [
            'as'=>'profesores.ajax',
            'uses' => 'profesorController@ajax'
        ]);
        Route::get('/profesores/{profesor}/ajaxeditar/{curso}', [
            'as'=>'profesores.ajaxEditar',
            'uses' => 'profesorController@ajaxEditar'
        ]);
    });
    //////////////////////////////////////
    /*          Pagos        */
    Route::middleware(['permisos:3'])->group(function () {
        Route::get('/pagos',[
            'as'=>'pagos.index',
            'uses' => 'pagosController@index'
        ]);
            Route::get('/pagos/{id}/guardar',[
                'as'=>'pagos.guardar',
                'uses' => 'pagosController@guardar'
            ]);
            Route::post('/pagos/{id}/store',[
                'as'=>'pagos.store',
                'uses' => 'pagosController@store'
            ]);
    });
    //////////////////////////////////////
    /*          usuario               */
    Route::get('/perfil',[
        'as'=>'usuario.perfil',
        'uses' => 'usuarioController@perfil'
    ]);
        Route::post('/perfil/datos',[
        'as'=>'usuario.datos_actualizar',
        'uses' => 'usuarioController@datos_actualizar'
        ]);
        Route::post('/perfil/actualizar',[
        'as'=>'usuario.perfil_actualizar',
        'uses' => 'usuarioController@perfil_actualizar'
        ]);
    Route::get('perfil/color/{clase}',[
        'as'=>'perfil.color',
        'uses' => 'usuarioController@color'
    ]);
    //////////////////////////////////////
    /*          configuracion         */
    Route::middleware(['permisos:4'])->group(function () {
        Route::get('/configuracion/{id}',[
            'as'=>'configuracion.index',
            'uses' => 'ConfiguracionController@index'
        ]);
            Route::post('/configuracion/colegio', [
                'as'=>'configuracion.colegio',
                'uses' => 'ConfiguracionController@colegio'
            ]);
            Route::post('/configuracion/confboletin', [
                'as'=>'configuracion.boletin',
                'uses' => 'ConfiguracionController@conf_boletin'
            ]);
            Route::post('/configuracion/habilitado', [
                'as'=>'configuracion.habilitar',
                'uses' => 'ConfiguracionController@habilitar_cursos'
            ]);
            /*       configuracion.Nivel            */
                Route::post('/configuracion/nivel', [
                    'as'=>'configuracion.cursos',
                    'uses' => 'ConfiguracionController@cursos'
                ]);
                Route::get('/nivel/{id}/eliminar',
                [
                'as'=>'cursos.eliminar',
                'uses' => 'ConfiguracionController@eliminar_cursos'
                ]);
                Route::post('/nivel/{id}/editar',
                [
                'as'=>'cursos.editar',
                'uses' => 'ConfiguracionController@editar_cursos'
                ]);
                Route::get('/detalle/{id}/nivel',
                [
                'as'=>'cursos.detalle',
                'uses' => 'ConfiguracionController@detalle_curso'
                ]);
                    Route::post('/detalle/{id}/registrar',[
                    'as'=>'detalle.registrar',
                    'uses' => 'ConfiguracionController@detalle_registrar'
                    ]);
                    Route::post('/detalle/asignar/{id}',[
                    'as'=>'detalle.asignar',
                    'uses' => 'ConfiguracionController@detalle_asignar'
                    ]);
                    Route::get('/detalle/eliminar/{id}/{curso}',[
                    'as'=>'detalle.eliminar',
                    'uses' => 'ConfiguracionController@detalle_eliminar'
                    ]);
            /*     @end configuracion.cursos       */
        Route::middleware(['permisos:'])->group(function () {
            /*       configuracion.usuarios            */
            Route::get('/usuario',[
                'as'=>'usuario',
                'uses' => 'usuarioController@index'
            ]);
                Route::get('/usuario/{id}/editar',[
                    'as'=>'usuario.editar',
                    'uses' => 'usuarioController@edit'
                ]);
                Route::post('/nivel/{id}/confirmar',[
                    'as'=>'usuario.confirmar',
                    'uses' => 'usuarioController@confirmar'
                ]);
                Route::post('/nivel/{id}/update',[
                    'as'=>'usuario.update',
                    'uses' => 'usuarioController@update'
                ]);
                Route::get('/usuario/{id}/eliminar',[
                    'as'=>'usuario.eliminar',
                    'uses' => 'usuarioController@delete'
                ]);
            /*        configuracion.historial        */
            Route::get('/historial',[
                'as'=>'configurar.historial',
                'uses' => 'historialController@index'
            ]);
            Route::get('/historial/boletines',[
                'as'=>'configurar.historial.boletin',
                'uses' => 'historialController@historial_boletin'
            ]);
        });
        ///////////////////////////////////
        /****** horario ******/
        Route::get('/horario', [
                'as'=>'horario',
                'uses' => 'horarioController@index'
        ]);
        Route::post('/horario/registrar', [
                'as'=>'horario.registrar',
                'uses' => 'horarioController@horario'
        ]);
        Route::post('/horario/store', [
                'as'=>'horario.store',
                'uses' => 'horarioController@store'
        ]);
    });
});
Auth::routes();


