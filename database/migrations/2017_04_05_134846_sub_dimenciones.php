<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SubDimenciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_dimenciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->integer('dimencion_id')->unsigned();           
            $table->timestamps();

            $table->foreign('dimencion_id')->references('id')->on('dimenciones')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_dimenciones');
    }
}
