<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BoletinSubDimenciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boletin_sub_dimenciones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ids');
            $table->string('nombre');
            $table->integer('boletin_dimencion_id')->unsigned();           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boletin_sub_dimenciones');
    }
}
