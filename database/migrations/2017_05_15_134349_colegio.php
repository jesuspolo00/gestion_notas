<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Colegio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('colegio', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre')->nullable(); 
            $table->string('sigla')->nullable();
            $table->string('logo')->nullable();
            $table->integer('año_escolar')->nullable();
            $table->integer('periodo_escolar')->nullable();  
            $table->string('boletin_codig')->nullable();
            $table->string('boletin_fecha')->nullable();
            $table->string('boletin_añolectivo')->nullable();    
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
