<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Dimenciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dimenciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('nivel');
            $table->timestamps();
        });
        //relacion de mucho a muchos tabla pibote
        Schema::create('dimencion_profesor', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dimencion_id')->unsigned();
            $table->integer('profesor_id')->unsigned();           
            $table->timestamps();

            $table->foreign('dimencion_id')->references('id')->on('dimenciones')->onDelete('cascade');
            $table->foreign('profesor_id')->references('id')->on('profesores')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dimenciones');
    }
}
