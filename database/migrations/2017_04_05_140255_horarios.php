<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Horarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('horario', function (Blueprint $table) {
            $table->increments('id');
            $table->string('materia');
            $table->integer('periodo');
            $table->integer('año');
            $table->integer('dia_id')->unsigned();
            $table->integer('hora_id')->unsigned();
            $table->integer('curso_id')->unsigned();          
            $table->timestamps();

            $table->foreign('dia_id')->references('id')->on('dias')->onDelete('cascade');
            $table->foreign('hora_id')->references('id')->on('horas')->onDelete('cascade');
            $table->foreign('curso_id')->references('id')->on('cursos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('horarios');
    }
}
