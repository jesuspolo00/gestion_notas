<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
class Horas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('horas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('hora');
            $table->integer('disponible')->default(0);
            $table->timestamps();
        });
        DB::table('horas')->insert([
            ['id' => null, 'hora' => '1:00'],
            ['id' => null, 'hora' => '2:00'],
            ['id' => null, 'hora' => '3:00'],
            ['id' => null, 'hora' => '4:00'],
            ['id' => null, 'hora' => '5:00'],
            ['id' => null, 'hora' => '6:00'],
            ['id' => null, 'hora' => '7:00'],
            ['id' => null, 'hora' => '8:00'],
            ['id' => null, 'hora' => '9:00'],
            ['id' => null, 'hora' => '10:00'],
            ['id' => null, 'hora' => '11:00'],
            ['id' => null, 'hora' => '12:00'],
            ['id' => null, 'hora' => '13:00'],
            ['id' => null, 'hora' => '14:00'],
            ['id' => null, 'hora' => '15:00'],
            ['id' => null, 'hora' => '16:00'],
            ['id' => null, 'hora' => '17:00'],
            ['id' => null, 'hora' => '18:00'],
            ['id' => null, 'hora' => '19:00'],
            ['id' => null, 'hora' => '20:00'],
            ['id' => null, 'hora' => '21:00'],
            ['id' => null, 'hora' => '22:00'],
            ['id' => null, 'hora' => '23:00'],
            ['id' => null, 'hora' => '24:00']
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('horas');
    }
}
