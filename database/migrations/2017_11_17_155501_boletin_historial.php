<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BoletinHistorial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boletin_historial', function (Blueprint $table) {
            $table->increments('id');
            $table->string('usuario');
            $table->string('movimiento');
            $table->string('curso');
            $table->string('periodo');
            $table->string('año');
            $table->string('estudiante');       
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boletin_historial');
    }
}
