<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Cursos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('cursos', function (Blueprint $table) {
            $table->increments('id');             
            $table->string('nombre'); 
            $table->integer('activo')->default(0);    
            $table->integer('id_categoria')->unsigned();
            $table->foreign('id_categoria')->references('id')->on('cursos_categoria');                           
            $table->timestamps();
            
        });
        Schema::create('detalle_curso', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('curso_id')->unsigned();
            $table->string('director_grupo')->nullable();
            $table->string('tipo_calificacion')->nullable();
            $table->integer('numero_estudiantes')->nullable();
            $table->string('nivel_formacion')->nullable();  
            $table->foreign('curso_id')->references('id')->on('cursos')->onDelete('cascade');                 
            $table->timestamps();           
        });
        Schema::create('curso_profesor', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('profesor_id')->unsigned();  
            $table->integer('curso_id')->unsigned();                    
            $table->timestamps(); 
            $table->foreign('curso_id')->references('id')->on('cursos')->onDelete('cascade');  
            $table->foreign('profesor_id')->references('id')->on('profesores')->onDelete('cascade');          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cursos');
    }
}
