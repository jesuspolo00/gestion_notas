<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Acudientes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acudientes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tipo_documento');
            $table->bigint('documento');
            $table->string('nombre');
            $table->string('apellido');
            $table->string('correo');
            $table->string('direccion');
            $table->integer('telefono');
            $table->string('genero');
            $table->timestamps();
        });
         //relacion de mucho a muchos tabla pibote
        Schema::create('acudiente_estudiante', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('estudiante_id')->unsigned();
            $table->integer('acudiente_id')->unsigned();
           
            $table->timestamps();

            $table->foreign('estudiante_id')->references('id')->on('estudiantes')->onDelete('cascade');
            $table->foreign('acudiente_id')->references('id')->on('acudientes')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acudientes');
        Schema::dropIfExists('estudiantes_acudientes');
    }
}
