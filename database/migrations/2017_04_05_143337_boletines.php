<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Boletines extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boletines', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo');
            $table->string('año_lectivo');
            $table->string('nombre');
            $table->string('nivel');
            $table->string('periodo');
            $table->string('fecha');
            $table->string('año_escolar');
            $table->string('ausencia');
            $table->string('observacion');
            $table->integer('estudiante_id')->unsigned();           
            $table->timestamps();

            $table->foreign('estudiante_id')->references('id')->on('estudiantes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boletines');
    }
}
