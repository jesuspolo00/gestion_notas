<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Nota extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nota', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('nota');
            $table->integer('periodo');
            $table->integer('año');
            $table->integer('curso_id')->unsigned();
            $table->integer('materia_id')->unsigned();
            $table->integer('estudiante_id')->unsigned();        
            $table->timestamps();

            $table->foreign('curso_id')->references('id')->on('materias')->onDelete('cascade');
            $table->foreign('materia_id')->references('id')->on('materias')->onDelete('cascade');
            $table->foreign('estudiante_id')->references('id')->on('estudiantes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nota');
    }
}
