<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BoletinNotas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boletin_notas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ids');
            $table->string('nota');
            $table->integer('periodo');
            $table->integer('año');
            $table->integer('curso_id');
            $table->integer('materia_id');
            $table->integer('estudiante_id');        
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boletin_notas');
    }
}
