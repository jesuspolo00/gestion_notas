<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detalle_curso extends Model
{
    protected $table = 'detalle_curso';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'curso_id','director_grupo', 'tipo_calificacion', 'numero_estudiantes', 'nivel_formacion', 
    ];
    public function curso(){
    	return $this->belongsTo('App\Curso');
    }
    
}
