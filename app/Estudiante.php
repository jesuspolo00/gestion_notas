<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Estudiante extends Model
{
	
    protected $table = 'estudiantes';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tipo_documento', 'documento', 'nombre','apellido', 'genero','tipo_sangre','foto',
    ];
    public function diploma(){
        return $this->hasMany('App\Diploma');
    }
    public function boletin(){
        return $this->hasMany('App\boletin');
    }
    public function pagos(){
        return $this->hasMany('App\pagos');
    }
    public function matricula(){
        return $this->hasMany('App\Matricula');
    }
    public function acudiente(){
        return $this->belongsToMany('App\Acudiente');
    }
    
    public static function scopeSearch($query,$buscar){
        return $query->join('matriculas', 'estudiantes.id', '=', 'matriculas.estudiante_id')->where('matriculas.estado','=','formacion')->Where(function ($query) use($buscar) {
                $query->Where('estudiantes.nombre','like',"%".$buscar."%")->orWhere('estudiantes.apellido','like',"%".$buscar."%")->orWhere('estudiantes.documento','like',"%".$buscar."%");
            });
    }
    public static function scopeSearchProfesor($query,$buscar){
        return $query->Where('estudiantes.nombre','like',"%".$buscar."%")->orWhere('estudiantes.apellido','like',"%".$buscar."%")->orWhere('estudiantes.documento','like',"%".$buscar."%");
    }
    public static function obtener_cursos($id){

        return Estudiante::join('matriculas', 'estudiantes.id', '=', 'matriculas.estudiante_id')
                ->join('cursos', 'matriculas.curso_id', '=', 'cursos.id')
                ->select('cursos.nombre as curso','cursos.id','matriculas.ausencia','cursos.activo','matriculas.observacion','cursos.id_categoria')
                ->where('estudiantes.id','=',$id)->where('matriculas.estado','=','formacion');
    }
    public static function estudiantes_cursos($id){

        return Estudiante::join('matriculas', 'estudiantes.id', '=', 'matriculas.estudiante_id')
                ->join('cursos', 'matriculas.curso_id', '=', 'cursos.id')
                ->select('cursos.nombre as curso','cursos.id','matriculas.ausencia','cursos.activo')
                ->where('cursos.id','=',$id)->where('matriculas.estado','=','formacion');
    }
}
