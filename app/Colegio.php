<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Colegio extends Model
{
     protected $table = 'colegio';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre','sigla','logo', 'año_escolar', 'periodo_escolar','boletin_codigo','boletin_fecha','boletin_añolectivo', 
    ];
    
}
