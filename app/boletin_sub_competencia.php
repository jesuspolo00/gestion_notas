<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class boletin_sub_competencia extends Model
{
    protected $table = 'boletin_sub_competencias';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ids','nombre','boletin_competencia_id',
    ];
}
