<?php

namespace App\Http\Middleware;
use Closure;
class permisos
{
     
    protected $auth;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$role)
    {
        if($request->user()->rol == 5){
            return $next($request);
        }else{
        $role = explode(".",$role);
            foreach ($role as $rol) {
                if($request->user()->rol == $rol){
                    return $next($request);
                }
            }    
            abort(401);        
        }
    }
}
