<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Restudiante;
use App\Http\Requests\UpdateEstudiante;
use App\Estudiante;
use App\Materia;
use App\Dimencion;
use App\Nota;
use App\Colegio;
use App\Matricula;
use App\Profesor;
use App\Historial;
use App\Acudiente;
use App\pagos;
use App\boletin;
use App\boletin_dimencion;
use App\Curso;
use Illuminate\Support\Facades\DB;
use App\Dia;
use App\Hora;
use App\Horario;
class EstudiantesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $periodo=['I TRIMESTRE (Agosto-Octubre)','II TRIMESTRE (Noviembre a Febrero)','III TRIMESTRE (Marzo a Junio)'];

    public function index(Request $request)
    {
        if(Auth::User()->rol =="2"){
            $id_profesor=Auth::user()->profesor()->select('profesores.id')->get();
            $profesor = Profesor::find($id_profesor[0]->id);
            $curso = $profesor->curso()->get();
        

            $estudiante = Estudiante::estudiantes_cursos($curso[0]->id)->SearchProfesor($request->resultado)->select('estudiantes.*')->paginate(15);
        }elseif(Auth::User()->rol =="1"){
            $id_acudiente=Auth::user()->acudiente()->select('acudientes.id')->get();

            $acudiente = Acudiente::find($id_acudiente[0]->id);
           
            $estudiante= $acudiente->estudiante()->paginate(15);
        }else{
            //search es un scope que se encuentra en el modelo estudiante 
            $estudiante = Estudiante::Search($request->resultado)->select('estudiantes.*')->paginate(15);
            
        }  
        return view('estudiantes.BuscarEstudiante')->with(['estudiante'=>$estudiante]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('estudiantes.RegistrarEstudiante');
    }
    public function perfil($id){   
        
        $estudiante=Estudiante::find($id);
        $curso = $estudiante->obtener_cursos($id)->orderBy('id', 'desc')->get();
        if(Auth::User()->rol !="2"){
            $dimencion = Dimencion::where('nivel',$curso[0]->id_categoria)->get();
        }else{
            $dimencion = Auth::User()->dimenciones_profesores();
        }
        
        $curso->toArray();
        $colegio = Colegio::find(1);
        if($colegio){

            if(Auth::User()->rol =="1" or Auth::User()->rol =="5"){
                $validar=pagos::validar_pagos($id);
                $boletin=$estudiante->boletin()->where([['nivel','=',$curso[0]->curso],['año_escolar','=',$colegio->año_escolar]])->get();
                $dia= Dia::all();
                $hora = Hora::where('disponible',1)->get();
                $horario = Horario::obtener_horario($curso[0]->id)->get(); 
            }else{
                $validar=[];
                $boletin=collect([]);
                $dia=collect([]);
                $hora=collect([]);
                $horario=collect([]);
            }
            $pago = $estudiante->pagos()->where('año',$colegio->año_escolar)->get();
            $nota = Estudiante::join('nota','nota.estudiante_id','=','estudiantes.id')
                    ->select('nota.*')
                    ->where([['nota.estudiante_id','=',$id],['nota.curso_id','=',$curso[0]->id],['nota.año','=',$colegio->año_escolar],['nota.periodo','=',$colegio->periodo_escolar]])->get();
        }
        $materia = Materia::all();
        return view('estudiantes.perfilEstudiante')->with(['estudiante'=>$estudiante,'materia'=>$materia,'curso'=>$curso,'dimencion'=>$dimencion,'nota'=>$nota,'pago'=>$pago,'validar'=>$validar,'boletin'=>$boletin,'dia'=>$dia,'hora'=>$hora,'horario'=>$horario]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Restudiante $request)
    {
        $estudiante = new Estudiante();
        $estudiante->nombre = $request->nombre;
        $estudiante->apellido = $request->apellido;     
        $estudiante->tipo_documento = $request->tdocumento;       
        $estudiante->documento = $request->documento;
        $estudiante->genero = $request->genero;
        $estudiante->tipo_sangre = $request->tipo;
        $file = $request->file('foto');
        $name = 'estudiante_'.time().'.'.$file->getClientOriginalExtension();
        $path = 'images/estudiantes/';
        $estudiante->foto = $file->move($path,$name);
       if($estudiante->save()){
            Historial::guardar('Registro','Estudiante',$estudiante->nombre.' '.$estudiante->apellido,'');
            flash('Datos guardados exitosamente! Por favor registre al acudiente.', 'success');
        }else{
            flash('ah ocurrrido un error', 'danger');
        }
        return redirect()->route('acudiente.registrar',$estudiante->id);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $estudiante = Estudiante::find($id);
        return view('estudiantes.editarEstudiante')->with(['estudiante'=>$estudiante]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEstudiante $request, $id)
    {
        $estudiante = Estudiante::find($id);
        $estudiante->nombre = $request->nombre;
        $estudiante->apellido = $request->apellido;     
        $estudiante->tipo_documento = $request->tdocumento;       
        $estudiante->documento = $request->documento;
        $estudiante->genero = $request->genero;
        $estudiante->tipo_sangre = $request->tipo;
        $file = $request->file('foto');
        $name = 'estudiante_'.time().'.'.$file->getClientOriginalExtension();
        $path = 'images/estudiantes/';
        $estudiante->foto = $file->move($path,$name);
        if($estudiante->save()){        
            Historial::guardar('Editado','Estudiante',$estudiante->nombre.' '.$estudiante->apellido,'');
            flash('Datos guardados exitosamente!', 'success');

        }else{
            flash('ah ocurrrido un error', 'danger');
        }
        return redirect()->route('estudiantes.buscar');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $estudiante = Estudiante::find($id);
        $matricula = $estudiante->matricula()->where('matriculas.estado','formacion')->select('matriculas.id')->get();
        $matricula = Matricula::find($matricula[0]->id);
        $matricula->estado = 'retirado';
        if($matricula->save()){
            Historial::guardar('Eliminado','Estudiante',$estudiante->nombre.' '.$estudiante->apellido,'RETIRADO');
            flash('Se retiro al estudiante correctamente!', 'success');
        }else{
            flash('ah ocurrrido un error', 'danger');
        }
        return redirect()->route('estudiantes.buscar');
    }
    public function calificar(Request $request,$curso,$estudiante){
        
        $request->all();
        $colegio = Colegio::find(1);
        if($colegio){
            $dimencion = Dimencion::all();        
            foreach($dimencion as $dimenciones){
                $materia_boletin = Dimencion::materias_boletin($dimenciones->id)->get();
                $subcompetencia = Dimencion::subcompetencias_boletin($dimenciones->id)->get();
                $competencia = Dimencion::competencias_boletin($dimenciones->id)->get();
                $subdimencion = Dimencion::subdimenciones_boletin($dimenciones->id)->get();
                if($materia_boletin->isNotEmpty()){

                foreach($subdimencion as $subdimenciones){
                    foreach($competencia as $competencias){
                        if($competencias->sub_dimencion_id == $subdimenciones->id){
                            foreach($subcompetencia as $subcompetencias){
                                if($subcompetencias->competencia_id == $competencias->id){
                                    foreach($materia_boletin as $mate){
                                        if($mate->sub_competencia_id == $subcompetencias->id){
                                            if(isset($request[$mate->id])){
                                                $explode=explode('-',$request[$mate->id]);
                                            }else{
                                                $explode[1]="";
                                            }
                                            if($mate->id == $explode[1]){
                                                $verificar = Nota::where([['estudiante_id','=',$estudiante],['materia_id','=',$mate->id],['año','=',$colegio->año_escolar],['periodo','=',$colegio->periodo_escolar]])->get();
                                                if($verificar->isNotEmpty()){
                                                    $verificar->ToArray();
                                                    $nota = Nota::find($verificar[0]['id']);
                                                    $nota->nota = $explode[0];
                                                    $nota->save();
                                                }else{                                        
                                                    if($request[$mate->id] != ""){
                                                        $nota = new Nota();
                                                        $nota->nota = $explode[0];
                                                        $nota->periodo = $colegio->periodo_escolar;
                                                        $nota->año = $colegio->año_escolar;
                                                        $nota->estudiante_id = $estudiante;
                                                        $nota->curso_id = $curso;
                                                        $nota->materia_id = $mate->id;
                                                        $nota->save();
                                                    }                                       
                                                }
                                            }else{
                                                continue;
                                            }

                                        }else{
                                            continue;
                                        }
                                    }
                                }else{
                                    continue;
                                }
                            }
                        }else{
                            continue;               
                        }
                    }
                }
            }
            }
        }else{
            flash('No se pudo completar el procedimiento!. Asegurece que las configuraciones esten ingresadas correctamente', 'danger');
        }
        $e = Estudiante::find($estudiante);
        Historial::guardar('Registro','Calificado',$e->nombre.' '.$e->apellido,'');
        return redirect()->route('estudiantes.perfil',$estudiante);
    }
    public function ausencia(Request $request,$id){
        $estudiante= Estudiante::find($id);
        $curso= $estudiante->obtener_cursos($id)->orderBy('id', 'desc')->get();
        $ausencia= Matricula::where([['estudiante_id','=',$estudiante->id],['curso_id','=',$curso[0]->id]])->get();
        $matricula = Matricula::find($ausencia[0]->id);
        $matricula->ausencia=$request->ausencia;
        $matricula->observacion=$request->observacion;
        if($matricula->save()){
            Historial::guardar('Registro','Ausencia',$estudiante->nombre.' '.$estudiante->apellido,$matricula->ausencia);
          flash('Datos guardados exitosamente!', 'success');

        }else{
            flash('ah ocurrrido un error', 'danger');
        }
        return redirect()->route('estudiantes.perfil',$estudiante->id);
    } 
    public function ver_boletin($id){
        $datos= boletin::find($id);
        $id = $datos->estudiante_id;
        $curso=$datos->nivel;
        $per=$datos->periodo;
        $año=$datos->año_escolar;

        $estudiante=Estudiante::find($id);
        $curso = Curso::where('nombre',$curso)->limit(1)->get();
        $curso->toArray();
        $dimencion = boletin_dimencion::where('nivel',$curso[0]->id_categoria)->get();
        $periodo_numero="";
        for ($i=0; $i <3 ; $i++) { 
            if($this->periodo[$i]==$per){
                $periodo_numero=$i+1;
            }
        }
        $boletin=Boletin::where([['estudiante_id','=',$estudiante->id],['nivel','=',$curso[0]->nombre],['año_escolar','=',$año],['periodo','=',$per]])->limit(1)->get();
   
        
        $nota = Estudiante::join('boletin_notas','boletin_notas.estudiante_id','=','estudiantes.id')
                ->select('boletin_notas.*')
                ->where([['boletin_notas.estudiante_id','=',$estudiante->id],['boletin_notas.curso_id','=',$curso[0]->id],['boletin_notas.año','=',$año],['boletin_notas.periodo','=',$periodo_numero]])->get();

        return view('estudiantes.ver_boletin')->with(['dimencion'=>$dimencion,'nota'=>$nota,'boletin'=>$boletin,'periodo'=>$periodo_numero,'estudiante'=>$estudiante,'curso'=>$curso,'per_letra'=>$per]);
    }   
    public function incompletos(Request $request){

        $filtro = Estudiante::whereNotExists(function ($query) {
                $query->select(DB::raw(1))
                      ->from('acudiente_estudiante')
                      ->whereRaw('acudiente_estudiante.estudiante_id = estudiantes.id');
            })->get();
        $filtro1 = Estudiante::whereNotExists(function ($query) {
                $query->select(DB::raw(1))
                      ->from('matriculas')
                      ->whereRaw('matriculas.estudiante_id = estudiantes.id');
            })->get();
$estudiante=$filtro1->union($filtro);
        return view('estudiantes.incompletoEstudiante')->with(['estudiante'=>$estudiante]);
    }
    public function incompletos_verificar($id){
        $filtro = Estudiante::where('id',$id)->whereExists(function ($query) {
                $query->select(DB::raw(1))
                      ->from('acudiente_estudiante')
                      ->whereRaw('acudiente_estudiante.estudiante_id = estudiantes.id');
            })->get();
        
        if($filtro->isNotEmpty()){
            $filtro1 = Estudiante::where('id',$id)->whereExists(function ($query) {
                $query->select(DB::raw(1))
                      ->from('matriculas')
                      ->whereRaw('matriculas.estudiante_id = estudiantes.id');
            })->get();
            if($filtro1->isEmpty()){
                return redirect()->route('matricula.registrar',$id);
            }

        }else{
            return redirect()->route('acudiente.registrar',$id);
        }
    }
}
