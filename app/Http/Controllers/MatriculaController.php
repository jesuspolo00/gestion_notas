<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Colegio;
use App\Matricula;
use App\Curso;
use App\Estudiante;
use App\Historial;
class MatriculaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {     
        $curso = Curso::all();   
         return view('estudiantes.RegistrarMatricula')->with(['estudiante'=>$id,'curso'=>$curso]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $matricula = new Matricula();
        $matricula->curso_id = $request->curso;
        $matricula->estudiante_id = $request->e;
        $matricula->estado = "formacion";
        $estudiante=Estudiante::find($matricula->estudiante_id);
        $curso=Curso::find($matricula->curso_id);
        if($matricula->save()){
            Historial::guardar('Registro','Matricula',$estudiante->nombre.' '.$estudiante->apellido,'Matriculado en '.$curso->nombre);
            flash('Se ah Registrado Correctamente', 'success');
        }else{
            flash('No se completo el proceso', 'danger');
        }
      
        return redirect()->route('estudiantes.buscar');
    }

    public function matricula(Request $request){
        $curso = Curso::all();
        if($request->nivel!=""){
            $old=$request->nivel;
            $estudiante = Curso::todos_estudiante($request->nivel)->select('estudiantes.nombre as estudiante','estudiantes.documento','estudiantes.tipo_documento','estudiantes.id','cursos.nombre','matriculas.id as matricula','estudiantes.apellido')->get();
        }else{
            $old="";
            $estudiante = collect([]);
        }
        return view('configuracion.matricula')->with(['curso'=>$curso,'estudiante'=>$estudiante,'old'=>$old]);
    }
    public function matricula_habilitar(Request $request,$old){
        $estudiante = Curso::todos_estudiante($old)->select('estudiantes.nombre as estudiante','estudiantes.documento','estudiantes.tipo_documento','estudiantes.id','cursos.nombre','matriculas.id as matricula','estudiantes.apellido')->get();
        $request->all();
        foreach($estudiante as $estudiantes){
            if($request[$estudiantes->matricula]=='on'){
                $matricula = Matricula::find($estudiantes->matricula);
                $matricula->curso_id = $request['promover'];
                if($matricula->save()){
                    $promovido = Curso::find($request['promover']);
                    Historial::guardar('Promovido','Nivel',$estudiantes->estudiante.' '.$estudiantes->apellido,'Promovido de '.$estudiantes->nombre.' a '.$promovido->nombre);
                }else{
                    flash('ah ocurrrido un error', 'danger');
                } 
            }
        }
        flash('Estudiantes promovidos exitosamente!', 'success');
        return redirect()->route('configurar.matricula');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
