<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\boletin_dimencion;
use App\Colegio;
use App\Estudiante;
use App\boletin_nota;
use App\boletin;
use App\Curso;
use App\boletin_historial;
use PDF;
use Zipper;
class boletinController extends Controller
{
    private $periodo=['I TRIMESTRE (Agosto-Octubre)','II TRIMESTRE (Noviembre a Febrero)','III TRIMESTRE (Marzo a Junio)'];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * reporte buscar boletin 
     * ver boletin individual
     */
    public function index($id)
    {        
        $datos= boletin::find($id);
        $id = $datos->estudiante_id;
        $curso=$datos->nivel;
        $per=$datos->periodo;
        $año=$datos->año_escolar;

        $estudiante=Estudiante::find($id);
        $curso = Curso::where('nombre',$curso)->limit(1)->get();
        $curso->toArray();
        $dimencion = boletin_dimencion::where('nivel',$curso[0]->id_categoria)->get();
        $periodo_numero="";

               
           

        for ($i=0; $i <3 ; $i++) { 
            if($this->periodo[$i]==$per){
                $periodo_numero=$i+1;
            }
        }
 
        $boletin=Boletin::where([['estudiante_id','=',$estudiante->id],['nivel','=',$curso[0]->nombre],['año_escolar','=',$año],['periodo','=',$per]])->limit(1)->get();
   
        
        $nota = Estudiante::join('boletin_notas','boletin_notas.estudiante_id','=','estudiantes.id')
                ->select('boletin_notas.*')
                ->where([['boletin_notas.estudiante_id','=',$estudiante->id],['boletin_notas.curso_id','=',$curso[0]->id],['boletin_notas.año','=',$año],['boletin_notas.periodo','=',$periodo_numero]])->get();
        
        $pdf= \PDF::loadview('boletin.boletin',['dimencion'=>$dimencion,'nota'=>$nota,'boletin'=>$boletin,'periodo'=>$periodo_numero]);
        return $pdf->stream();
    }
    /* viene de configuracion-generar boletines aca guarda todos los datos y notas del estudiante 
    * en las tablas de boletines
    *
    */
    public function guardar_boletin($id){
        $cur=Curso::find($id);
        if($cur->activo==1){
        $todos=Curso::todos_estudiante($id)->get();
            foreach($todos as $todos_est){
                $estudiante=Estudiante::find($todos_est->id);
                $curso = $estudiante->obtener_cursos($todos_est->id)->orderBy('id', 'desc')->get();        
                $curso->toArray();      

                $colegio = Colegio::find(1);
                if($colegio){
                    $boletin = new boletin();
                    $boletin->ausencia=$curso[0]->ausencia;
                    $boletin->observacion=$curso[0]->observacion;
                    $boletin->nombre=$estudiante->nombre." ".$estudiante->apellido;
                    $boletin->año_lectivo=$colegio->boletin_añolectivo;
                    $boletin->nivel=$curso[0]->curso;
                    $boletin->periodo=$this->periodo[$colegio->periodo_escolar-1];
                    $boletin->año_escolar=$colegio->año_escolar;
                    $boletin->codigo=$colegio->boletin_codigo;
                    $fecha_array=explode('-',$colegio->boletin_fecha);
                    $fecha=$fecha_array[2].'/'.$fecha_array[1].'/'.$fecha_array[0];
                    $boletin->fecha=$fecha;
                    $boletin->estudiante_id=$estudiante->id;
                    $boletin->save();
                    $nota = Estudiante::join('nota','nota.estudiante_id','=','estudiantes.id')
                            ->select('nota.*')
                            ->where([['nota.estudiante_id','=',$estudiante->id],['nota.curso_id','=',$curso[0]->id],['nota.año','=',$colegio->año_escolar],['nota.periodo','=',$colegio->periodo_escolar]])->get();
                    foreach($nota as $notas){
                        $guardar= new boletin_nota();
                        $guardar->ids=$notas->id;
                        $guardar->nota=$notas->nota;
                        $guardar->periodo=$notas->periodo;
                        $guardar->año=$notas->año;
                        $guardar->curso_id=$notas->curso_id;
                        $guardar->materia_id=$notas->materia_id;
                        $guardar->estudiante_id=$notas->estudiante_id;
                        $guardar->save();
                    }
                    $año=$boletin->año_escolar+1;
                    boletin_historial::guardar('Registro de boletin',$boletin->nombre,$boletin->nivel,$boletin->periodo,$boletin->año_escolar.' - '.$año);
                }
            }
            flash('Datos guardados exitosamente!', 'success');
            $cur->activo=2;
            $cur->save();
        }else{
            flash('ah ocurrrido un error', 'danger');
        }
        
        return redirect()->route('configuracion.index','index');

    }
    /* reporte-descargar boletines
    *  se descargan todos los boletines del curso
    *  año actual
    */
    public function descargar_boletin($id)
    {     
        $todos=Curso::todos_estudiante($id)->get();
        if($todos->isNotEmpty()){
            foreach($todos as $todos_est){   

            $estudiante=Estudiante::find($todos_est->id);
            $curso = $estudiante->obtener_cursos($todos_est->id)->orderBy('id', 'desc')->get();
            $curso->toArray();
            $colegio = Colegio::find(1);
            $dimencion = boletin_dimencion::where('nivel',$curso[0]->id_categoria)->get();
            if($colegio){
                $boletin=Boletin::where([['estudiante_id','=',$estudiante->id],['nivel','=',$curso[0]->curso],['año_escolar','=',$colegio->año_escolar],['periodo','=',$this->periodo[$colegio->periodo_escolar-1] ]])->limit(1)->get();
                if($boletin->isNotEmpty()){
                    $nota = Estudiante::join('boletin_notas','boletin_notas.estudiante_id','=','estudiantes.id')
                        ->select('boletin_notas.*')
                        ->where([['boletin_notas.estudiante_id','=',$estudiante->id],['boletin_notas.curso_id','=',$curso[0]->id],['boletin_notas.año','=',$colegio->año_escolar],['boletin_notas.periodo','=',$colegio->periodo_escolar]])->get();
                }else{
                    continue;
                }
            }
           ini_set('max_execution_time', 300);
            $pdf = new PDF();
            $pdf= $pdf::loadview('boletin.boletin',['dimencion'=>$dimencion,'nota'=>$nota,'boletin'=>$boletin,'periodo'=>$colegio->periodo_escolar])->save('pdf/'.$curso[0]->curso."-".$estudiante->nombre."-".$estudiante->documento.".pdf");
          }
             if($this->comprimir($todos[0]->nombre)){
                return response()->download(public_path().'/boletines/'.$todos[0]->nombre.'.zip')->deleteFileAfterSend(true);
            }  
        }else{
            flash('No se encuentra disponible!', 'success');
            return redirect()->route('reportes.descargar');
        }
              
    }
    /* se comprime los pdf en el servidor para asi continuar con la descarga
    *
    *
    */
    protected function comprimir($nombre){
        $zip = new Zipper();
        $files = glob('pdf/*');
        Zipper::make(public_path().'/boletines/'.$nombre.'.zip')->add($files)->close();
        foreach($files as $file){            
            unlink($file);
        }
        return true;
    }
}
