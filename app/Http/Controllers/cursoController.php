<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Curso;
use App\Materia;
use App\Horario;
use App\Dia;
use App\Hora;
use App\Profesor;
use App\Historial;
use App\curso_categoria;
class cursoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(Auth::User()->rol =="2"){
            $id_profesor=Auth::user()->profesor()->select('profesores.id')->get();
            $profesor = Profesor::find($id_profesor[0]->id);
            $curso = $profesor->curso()->join('detalle_curso','cursos.id','=','detalle_curso.curso_id')->select('cursos.*','detalle_curso.*')->paginate(10);
        }else{
            $curso = Curso::Search($request->listar)->paginate(10);
        }        
        return view('cursos.BuscarCursos')->with(['curso'=>$curso]);
    }
    public function perfil(Request $request,$id){   
        $busqueda="";
        $tab=""; 
        $curso = Curso::find($id);     
        $dia= Dia::all();
        $hora = Hora::where('disponible',1)->get();
        $horario = Horario::obtener_horario($id)->get();        
        $calificacion="";        
        $nombre_nota="";
        $nota="";
        $detalle = $curso->detalle()->get(); 
            $materia= Materia::all();
        $detalle->all();
        
        $lista_estudiante = Curso::join('matriculas', 'cursos.id', '=', 'matriculas.curso_id')
                                    ->join('estudiantes','matriculas.estudiante_id','=','estudiantes.id')
                                    ->select('estudiantes.*')
                                    ->where([['matriculas.estado','=','formacion'],['cursos.id','=',$id]])
                                    ->get();
        return view('cursos.curso')->with(['curso'=>$curso,'detalle'=>$detalle,'estudiante'=>$lista_estudiante,'nombre_nota'=>$nombre_nota,'materia'=>$materia,'tab'=>$tab,'busqueda'=>$busqueda,'nota'=>$nota,'horario'=>$horario,'hora'=>$hora,'dia'=>$dia]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function cerrar_periodo($id){
        $curso= Curso::find($id);
        $curso->activo=1;
        if($curso->save()){
            Historial::guardar('Finalizo','Cerrar Periodo','',$curso->nombre);
            flash('Cierre de periodo exitoso!', 'success');
        }else{
            flash('ah ocurrrido un error', 'danger');
        }
        return redirect()->route('cursos.perfil',$id);  
    }
    public function nivel_categoria(){
        $categoria= curso_categoria::all();
        return view('cursos.categoria')->with(['categoria'=>$categoria]);
    }
    public function nivelCat_agregar(Request $request){
        $valor=$request->all();
            
        for ($i=0; $i < count($valor['categoria']) ; $i++) { 
            $categoria = new curso_categoria();
            $categoria->nombre = $valor['categoria'][$i];  
               
           if($categoria->save()){ 
                Historial::guardar('Registro','Nivel Categoria','',$categoria->nombre);
            }else{
                flash('ah ocurrrido un error', 'danger');
            }
                  
        }
        flash('Datos guardados exitosamente!', 'success');
        return redirect()->route('nivel.categoria');
    }
    public function nivelCat_editar(Request $request,$id){
        $cursos= curso_categoria::find($id);
        $cursos->nombre = $request->nombre;
        if($cursos->save()){
            Historial::guardar('Editado','Nivel Categoria','',$cursos->nombre);
            flash('Datos editados exitosamente!', 'success');
        }
        return redirect()->route('nivel.categoria');
    }
    public function nivelCat_eliminar($id){
        $cursos = curso_categoria::find($id);
        $curso=$cursos->nombre;
        if($cursos->delete()){
            Historial::guardar('Eliminado','Nivel','',$curso);
          flash('Datos eliminados exitosamente!', 'success');  
        }
        return redirect()->route('nivel.categoria');
    }
}
