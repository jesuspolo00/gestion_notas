<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Historial;
use App\boletin_historial;
class historialController extends Controller
{
    public function index(Request $request){

        $historial=Historial::Shearch($request->buscar,$request->movimiento,$request->lugar)->paginate(25);

        $movimiento = Historial::distinct('movimiento')->select('movimiento')->get();
        $lugar = Historial::distinct('lugar')->select('lugar')->get();
        return view('historial.buscar')->with(['historial'=>$historial,'movimiento'=>$movimiento,'lugar'=>$lugar]);
    }
    public function historial_boletin(){
    	$historial=boletin_historial::OrderBy('created_at','desc')->paginate(25);
    	return view('historial.boletin')->with(['historial'=>$historial]);
    }

}