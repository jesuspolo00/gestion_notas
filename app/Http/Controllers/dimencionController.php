<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dimencion;
use App\Sub_dimencion;
use App\Competencia;
use App\Sub_competencia;
use App\Materia;
use App\Curso;
use App\boletin_dimencion;
use App\boletin_sub_dimencion;
use App\boletin_competencia;
use App\boletin_sub_competencia;
use App\boletin_materia;
use App\Historial;
use App\curso_categoria;
class dimencionController extends Controller
{


	public function dimencion()
    {
        $curso = curso_categoria::all();//falta
    	$dimencion = Dimencion::all();
        return view('dimenciones.dimencion')->with(['dimencion'=>$dimencion,'curso'=>$curso]);
    }
    public function agregar_dimencion(Request $request)
    {
        
        $valor=$request->all();
            
        for ($i=0; $i < count($valor['dimencion']) ; $i++) { 
            $dimencion = new Dimencion();
            $dimencion->nombre = $valor['dimencion'][$i]; 
            $dimencion->nivel = $valor['nivel'][$i];  
               
           if($dimencion->save()){                
                $boletin = new boletin_dimencion();
                $boletin->ids=$dimencion->id;
                $boletin->nombre = $valor['dimencion'][$i]; 
                $boletin->nivel = $valor['nivel'][$i]; 
                $boletin->save();
                Historial::guardar('Registro','Dimencion','',$dimencion->nivel);
            }else{
                flash('ah ocurrrido un error', 'danger');
            }
                  
        }
        flash('Datos guardados exitosamente!', 'success');
        return redirect()->route('dimencion');
    
    }
    public function eliminar_dimencion(Request $request,$id){
        $dimencion = Dimencion::find($id);
        $nivel = $dimencion->nivel;
        if($dimencion->delete()){
            Historial::guardar('Eliminado','Dimencion','',$nivel);
          flash('Datos eliminados exitosamente!', 'success');  
        }
        return redirect()->route('dimencion');
    }

    public function subdimencion(Request $request){
    	$dimencion = Dimencion::distinct()->get(['nivel']);
        $curso=curso_categoria::all();
    	$subdimencion = Sub_dimencion::Search($request->buscar)->get();
        $verificar_dimencion = Dimencion::find($request->buscar);
        return view('dimenciones.subdimenciones')->with(['curso'=>$curso,'subdimencion'=>$subdimencion,'dimencion'=>$dimencion,'verificar'=>$verificar_dimencion]);
    }
    public function subdimencion_listar($nivel){
        $subdimencion=Dimencion::Where('dimenciones.nivel',$nivel)->get();
        return view('dimenciones.dimencion_ajax')->with(['dimencion'=>$subdimencion]);
    }
    public function agregar_subdimencion(Request $request,$id)
    {
        
        $valor=$request->all();
            
        for ($i=0; $i < count($valor['dimencion']) ; $i++) { 
            $subdimencion = new Sub_dimencion();
            $subdimencion->nombre = $valor['dimencion'][$i];   
            $subdimencion->dimencion_id = $id;
           if($subdimencion->save()){                
                $boletin = new boletin_sub_dimencion();
                $boletin->ids=$subdimencion->id;
                $boletin->nombre = $valor['dimencion'][$i]; 
                $boletin->boletin_dimencion_id = $id; 
                $boletin->save();
                Historial::guardar('Registro','SubDimencion','','');
            }else{
                flash('ah ocurrrido un error', 'danger');
            }
                  
        }
        flash('Datos guardados exitosamente!', 'success');
        return redirect()->route('subdimencion');
    
    }
    public function eliminar_subdimencion(Request $request,$id){
        $subdimencion = Sub_dimencion::find($id);
        if($subdimencion->delete()){
            Historial::guardar('Eliminado','SubDimencion','','');
          flash('Datos eliminados exitosamente!', 'success');  
        }
        return redirect()->route('subdimencion');
    }
    public function competencias(Request $request){
        $dimencion = Dimencion::distinct()->get(['nivel']);
        $curso=curso_categoria::all();
        $competencia = Competencia::Search($request->subdimencion)->get();
        $verificar = Sub_dimencion::find($request->subdimencion);
        return view('dimenciones.competencias')->with(['curso'=>$curso,'competencia'=>$competencia,'dimencion'=>$dimencion,'verificar'=>$verificar]);
    }
    public function competencias_listar($id){
        $subdimencion=Dimencion::join('sub_dimenciones', 'dimenciones.id', '=', 'sub_dimenciones.dimencion_id')
                    ->select('sub_dimenciones.*')
                    ->Where('dimenciones.id',$id)->get();
        return view('dimenciones.subdi_ajax')->with(['subdimencion'=>$subdimencion]);
    }
    public function agregar_competencias(Request $request,$id)
    {
        $valor=$request->all();
        for ($i=0; $i < count($valor['competencia']) ; $i++) { 
            $competencia = new Competencia();
            $competencia->nombre = $valor['competencia'][$i];   
            $competencia->sub_dimencion_id = $id;
           if($competencia->save()){                
                $boletin = new boletin_competencia();
                $boletin->ids=$competencia->id;
                $boletin->nombre = $valor['competencia'][$i]; 
                $boletin->boletin_sub_dimencion_id = $id; 
                $boletin->save();
                Historial::guardar('Registro','Competencia','','');
            }else{
                flash('ah ocurrrido un error', 'danger');
            }
                  
        }
        flash('Datos guardados exitosamente!', 'success');
        return redirect()->route('competencias');
    
    }
    public function eliminar_competencias(Request $request,$id){
        $competencia = Competencia::find($id);
        if($competencia->delete()){
            Historial::guardar('Eliminado','Competencia','','');
          flash('Datos eliminados exitosamente!', 'success');  
        }
        return redirect()->route('competencias');
    }
    public function subcompetencias(Request $request){
        $dimencion = Dimencion::distinct()->get(['nivel']);
        $curso=curso_categoria::all();
        $competencia = Sub_competencia::Search($request->competencia)->get();
        $verificar = Competencia::find($request->competencia);
        return view('dimenciones.subcompetencias')->with(['curso'=>$curso,'competencia'=>$competencia,'dimencion'=>$dimencion,'verificar'=>$verificar]);
    }
    public function subcompetencias_listar($id){
        $competencia=Sub_dimencion::join('competencias', 'sub_dimenciones.id', '=', 'competencias.sub_dimencion_id')
                    ->select('competencias.*')
                    ->Where('sub_dimenciones.id',$id)->get();
        return view('dimenciones.competencia_ajax')->with(['competencias'=>$competencia]);
    }
    public function agregar_subcompetencias(Request $request,$id)
    {
        $valor=$request->all();
        for ($i=0; $i < count($valor['subcompetencia']) ; $i++) { 
            $subcompetencia = new Sub_competencia();
            $subcompetencia->nombre = $valor['subcompetencia'][$i];   
            $subcompetencia->competencia_id = $id;
           if($subcompetencia->save()){ 
                $boletin = new boletin_sub_competencia();
                $boletin->ids=$subcompetencia->id;
                $boletin->nombre = $valor['subcompetencia'][$i]; 
                $boletin->boletin_competencia_id = $id; 
                $boletin->save();  
                Historial::guardar('Registro','SubCompetencia','','');             
                
            }else{
                flash('ah ocurrrido un error', 'danger');
            }
                  
        }
        flash('Datos guardados exitosamente!', 'success');
        return redirect()->route('subcompetencias');
    
    }
    public function eliminar_subcompetencias(Request $request,$id){
        $subcompetencia = Sub_competencia::find($id);
        if($subcompetencia->delete()){
            Historial::guardar('Eliminado','SubCompetencia','','');
          flash('Datos eliminados exitosamente!', 'success');  
        }
        return redirect()->route('subcompetencias');
    }
    public function materias(Request $request){
         $dimencion = Dimencion::distinct()->get(['nivel']);
        $curso=curso_categoria::all();
        $competencia = Materia::Search($request->subcompetencia)->get();
        $verificar = Sub_competencia::find($request->subcompetencia);
        return view('dimenciones.materias')->with(['curso'=>$curso,'competencia'=>$competencia,'dimencion'=>$dimencion,'verificar'=>$verificar]);
    }
    public function materias_listar($id){
        $subcompetencia=Competencia::join('sub_competencias', 'sub_competencias.competencia_id', '=', 'competencias.id')
                    ->select('sub_competencias.*')
                    ->Where('competencias.id',$id)->get();
        return view('dimenciones.subcompetencia_ajax')->with(['subcompetencias'=>$subcompetencia]);
    }
    public function agregar_materias(Request $request,$id)
    {

        $valor=$request->all();
        for ($i=0; $i < count($valor['materia']) ; $i++) { 
            $materias = new Materia();
            $materias->nombre = $valor['materia'][$i]; 
            $materias->periodo = $valor['periodo'][$i];  
            $materias->sub_competencia_id = $id;
           if($materias->save()){ 
                $boletin = new boletin_materia();
                $boletin->ids=$materias->id;
                $boletin->nombre = $valor['materia'][$i]; 
                $boletin->periodo = $valor['periodo'][$i]; 
                $boletin->boletin_sub_competencia_id = $id; 
                $boletin->save();   
                Historial::guardar('Registro','Materia','','');               
            }else{
                flash('ah ocurrrido un error', 'danger');
            }
                  
        }
        flash('Datos guardados exitosamente!', 'success');
        return redirect()->route('materias');
    
    }
    public function eliminar_materias(Request $request,$id){
        $materias = Materia::find($id);
        if($materias->delete()){
            Historial::guardar('Eliminado','Materia','','');
          flash('Datos eliminados exitosamente!', 'success');  
        }
        return redirect()->route('materias');
    }
}
