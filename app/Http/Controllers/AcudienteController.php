<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\acudienteRequest;
use App\Acudiente;
use App\Estudiante;
use App\Historial;
use App\Http\Requests\UpdateEstudiante;
class AcudienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request,$id)
    {
        $acudiente = Acudiente::Search($request->listar)->get();
       return view('acudiente.acudienteRegistrar')->with(['estudiante'=>$id,'acudiente'=>$acudiente]); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(acudienteRequest $request)
    {
        $acudiente = new acudiente();
        $acudiente->nombre = $request->nombre;
        $acudiente->apellido = $request->apellido;     
        $acudiente->tipo_documento = $request->tdocumento;       
        $acudiente->documento = $request->documento;
        $acudiente->correo = $request->correo;
        $acudiente->direccion = $request->direccion;
        $acudiente->telefono = $request->telefono;
        $acudiente->genero = $request->genero;

        
        if($acudiente->save()){
            $estudiante = Estudiante::find($request->e);
            $estudiante->acudiente()->sync($acudiente->id);
            Historial::guardar('Registro','Acudiente',$acudiente->nombre.' '.$acudiente->apellido,'');
            flash('Datos guardados exitosamente!', 'success');
        }else{
            flash('ah ocurrrido un error', 'danger');
        }

        return redirect()->route('matricula.registrar',$request->e);
    }
    public function update(UpdateEstudiante $request,$id,$acudiente){
        $acudiente = Acudiente::find($acudiente);
        $acudiente->nombre = $request->nombre;
        $acudiente->apellido = $request->apellido;     
        $acudiente->tipo_documento = $request->tdocumento;       
        $acudiente->documento = $request->documento;
        $acudiente->correo = $request->correo;
        $acudiente->direccion = $request->direccion;
        $acudiente->telefono = $request->telefono;
        $acudiente->genero = $request->genero;

        
        if($acudiente->save()){
            Historial::guardar('Editado','Acudiente',$acudiente->nombre.' '.$acudiente->apellido,'');
            flash('Datos guardados exitosamente!', 'success');
        }else{
            flash('ah ocurrrido un error', 'danger');
        }
        return redirect()->route('estudiantes.acudiente',$id);
    }

    public function seleccionar($id,$acudiente){
        $estudiante = Estudiante::find($id);
        $estudiante->acudiente()->sync($acudiente);
        flash('Datos guardados exitosamente!', 'success');
        return redirect()->route('matricula.registrar',$id);
    }
    public function acudiente_seleccion($id,$acudiente){
        $estudiante = Estudiante::find($id);
        $estudiante->acudiente()->sync($acudiente);
        flash('Nuevo acudiente seleccionado!', 'success');
        return redirect()->route('estudiantes.acudiente',$id);
    }
    public function editar(Request $request,$estudiante){
        $acudi = Acudiente::Search($request->listar)->get();
        $estudiante = Estudiante::find($estudiante);
        $acudiente= $estudiante->acudiente()->get();
        return view('acudiente.editar')->with(['acudi'=>$acudi,'acudiente'=>$acudiente,'estudiante'=>$estudiante]);
    }
}
