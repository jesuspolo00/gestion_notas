<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Curso;
use App\boletin_nota;
use App\boletin;
class reporteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function individual(Request $request){
       
        $boletin=boletin::Search($request->curso,$request->periodo,$request->año,$request->estudiante)->paginate(20);
        $año = boletin_nota::distinct('año')->select('año')->get();
        $curso = boletin_nota::join('cursos','cursos.id','=','boletin_notas.curso_id')->distinct('cursos_id')->select('cursos.nombre','cursos.id')->get();
        return view('reporte.buscar')->with(['año'=>$año,'curso'=>$curso,'boletin'=>$boletin]);
    }
    public function descargar_boletin()
    {
        $curso= Curso::Where('cursos.activo',2)->paginate(15);
        return view('reporte.descargar')->with(['curso'=>$curso]);
    }
}
