<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\confirmarRequest;
use App\Usuario;
use App\Historial;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Acudiente;
use App\Profesor;
class usuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $usuario = Usuario::Search($request->buscar)->orderBy('id','DESC')->paginate(15);
        return view('usuario.buscar')->with(['usuario'=>$usuario]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario = Usuario::find($id);
        return view('usuario.editar')->with(['usuario'=>$usuario]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $usuario = Usuario::find($id);
        $usuario->usuario = $request->name;
        $usuario->documento = $request->documento;
        $usuario->email = $request->email;
        $usuario->rol = $request->rol;
        if($usuario->save()){
            Historial::guardar('Editado','Usuario',$usuario->usuario,'');
            flash('Datos guardados exitosamente!', 'success');
        }else{
            flash('ah ocurrrido un error', 'danger');
        }
        return redirect()->route('usuario.editar',$id);
    }
    public function confirmar(confirmarRequest $request, $id){
        $usuario = Usuario::find($id);
        $usuario->password = bcrypt($request->password);
        if($usuario->save()){
            Historial::guardar('Editado','Usuario',$usuario->usuario,'Cambio de clave');
            flash('Datos guardados exitosamente!', 'success');
        }else{
            flash('ah ocurrrido un error', 'danger');
        }
        return redirect()->route('usuario.editar',$id);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $usuario = Usuario::find($id);
         Historial::guardar('Eliminado','Usuario',$usuario->usuario,'');
        if($usuario->delete()){
            flash('Se eliminaron los datos correctamente!', 'success');
        }else{
            flash('ah ocurrrido un error', 'danger');
        }
        return redirect()->route('usuario');
    }
    public function perfil(){
        if(Auth::User()->rol =="1"){
            $usuario = Usuario::join('acudientes','acudientes.documento','=','usuarios.documento')->where('usuarios.id',Auth::User()->id)->limit(1)->get();
            if($usuario->isEmpty()){
            flash('Esta cuenta no está vinculada por favor contacte con el administrador!', 'warning');
            return redirect()->route('inicio');
            }
        }elseif(Auth::User()->rol =="2"){
            $usuario = Usuario::join('profesores','profesores.documento','=','usuarios.documento')->where('usuarios.id',Auth::User()->id)->limit(1)->get();
            if($usuario->isEmpty()){
            flash('Esta cuenta no está vinculada por favor contacte con el administrador!', 'warning');
            return redirect()->route('inicio');
            }
        }else{
            $usuario = Usuario::find(Auth::User()->id);
        }
        return view('usuario.perfil')->with(['usuario'=>$usuario]);
    }
    public function perfil_actualizar(confirmarRequest $request){
        $usuario = Usuario::find(Auth::User()->id);
        if (Hash::check($request->actual, $usuario->password)) {
            $usuario->password=bcrypt($request->password);
            if($usuario->save()){
            Historial::guardar('Editado','Usuario',$usuario->usuario,'Cambio de clave');
                flash('Datos guardados exitosamente!', 'success');
            }else{
                flash('ah ocurrrido un error', 'danger');
            }
        }else{
            flash('Contraseña actual incorrecta', 'danger');
        }
        return redirect()->route('usuario.perfil');
        
    }
    public function datos_actualizar(Request $request){
        
        $usuario = Usuario::find(Auth::User()->id);
        $usuario->usuario=$request->usuario;
        if($usuario->save()){
            if(Auth::User()->rol =="1"){
                $id_acudiente = Usuario::join('acudientes','acudientes.documento','=','usuarios.documento')->where('usuarios.id',Auth::User()->id)->select('acudientes.id')->limit(1)->get();
                $acudiente=Acudiente::find($id_acudiente[0]->id);
                $acudiente->nombre = $request->nombre;
                $acudiente->apellido=$request->apellido;
                $acudiente->correo=$request->correo;
                $acudiente->genero=$request->genero;
                $acudiente->telefono=$request->telefono;
                $acudiente->direccion=$request->direccion;
                if($acudiente->save()){
                    flash('Datos guardados exitosamente!', 'success');
                }else{
                    flash('ah ocurrrido un error', 'danger');
                }           
            
            }elseif(Auth::User()->rol =="2"){
                $id_profesor = Usuario::join('profesores','profesores.documento','=','usuarios.documento')->where('usuarios.id',Auth::User()->id)->select('profesores.id')->limit(1)->get();
                $profesor=Profesor::find($id_profesor[0]->id);
                $profesor->nombre = $request->nombre;
                $profesor->apellido=$request->apellido;
                $profesor->correo=$request->correo;
                $profesor->genero=$request->genero;
                $profesor->telefono=$request->telefono;
                $profesor->direccion=$request->direccion;
                if($profesor->save()){
                    flash('Datos guardados exitosamente!', 'success');
                }else{
                    flash('ah ocurrrido un error', 'danger');
                }
            }
        }
        return redirect()->route('usuario.perfil');

    }
    public function color($clase){
    
        $usuario = Usuario::find(Auth::User()->id);
        $usuario->color = $clase;
        if($usuario->save()){
            echo "guardado";
        }else{
            echo "";
        }
       
    }

}
