<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Curso;
use App\Colegio;
use App\Matricula;
use App\Estudiante;
use App\Historial;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return view('index');
    }
    public function finalizar_periodo(){
        $colegio = Colegio::find(1);
       
        if($colegio){ 
            if($colegio->periodo_escolar!=3){
                $curso = Curso::all();
                foreach($curso as $cursos){
                    $cur = Curso::find($cursos->id);
                    $cur->activo=0;
                    if($cur->save()){

                    }else{
                        flash('ah ocurrrido un error', 'danger');
                    }
                }
                $matricula = Matricula::all();
                foreach($matricula as $matri){
                    $ma = Matricula::find($matri->id);
                    $ma->ausencia=0;
                    if($ma->save()){

                    }else{
                        flash('ah ocurrrido un error', 'danger');
                    }
                }
                $colegio->periodo_escolar=$colegio->periodo_escolar+1;
                if($colegio->save()){

                }else{
                    flash('ah ocurrrido un error', 'danger');
                }
                Historial::guardar('Finalizo',$colegio->periodo_escolar,'','');
                flash('Se guardo la configuracion exitosamente!', 'success');
            }else{
                flash('No se puede finalizar mas periodos!. Para continuar finalizar año', 'warning');
            }
        }

        return redirect()->route('inicio');
    }
    public function finalizar_año(){
        $colegio = Colegio::find(1);
       
        if($colegio){ 
            if($colegio->periodo_escolar==3){

                $curso = Curso::all();
                foreach($curso as $cursos){
                    $cur = Curso::find($cursos->id);
                    $cur->activo=0;
                    if($cur->save()){

                    }else{
                        flash('ah ocurrrido un error', 'danger');
                    }
                }
                $matricula = Matricula::all();
                foreach($matricula as $matri){
                    $ma = Matricula::find($matri->id);
                    $ma->ausencia=0;
                    if($ma->save()){

                    }else{
                        flash('ah ocurrrido un error', 'danger');
                    }
                }
            
                $colegio->periodo_escolar=1;
                $colegio->año_escolar=$colegio->año_escolar+1;

                if($colegio->save()){

                }else{
                    flash('ah ocurrrido un error', 'danger');
                }
                Historial::guardar('Finalizo',$colegio->año_escolar,'','');
                flash('Se guardo la configuracion exitosamente!', 'success');
            }else{
                flash('Solo se puede finalizar el año en el ultimo periodo', 'warning');
            }

        }
        return redirect()->route('inicio');
    }
    
}
