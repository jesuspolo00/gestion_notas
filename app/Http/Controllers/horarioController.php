<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hora;
use App\Dia;
use App\Horario;
use App\Curso;
use App\Materia;
class horarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hora = Hora::all();
        return view('configuracion.horario')->with(['hora'=>$hora]);
    }
    public function cursos(Request $request){
        $selected="";
        $horario=collect([]);
        if($request->boton=="buscar"){
            $horario = Horario::Buscar_Horario($request)->get();
            $selected=['curso'=>$request->curso,'periodo'=>$request->periodo,'año'=>$request->año];
        }      
        
        $dia = Dia::all();
        $hora = Hora::where('disponible',1)->get(); 
        $curso = Curso::all();
        return view('cursos.horario')->with(['curso'=>$curso,'dia'=>$dia,'hora'=>$hora,'horario'=>$horario,'selected'=>$selected]);
    }

    public function store(Request $request){
        $request->all();
        for ($i=1; $i < 25 ; $i++) { 
            if($request['h'.$i] !=""){

                $hora = Hora::find($i);  
                $hora->disponible = 1;
                $hora->save();         
            }else{
                $hora = Hora::find($i);  
                $hora->disponible = 0;
                $hora->save();  
            }
        }
        flash('Se guardo la configuracion exitosamente!', 'success');
        return redirect()->route('horario');
    }

    

}
