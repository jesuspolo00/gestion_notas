<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Colegio;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Materia;
use App\Curso;
use App\Hora;
use App\Dia;
use App\Horario;
use App\Detalle_curso;
use App\Historial;
use App\Dimencion;
use App\curso_categoria;
class ConfiguracionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request,$tab)
    {
        $selected="";
        $colegio = Colegio::find(1);
        $horario=collect([]);
        $dia = Dia::all();
        $hora = Hora::where('disponible',1)->get(); 
        $curso = Curso::all();
        $mat = $curso->first();
        $categoria= curso_categoria::all();
        $materia=Dimencion::where('nivel',$mat->id)->get();
       
        if($request->boton=="buscar"){
            $horario = Horario::Buscar_Horario($request)->get();
            $selected=['curso'=>$request->curso,'periodo'=>$request->periodo,'año'=>$request->año];
            $materia=Dimencion::where('nivel',$request->curso)->get();
        }elseif($request->boton=="guardar"){
            Horario::Guardar($request);
        }
        if($request->boletin=="boletin" and $request->generar!=""){
            $generar=Curso::Buscar($request->generar)->get();
        }else{
            $generar=Curso::where('activo',1)->get();
        }
        return view('configuracion.configuracion')->with(['colegio'=>$colegio,'materia'=>$materia,'curso'=>$curso,'dia'=>$dia,'hora'=>$hora,'tab'=>$tab,'horario'=>$horario,'selected'=>$selected,'generar'=>$generar,'categoria'=>$categoria]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function colegio(Request $request)
    {
        $colegio = Colegio::find(1);
       
        if($colegio){           

            $colegio->nombre = $request->nombre;
            $colegio->sigla = $request->sigla;
            $colegio->año_escolar=$request->año;
            $colegio->periodo_escolar=$request->periodo;
            /*$file=$request->file('logo');
            if($file !=""){
                $name= "\colegio"."-".time().".".$file->getClientOriginalExtension();
                $path = public_path()."\images\logo";
                $file->move($path,$name);
                if($colegio->logo !=""){
                    unlink($colegio->logo);
                }               
               $colegio->logo="\images\logo".$name;
            }*/
            
            if($colegio->save()){
                Historial::guardar('Editado','Configuracion','','');
                 flash('Se guardo la configuracion exitosamente!', 'success');
            }
        }else{
            $colegio = new Colegio();
            $colegio->nombre = $request->nombre;
            $colegio->sigla = $request->sigla;
            $colegio->año_escolar=$request->año;
            $colegio->periodo_escolar=$request->periodo;
            /*$file=$request->file('logo');
            if($file !=""){
                $name= "\colegio"."-".time().".".$file->getClientOriginalExtension();
                $path = public_path()."\images\logo";
                $file->move($path,$name);
                $colegio->logo="\images\logo".$name;
            }*/
            if($colegio->save()){
                Historial::guardar('Registro','Configuracion','','');
                flash('Se guardo la configuracion exitosamente!', 'success');
            }

        }
        return redirect()->route('configuracion.index','index');
    }
    public function conf_boletin(Request $request){
        $colegio = Colegio::find(1);
       
        if($colegio){ 
            
            $colegio->boletin_codigo=$request->codigo;
            $colegio->boletin_fecha=$request->fecha;
            $colegio->boletin_añolectivo=$request->lectivo;
            if($colegio->save()){
                Historial::guardar('Editado','Configuracion del boletin','','');
                flash('Se guardo la configuracion exitosamente!', 'success');
            }
        }else{
            $colegio=new Colegio();
            $colegio->boletin_codigo=$request->codigo;
            $colegio->boletin_fecha=$request->fecha;
            $colegio->boletin_añolectivo=$request->lectivo;
            if($colegio->save()){
                Historial::guardar('Registro','Configuracion del boletin','','');
                flash('Se guardo la configuracion exitosamente!', 'success');
            }
        }
        return redirect()->route('configuracion.index','boletin');
    }
     /**
     * Display the specified resource.
     *
     * @param  request
     * @return \Illuminate\Http\Response
        **********-curso-***********
     */
    public function cursos(Request $request)
    {
        
        $valor=$request->all();
       
        for($f=0;$f < count($valor['curso']);$f++) {
            
            $cursos = new Curso();
            $cursos->nombre = $valor['curso'][$f];
            $cursos->id_categoria = $valor['categoria'][$f];       
           if($cursos->save()){                
                Historial::guardar('Registro','Nivel','',$cursos->nombre);
            }else{
                flash('ah ocurrrido un error', 'danger');
            }
        }
        flash('Datos guardados exitosamente! Por favor completar la configuracion del Nivel', 'success');
        return redirect()->route('configuracion.index','nivel');
    
    }
    public function editar_cursos(Request $request,$id){
        $cursos= Curso::find($id);
        $cursos->nombre = $request->nombre;
        if($cursos->save()){
            Historial::guardar('Editado','Nivel','',$cursos->nombre);
            flash('Datos editados exitosamente!', 'success');
        }
        return redirect()->route('configuracion.index','nivel');
    }
    public function eliminar_cursos(Request $request,$id){
        $cursos = Curso::find($id);
        $curso=$cursos->nombre;
        if($cursos->delete()){
            Historial::guardar('Eliminado','Nivel','',$curso);
          flash('Datos eliminados exitosamente!', 'success');  
        }
        return redirect()->route('configuracion.index','nivel');
    }
    public function detalle_curso($id){
        $curso = Curso::find($id);
        $detalle= $curso->detalle()->get();
        $materias= Materia::all();
        
        $asignadas= $curso->materia()->get();
        
        return view('configuracion.detalleCurso')->with(['detalle'=>$detalle,'curso'=>$curso,'materia'=>$materias,'asignadas'=>$asignadas]); 
    }
    public function detalle_registrar(Request $request,$id){
        $curso = Curso::find($id);
        $detalle = $curso->detalle()->get();
        if($detalle->isNotEmpty()){

            $detalle = Detalle_curso::find($detalle[0]->id);
            $detalle->director_grupo = $request->director;
            $detalle->tipo_calificacion = $request->tcalificacion;
            $detalle->numero_estudiantes = $request->numero;
            $detalle->nivel_formacion = $request->nivel;
            if($detalle->save()){
                Historial::guardar('Registro','Detalle Nivel','',$curso->nombre);
                flash('Datos editados exitosamente!', 'success');
            }else{
                flash('ah ocurrrido un error', 'danger');
            }
        }else{
            $detalle = new Detalle_curso();
            $detalle->curso_id = $id;
            $detalle->director_grupo = $request->director;
            $detalle->tipo_calificacion = $request->tcalificacion;
            $detalle->numero_estudiantes = $request->numero;
            $detalle->nivel_formacion = $request->nivel;
            if($detalle->save()){
                flash('Datos registrados exitosamente!', 'success');
                Historial::guardar('Editado','Detalle Nivel','',$curso->nombre);
            }else{
                flash('ah ocurrrido un error', 'danger');
            }
        }
        return redirect()->route('cursos.detalle',$id);
    }
    public function detalle_asignar(Request $request,$id){
        $valor=$request->all();
       
        for($f=1;$f < count($valor);$f++) {
            
            $curso = Curso::find($id);
           $curso->materia()->attach($request->materia);     
          
        }
        return redirect()->route('cursos.detalle',$id);
    }
    public function detalle_eliminar($id,$curso){        
        $pivote = DB::table('curso_materia')->Where([['curso_id','=',$curso],['materia_id','=',$id]])->delete();

        flash('Datos eliminados exitosamente!', 'success');
        return redirect()->route('cursos.detalle',$curso);
    }
    // habilitado 1
    // no habilitado 0
    public function habilitar_cursos(Request $request){
        $request=$request->all();
        $curso=Curso::all();
        foreach ($curso as $cursos) { 
            if(isset($request["c".$cursos->id])){
                $habilitar = Curso::find($cursos->id);           
                $habilitar->activo=$request["c".$cursos->id];
                if($habilitar->save()){

                }else{
                    flash('ah ocurrrido un error', 'danger');
                }
            }
            
        }
        Historial::guardar('Editado','Habilito y/o Desahabilito Nivel','','');
        flash('Datos registrados exitosamente!', 'success');
        return redirect()->route('configuracion.index','habilitar');
    }

}
