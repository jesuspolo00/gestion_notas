<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Curso;
use App\Estudiante;
use App\pagos;
use App\Colegio;
use App\Historial;
class pagosController extends Controller
{
    protected $meses = ['enero','febrero','marzo','abril','mayo','junio','julio','agosto','septiembre','octubre','noviembre','diciembre'];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $curso = Curso::all();
        if($request->buscar !=""){
            $estudiante = Curso::todos_estudiante($request->buscar)->select('estudiantes.*')->paginate(25);
        }else{
            $estudiante = collect([]);
        }
        return view('pagos.pagos')->with(['curso'=>$curso,'estudiante'=>$estudiante]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function guardar($id)
    {
        $estudiante = Estudiante::find($id);
        $colegio = Colegio::find(1);
        $pago = $estudiante->pagos()->where('año',$colegio->año_escolar)->get();
        return view('pagos.guardar')->with(['estudiante'=>$estudiante,'pago'=>$pago]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $estudiante=Estudiante::find($id);
        $request->all();
        foreach ($this->meses as $meses) {
            $colegio = Colegio::find(1);
            $validar= pagos::where('estudiante_id',$id)->where('mes',$meses)->where('año',$colegio->año_escolar)->get();
            $pago = $request[$meses];
            if($validar->isNotEmpty()){
                if($pago != ""){
                    
                }else{
                    $update=pagos::find($validar[0]->id);
                    Historial::guardar('Eliminado','Pago',$estudiante->nombre.' '.$estudiante->apellido,'Mes de '.$meses);
                    if($update->delete()) {
                    }else{
                        flash('ah ocurrrido un error', 'danger');
                    }
                }  
            }else{
                if($pago != ""){
                    $pagos=new pagos();
                    $pagos->mes=$meses;
                    $pagos->año=$colegio->año_escolar;
                    $pagos->pago='si';
                    $pagos->estudiante_id=$id;
                    if ($pagos->save()) {
                        Historial::guardar('Registro','Pago',$estudiante->nombre.' '.$estudiante->apellido,'Mes de '.$meses);
                    }else{
                        flash('ah ocurrrido un error', 'danger');
                    }
                }            
            }
        }
        flash('Datos guardados exitosamente!', 'success');
         return redirect()->route('pagos.guardar',$id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
