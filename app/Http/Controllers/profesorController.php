<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profesor;
use App\Dimencion;
use App\Curso;
use App\Historial;
use App\Http\Requests\profesoresRequest;
class profesorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $profesor = Profesor::Search($request->listar)->orderBy('id','DESC')->paginate(10);
        return view('profesores.BuscarProfesor')->with(['profesor'=>$profesor]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $curso = Curso::all();
        return view('profesores.registrarProfesor')->with(['curso'=>$curso]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(profesoresRequest $request)
    {
        $profesor = new Profesor();
        $profesor->nombre = $request->nombre;
        $profesor->apellido = $request->apellido;     
        $profesor->tipo_documento = $request->tdocumento;       
        $profesor->documento = $request->documento;
        $profesor->correo = $request->correo;
        $profesor->direccion = $request->direccion;
        $profesor->telefono = $request->telefono;
        $profesor->genero = $request->genero;        
        if($profesor->save()){
            $profesor->dimencion()->sync($request->dimencion);
            $profesor->curso()->sync($request->curso);
            Historial::guardar('Registro','Profesor',$profesor->nombre.' '.$profesor->apellido,'');
            flash('Datos guardados exitosamente!', 'success');
        }else{
            flash('ah ocurrrido un error', 'danger');
        }
       
        return redirect()->route('profesores.buscar');
       
    }

   
    public function edit($id)
    {
        $salon=Curso::all();
        $profesor = Profesor::find($id);
        $curso = $profesor->curso()->get();
        $materia = Dimencion::where('nivel',$curso[0]->id)->get();
        $dimencion=$profesor->dimencion()->get();


        return view('profesores.editarProfesor')->with(['profesor'=>$profesor,'curso'=>$curso,'dimencion'=>$dimencion,'materia'=>$materia,'salon'=>$salon]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $profesor = Profesor::find($id);
        $profesor->nombre = $request->nombre;
        $profesor->apellido = $request->apellido;     
        $profesor->tipo_documento = $request->tdocumento;       
        $profesor->documento = $request->documento;
        $profesor->correo = $request->correo;
        $profesor->direccion = $request->direccion;
        $profesor->telefono = $request->telefono;
        $profesor->genero = $request->genero;
        $profesor->dimencion()->sync($request->dimencion);
        $profesor->curso()->sync($request->curso);
        if($profesor->save()){        
            Historial::guardar('Editado','Profesor',$profesor->nombre.' '.$profesor->apellido,'');
            flash('Datos guardados exitosamente!', 'success');

        }else{
            flash('ah ocurrrido un error', 'danger');
        }
        return redirect()->route('profesores.buscar');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $profesor = Profesor::find($id);
        Historial::guardar('Eliminado','Profesor',$profesor->nombre.' '.$profesor->apellido,'');
        if($profesor->delete()){
            flash('Se eliminaron los datos correctamente!', 'success');
        }else{
            flash('ah ocurrrido un error', 'danger');
        }
        return redirect()->route('profesores.buscar');
    }
    public function ajax($id){
        $dimencion = Dimencion::where('nivel',$id)->get();
        $return ="";
        foreach($dimencion as $dimenciones){
            $return.='<option value="'.$dimenciones->id.'">'.$dimenciones->nombre.'</option>';
        }
       
        echo $return;
    }
    public function ajaxEditar($profesor,$id){
       
        $profesor=Profesor::find($profesor);
        $nivel=$profesor->curso()->get();
        if($nivel[0]->id_categoria==$id){
            $dimencion = Dimencion::where('nivel',$nivel[0]->id_categoria)->get();
            $di=$profesor->dimencion()->get();       
            $return ="";
            foreach($dimencion as $dimenciones){
                $verificar=0;
                foreach($di as $profesores){
                    if($profesores->id==$dimenciones->id){
                        $return.='<option value="'.$dimenciones->id.'" selected>'.$dimenciones->nombre.'</option>';
                        $verificar=1;
                    }else{
                        continue;
                    }

                }
                if($verificar==0){

                $return.='<option value="'.$dimenciones->id.'">'.$dimenciones->nombre.'</option>';
                }
            }

        }else{
            $dimencion = Dimencion::where('nivel',$id)->get();
            $return ="";
            foreach($dimencion as $dimenciones){
                $return.='<option value="'.$dimenciones->id.'">'.$dimenciones->nombre.'</option>';
            }
        }
        echo $return;
    }
}
