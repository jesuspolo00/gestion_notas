<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class profesoresRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre'=>'max:120|required',
            'apellido'=>'max:120|required',
            'tdocumento'=>'max:16|required',
            'documento'=>'unique:profesores,documento|required|numeric',
            'correo'=>'email|required',
            'telefono'=>'required|integer',
            'direccion'=>'required',
            'genero'=>'required'
        ];
    }
}
