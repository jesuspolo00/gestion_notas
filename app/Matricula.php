<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Matricula extends Model
{
   protected $table = 'matriculas';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ausencia','observacion','estado','curso_id','estudiante_id',
    ];

    public function curso(){
    	return $this->belongsTo('App\Curso');
    }
    public function estudiante(){
    	return $this->belongsTo('App\Estudiante');
    }
}
