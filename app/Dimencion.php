<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Dimencion;
use App\Competencia;
use App\Colegio;
class Dimencion extends Model
{
    protected $table = 'dimenciones';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre','nivel', 
    ];
    public function sub_dimencion(){
    	return $this->hasMany('App\Sub_dimencion');
    }
    public function profesor(){
        return $this->belongsToMany('App\Profesor');
    }
    public static function materias_boletin($id){
        $colegio=Colegio::find(1);
        return Dimencion::join('sub_dimenciones','dimenciones.id','=','sub_dimenciones.dimencion_id')
                        ->join('competencias','sub_dimenciones.id','=','competencias.sub_dimencion_id')
                        ->join('sub_competencias','competencias.id','=','sub_competencias.competencia_id')
                        ->join('materias','sub_competencias.id','=','materias.sub_competencia_id')
                        ->select('materias.*','dimenciones.nombre as dimencion','sub_dimenciones.nombre as sub_dimencion','competencias.nombre as competencia','sub_competencias.nombre as sub_competencia')
                        ->where([['dimenciones.id','=',$id],['materias.periodo','=',$colegio->periodo_escolar]]);
    }
    public static function subcompetencias_boletin($id){
        return Dimencion::join('sub_dimenciones','dimenciones.id','=','sub_dimenciones.dimencion_id')
                        ->join('competencias','sub_dimenciones.id','=','competencias.sub_dimencion_id')
                        ->join('sub_competencias','competencias.id','=','sub_competencias.competencia_id')
                        ->select('sub_competencias.*')
                        ->where('dimenciones.id',$id);
    }
    public static function competencias_boletin($id){
        return Dimencion::join('sub_dimenciones','dimenciones.id','=','sub_dimenciones.dimencion_id')
                        ->join('competencias','sub_dimenciones.id','=','competencias.sub_dimencion_id')
                        ->select('competencias.*')
                        ->where('dimenciones.id',$id);
    }
    public static function subdimenciones_boletin($id){
        return Dimencion::join('sub_dimenciones','dimenciones.id','=','sub_dimenciones.dimencion_id')
                        ->select('sub_dimenciones.*')
                        ->where('dimenciones.id',$id);
    }
    public static function contar($id){
        $colegio= Colegio::find(1);
        $materia=Competencia::join('sub_competencias','competencias.id','=','sub_competencias.competencia_id')->join('materias','sub_competencias.id','=','materias.sub_competencia_id')
                    ->where('competencias.id',$id)->where('materias.periodo',$colegio->periodo_escolar);
        $subc=Competencia::join('sub_competencias','competencias.id','=','sub_competencias.competencia_id')->where('competencias.id',$id);
        return $materia->count()+$subc->count()+1;
    }
}
