<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Competencia extends Model
{
    protected $table = 'competencias';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre','sub_dimencion_id', 
    ];

    public function sub_dimencion(){
    	return $this->belongsTo('App\Sub_dimencion');
    }
    public function sub_competencia(){
    	return $this->hasMany('App\Sub_competencia');
    }
    public static function scopeSearch($query,$buscar){
        return $query->join('sub_dimenciones','sub_dimenciones.id','=','competencias.sub_dimencion_id')
                    ->join('dimenciones', 'dimenciones.id', '=', 'sub_dimenciones.dimencion_id')                    
                    ->select('competencias.*')
                    ->Where('sub_dimenciones.id',$buscar);
    }
}
