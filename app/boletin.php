<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class boletin extends Model
{
    protected $table = 'boletines';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'codigo','año_lectivo','nombre','nivel','periodo','fecha','ausencia','año_escolar','observacion','estudiante_id',
    ];
    public function estudiante(){
    	return $this->belongsTo('App\Estudiante');
    }
    public static function scopeSearch($query,$curso="",$periodo="",$año="",$estudiante=""){
        $array = ['I TRIMESTRE (Agosto-Octubre)','II TRIMESTRE (Noviembre a Febrero)','III TRIMESTRE (Marzo a Junio)'];
        if($año!=""){
            $año = explode("-",$año);
            $año = $año[0]." - ".$año[1];
        }else{
            $año="";
        }
        if($periodo!=""){
           $periodo= $array[$periodo-1];
        }else{
            $periodo="";
        }  

        if($curso !="" and $periodo!="" and $año!="" and $estudiante!=""){
            return $query->join('estudiantes','estudiantes.id','=','boletines.estudiante_id')
            ->where('nivel',$curso)
            ->where('periodo',$periodo)
            ->where('año_lectivo',$año)
            ->Where(function ($query) use($estudiante) {
                $query->where('estudiantes.nombre','like','%'.$estudiante.'%')->orwhere('estudiantes.apellido','like','%'.$estudiante.'%');
            })->select('boletines.*','boletines.id');

        }elseif($curso !="" and $periodo !="" and $año!="" and $estudiante==""){
            return $query->join('estudiantes','estudiantes.id','=','boletines.estudiante_id')
            ->where([['nivel','=',$curso],['periodo','=',$periodo],['año_lectivo','=',$año]])
            ->select('boletines.*','boletines.id');

        }elseif($curso !="" and $periodo =="" and $año!="" and $estudiante!=""){
            return $query->join('estudiantes','estudiantes.id','=','boletines.estudiante_id')
            ->where([['nivel','=',$curso],['año_lectivo','=',$año]])
            ->Where(function ($query) use($estudiante) {
                $query->where('estudiantes.nombre','like','%'.$estudiante.'%')->orwhere('estudiantes.apellido','like','%'.$estudiante.'%');
            })
            ->select('boletines.*','boletines.id');

        }elseif($curso !="" and $periodo =="" and $año=="" and $estudiante!=""){
            return $query->join('estudiantes','estudiantes.id','=','boletines.estudiante_id')
            ->where(['nivel','=',$curso])
            ->Where(function ($query) use($estudiante) {
                $query->where('estudiantes.nombre','like','%'.$estudiante.'%')->orwhere('estudiantes.apellido','like','%'.$estudiante.'%');
            })->select('boletines.*','boletines.id');

        }elseif($curso !="" and $periodo =="" and $año!="" and $estudiante==""){
            return $query->join('estudiantes','estudiantes.id','=','boletines.estudiante_id')
            ->where([['nivel','=',$curso],['año_lectivo','=',$año]])
            ->select('boletines.*','boletines.id');

        }elseif($curso =="" and $periodo !="" and $año!="" and $estudiante==""){
            return $query->join('estudiantes','estudiantes.id','=','boletines.estudiante_id')
            ->where([['periodo','=',$periodo],['año_lectivo','=',$año]])
            ->select('boletines.*','boletines.id');

        }elseif($curso =="" and $periodo =="" and $año!="" and $estudiante==""){
            return $query->join('estudiantes','estudiantes.id','=','boletines.estudiante_id')
            ->where('año_lectivo',$año)
            ->select('boletines.*','boletines.id');

        }elseif($curso !="" and $periodo =="" and $año=="" and $estudiante==""){
            return $query->join('estudiantes','estudiantes.id','=','boletines.estudiante_id')
            ->where('nivel',$curso)
            ->select('boletines.*','boletines.id');

        }elseif($curso =="" and $periodo =="" and $año=="" and $estudiante!=""){
            return $query->join('estudiantes','estudiantes.id','=','boletines.estudiante_id')
            ->where('estudiantes.nombre','like','%'.$estudiante.'%')->orwhere('estudiantes.apellido','like','%'.$estudiante.'%')
            ->select('boletines.*','boletines.id');
        }else{
           

        }

    }
}
