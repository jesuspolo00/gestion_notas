<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nota extends Model
{
    protected $table = 'nota';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nota','periodo','año','curso_id','materia_id','estudiante_id',
    ];

    public function materia(){
    	return $this->belongsTo('App\Materia');
    }
    public function estudiante(){
    	return $this->belongsTo('App\Estudiante');
    }
    public function curso(){
        return $this->belongsTo('App\Curso');
    }
}
