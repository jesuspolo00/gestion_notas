<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sub_competencia extends Model
{
    protected $table = 'sub_competencias';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre','competencia_id',
    ];
    public function competencia(){
    	 return $this->belongsTo('App\Competencia');
    }
    public function materia(){
    	return $this->hasMany('App\Materia');
    }
    public static function scopeSearch($query,$buscar){
        return $query->join('competencias','sub_competencias.competencia_id','=','competencias.id')
                    ->join('sub_dimenciones','sub_dimenciones.id','=','competencias.sub_dimencion_id')
                    ->join('dimenciones', 'dimenciones.id', '=', 'sub_dimenciones.dimencion_id')                    
                    ->select('sub_competencias.*')
                    ->Where('competencias.id',$buscar);
    }
}
