<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sub_dimencion extends Model
{
    protected $table = 'sub_dimenciones';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre','dimencion_id', 
    ];
    public function competencia(){
    	 return $this->hasMany('App\Competencia');
    }
    public function dimencion(){
    	return $this->belongsTo('App\Dimencion');
    }
    public static function scopeSearch($query,$buscar){
        return $query->join('dimenciones', 'dimenciones.id', '=', 'sub_dimenciones.dimencion_id')
                    ->select('dimenciones.*', 'sub_dimenciones.id as id_sub','sub_dimenciones.nombre as nombre_sub')
                    ->Where('dimenciones.id',$buscar);
    }
}
