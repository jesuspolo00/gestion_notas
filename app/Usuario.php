<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Profesor;
class Usuario extends Authenticatable
{
    use Notifiable;
    protected $table = 'usuarios';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'usuario', 'email', 'password','rol','documento','color',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'clave', 'remember_token',
    ];

    
    public static function scopeSearch($query,$buscar){
        return $query->Where('usuarios.usuario','like',"%".$buscar."%")->orWhere('usuarios.email','like',"%".$buscar."%")->orWhere('usuarios.documento','like',"%".$buscar."%");
    }
    public function validar($role=""){

        if($this->rol == 5){
            return true;
        }else{
        $role = explode(".",$role);
            foreach ($role as $rol) {
                if($this->rol == $rol){
                    return true;
                }
            }    
            return false;        
        }
    }
    public function profesor(){
        $query = $this->join('profesores','profesores.documento','=','usuarios.documento')->where('usuarios.id',$this->id);
        return $query;
    }
    public function dimenciones_profesores(){
        $prof=$this->profesor()->get();
        $profesor= Profesor::find($prof[0]->id);
        return $profesor->dimencion()->get();
    }
    public function acudiente(){
        $query = $this->join('acudientes','acudientes.documento','=','usuarios.documento')->where('usuarios.id',$this->id);
        return $query;
    }
}
