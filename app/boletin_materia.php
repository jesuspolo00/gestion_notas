<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class boletin_materia extends Model
{
    protected $table = 'boletin_materias';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ids','nombre','periodo','boletin_sub_competencia_id',
    ];
}
