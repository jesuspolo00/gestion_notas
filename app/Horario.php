<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Dia;
use App\Hora;
use App\Horario;
use App\Colegio;
use App\Historial;
use App\Curso;
class Horario extends Model
{
    protected $table = 'horario';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'materia','periodo','año','dia_id','hora_id','curso_id',
    ];
    public function dia(){
    	return $this->belongsTo('App\Dia');
    }
    public function hora(){
    	return $this->belongsTo('App\Hora');
    }
    public function curso(){
    	return $this->belongsTo('App\Curso');
    }
    public static function scopeBuscar_Horario($query,$buscar){
        return $query->Where([['curso_id','=',$buscar->curso],['año','=',$buscar->año],['periodo','=',$buscar->periodo]]);
    }
    public static function obtener_horario($id){
        $colegio = Colegio::find(1);

        if($colegio == null){
            $año="";
            $periodo="";
        }else{
            $año = $colegio->año_escolar;
            $periodo = $colegio->periodo_escolar;
        }
        
        return Horario::Where([['curso_id','=',$id],['año','=',$año],['periodo','=',$periodo]]);
    }
    public static function Guardar($request){
        $dia = Dia::all();
        $hora = Hora::where('disponible',1)->get();
        $hora->all();
       
       foreach ($hora as $horas) {               
          
           foreach ($dia as $dias) {

                if($request[$horas->id.'-'.$dias->id] != ""){
                    $verificar = Horario::Where([['hora_id','=',$horas->id],['dia_id','=',$dias->id],['curso_id','=',$request->curso],['año','=',$request->año],['periodo','=',$request->periodo]])->get();
                    
                    if($verificar->isEmpty()){
                        $horario= new Horario();
                        $horario->materia = $request[$horas->id.'-'.$dias->id];
                        $horario->hora_id = $horas->id;
                        $horario->dia_id = $dias->id;
                        $horario->periodo = $request['periodo'];
                        $horario->curso_id = $request['curso'];
                        $horario->año = $request['año'];
                        $horario->save();
                        $c=Curso::find($request['curso']);
                        Historial::guardar('Registro','Horario','',$c->nombre);
                    }else{
                        $verificar->ToArray();
                        $horario = Horario::find($verificar[0]['id']);
                        $horario->materia=$request[$horas->id.'-'.$dias->id];
                        $horario->save();
                        $c=Curso::find($horario->curso_id);
                        Historial::guardar('Registro','Horario','',$c->nombre);
                    }
                }
                
              
           }

       }
       flash('Se guardo la configuracion exitosamente!', 'success');

             
    }
}
