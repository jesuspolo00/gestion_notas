<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class boletin_competencia extends Model
{
    protected $table = 'boletin_competencias';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ids','nombre','boletin_sub_dimencion_id', 
    ];
}
