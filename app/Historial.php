<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Historial extends Model
{
    protected $table = 'historial';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'usuario','movimiento','afectado','lugar','observacion',
    ];
    public static function scopeShearch($query,$buscar,$movimiento,$lugar){

        if($buscar!="" and $movimiento!="" and $lugar != ""){
            return $query->Where([['movimiento','=',$movimiento],['lugar','=',$lugar]])
                ->Where(function ($query) use($buscar) {
                $query->where('usuario','like','%'.$buscar.'%')->orwhere('observacion','like','%'.$buscar.'%')->orWhere('afectado','like','%'.$buscar.'%');
                });

        }elseif($buscar=="" and $movimiento!="" and $lugar != ""){
            return $query->Where([['movimiento','=',$movimiento],['lugar','=',$lugar]]);

        }elseif($buscar!="" and $movimiento!="" and $lugar == ""){
            return $query->Where('movimiento','=',$movimiento)
                ->Where(function ($query) use($buscar) {
                $query->where('usuario','like','%'.$buscar.'%')->orwhere('observacion','like','%'.$buscar.'%')->orWhere('afectado','like','%'.$buscar.'%');
                });

        }elseif($buscar!="" and $movimiento=="" and $lugar != ""){
            return $query->Where('lugar','=',$lugar)
                ->Where(function ($query) use($buscar) {
                $query->where('usuario','like','%'.$buscar.'%')->orwhere('observacion','like','%'.$buscar.'%')->orWhere('afectado','like','%'.$buscar.'%');
                });

        }elseif($buscar=="" and $movimiento=="" and $lugar != ""){
            return $query->Where('lugar','=',$lugar);

        }elseif($buscar=="" and $movimiento!="" and $lugar == ""){
            return $query->Where('movimiento','=',$movimiento);

        }else{
            return $query->OrderBy('id','desc');
        }
    }
    public static function guardar($movimiento,$lugar,$afectado="",$observacion=""){
    	$historial = new Historial();
    	$historial->usuario = Auth::User()->email;
    	$historial->movimiento = $movimiento;
    	$historial->lugar = $lugar;
    	$historial->afectado = $afectado;
    	$historial->observacion = $observacion;
    	if($historial->save()){
    		return true;
    	}else{
    		return false;
    	}
    }
}
