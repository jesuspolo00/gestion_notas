<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profesor extends Model
{
   
    protected $table = 'profesores';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tipo_documento', 'documento', 'nombre','apellido', 'correo', 'direccion', 'telefono', 'genero',
    ];

    public function dimencion(){
        return $this->belongsToMany('App\Dimencion');
    }
     public function curso(){
        return $this->belongsToMany('App\Curso');
    }
    public static function scopeSearch($query,$buscar){
        return $query->Where('nombre','like',"%".$buscar."%")->orWhere('apellido','like',"%".$buscar."%")->orWhere('documento','like',"%".$buscar."%");
    }
            
    
}
