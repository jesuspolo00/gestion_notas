<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Diploma extends Model
{
    protected $table = 'diplomas';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'estudiante_id',
    ];
    public function estudiante(){
    	return $this->belongsTo('App\Estudiante');
    }
}
