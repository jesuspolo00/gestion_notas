<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class boletin_nota extends Model
{
    protected $table = 'boletin_notas';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ids','nota','periodo','año','curso_id','materia_id','estudiante_id',
    ];

    
}
