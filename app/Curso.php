<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Estudiante;
class Curso extends Model
{
 
    protected $table = 'cursos';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre','activo','id_categoria',
    ];
    public function matricula(){
    	return $this->hasMany('App\Matricula');
    }
    public function horario(){
    	return $this->hasMany('App\Horario');
    }
    public function nota(){
        return $this->hasMany('App\Nota');
    }
    public function profesor(){
        return $this->belongsToMany('App\Profesor');
    }
    public function materia(){
        return $this->belongsToMany('App\Materia');
    }
    public function detalle(){
        return $this->hasMany('App\Detalle_curso');
    }
    
    public static function scopeSearch($query,$buscar){
        return $query->join('detalle_curso', 'cursos.id', '=', 'detalle_curso.curso_id')
                    ->select('cursos.*', 'detalle_curso.*','cursos.id as id')
                    ->Where('cursos.nombre','like',"%".$buscar."%");
    }
    public static function scopeBuscar($query,$buscar=""){
        return $query->Where('cursos.activo',$buscar);
    }
    public static function todos_estudiante($id){
        return Estudiante::join('matriculas', 'estudiantes.id', '=', 'matriculas.estudiante_id')
                ->join('cursos', 'matriculas.curso_id', '=', 'cursos.id')
                ->select('estudiantes.id','cursos.nombre','matriculas.id as matricula')
                ->where('cursos.id','=',$id)->where('matriculas.estado','=','formacion');
    }
    
}
