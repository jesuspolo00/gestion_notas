<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
class boletin_historial extends Model
{
    protected $table = 'boletin_historial';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'usuario','movimiento','curso','periodo','año','estudiante',
    ];

    public static function guardar($movimiento,$estudiante,$curso,$periodo,$año){
    	$historial = new boletin_historial();
    	$historial->usuario = Auth::User()->email;
    	$historial->movimiento = $movimiento;
    	$historial->curso = $curso;
    	$historial->periodo = $periodo;
    	$historial->año = $año;
    	$historial->estudiante = $estudiante;
    	if($historial->save()){
    		return true;
    	}else{
    		return false;
    	}
    }
}
