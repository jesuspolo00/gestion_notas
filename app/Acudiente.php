<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Acudiente extends Model
{
    
    protected $table = 'acudientes';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tipo_documento', 'documento', 'nombre','apellido', 'correo', 'direccion', 'telefono', 'genero',
    ];

    public function estudiante(){
    	return $this->belongsToMany('App\Estudiante');
    }
    public function prueba(){
        return $this->join('acudiente_estudiante','acudiente.id','=','acudiente_estudiante.acudiente_id');
    }
    public static function scopeSearch($query,$buscar){
        return $query->where('documento',$buscar);
    }
}
