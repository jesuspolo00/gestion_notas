<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Estudiante;
use App\Colegio;
class pagos extends Model
{
    protected $table = 'pagos';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'mes','año','pago','estudiante_id',
    ];
    public function estudiante(){
    	return $this->belongsTo('App\Estudiantes');
    }

    public static function validar_pagos($id){

        $periodo1 = ['agosto','septiembre','octubre'];
        $periodo2=['noviembre','diciembre','enero','febrero'];
        $periodo3=['marzo','abril','mayo','junio'];
        $estudiante=Estudiante::find($id);
        $colegio = Colegio::find(1);
        $validar=['periodo1'=>0,'periodo2'=>0,'periodo3'=>0];
        if($colegio){
        $procesar=$estudiante->pagos()->where('año',$colegio->año_escolar)->get();
        $intervalo=0;
            foreach ($periodo1 as $meses) {
                foreach ($procesar as $proceso) {
                    if($proceso->mes==$meses){
                        $intervalo++;
                    }
                }
                if($intervalo == count($periodo1)){
                    $validar['periodo1']=1;
                    $intervalo=0;
                }
            }
            foreach ($periodo2 as $meses) {
                foreach ($procesar as $proceso) {
                    if($proceso->mes==$meses){
                        $intervalo++;
                    }
                }
                if($intervalo == count($periodo1)){
                    $validar['periodo2']=1;
                    $intervalo=0;
                }
            }
            foreach ($periodo3 as $meses) {
                foreach ($procesar as $proceso) {
                    if($proceso->mes==$meses){
                        $intervalo++;
                    }
                }
                if($intervalo == count($periodo1)){
                    $validar['periodo3']=1;
                    $intervalo=0;
                }
            }
            return $validar;
        }

    }
}
