<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Materia extends Model
{
    protected $table = 'materias';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre','periodo','sub_competencia_id',
    ];
     
    public function sub_competencia(){
    	return $this->belongsTo('App\Sub_competencia');
    }
    public function nota(){
    	return $this->hasMany('App\Nota');
    }
    public function curso(){
        return $this->belongsToMany('App\Curso');
    }
    public static function scopeCalificacion($query,$materia,$id){
        return $query->join('nombre_nota','materias.id','=','nombre_nota.materia_id')                     
                     ->join('nota','nombre_nota.id','=','nota.nombre_id')
                     ->join('estudiantes','estudiantes.id','=','nota.estudiante_id')
                     ->join('cursos','cursos.id','=','nombre_nota.curso_id')
                     ->select('estudiantes.*','nombre_nota.nombre','nota.*','materias.*')
                     ->where([['materias.id','=',$materia],['cursos.id','=',$id]]);
    }
    public static function scopeSearch($query,$buscar){
        return $query->join('sub_competencias','sub_competencias.id','=','materias.sub_competencia_id')
                    ->join('competencias','sub_competencias.competencia_id','=','competencias.id')
                    ->join('sub_dimenciones','sub_dimenciones.id','=','competencias.sub_dimencion_id')
                    ->join('dimenciones', 'dimenciones.id', '=', 'sub_dimenciones.dimencion_id')                    
                    ->select('materias.*')
                    ->Where('sub_competencias.id',$buscar);
    }
}
