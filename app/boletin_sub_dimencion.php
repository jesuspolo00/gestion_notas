<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class boletin_sub_dimencion extends Model
{
    protected $table = 'boletin_sub_dimenciones';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ids','nombre','boletin_dimencion_id', 
    ];
}
