<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hora extends Model
{
   protected $table = 'horas';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'hora','disponible',
    ];
    public function horario(){
    	return $this->hasMany('App\Horario');
    }
    
}
