<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class boletin_dimencion extends Model
{
    protected $table = 'boletin_dimenciones';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ids', 'nombre', 'nivel',
    ];
    public static function boletin_materias_boletin($id, $periodo, $estudiante)
    {
        DB::statement('SET SQL_BIG_SELECTS =1');
        return boletin_dimencion::join('boletin_sub_dimenciones', 'boletin_dimenciones.ids', '=', 'boletin_sub_dimenciones.boletin_dimencion_id')
            ->join('boletin_competencias', 'boletin_sub_dimenciones.ids', '=', 'boletin_competencias.boletin_sub_dimencion_id')
            ->join('boletin_sub_competencias', 'boletin_competencias.ids', '=', 'boletin_sub_competencias.boletin_competencia_id')
            ->join('boletin_materias', 'boletin_sub_competencias.ids', '=', 'boletin_materias.boletin_sub_competencia_id')
            ->join('boletin_notas', 'boletin_materias.ids', '=', 'boletin_notas.materia_id')
            ->select('boletin_materias.*', 'boletin_dimenciones.nombre as dimencion', 'boletin_sub_dimenciones.nombre as sub_dimencion', 'boletin_competencias.nombre as competencia', 'boletin_sub_competencias.nombre as sub_competencia')
            ->where([['boletin_dimenciones.ids', $id], ['boletin_materias.periodo', '=', $periodo], ['boletin_notas.estudiante_id', '=', $estudiante]]);
    }
    public static function boletin_subcompetencias_boletin($id, $periodo, $estudiante)
    {
        return boletin_dimencion::join('boletin_sub_dimenciones', 'boletin_dimenciones.ids', '=', 'boletin_sub_dimenciones.boletin_dimencion_id')
            ->join('boletin_competencias', 'boletin_sub_dimenciones.ids', '=', 'boletin_competencias.boletin_sub_dimencion_id')
            ->join('boletin_sub_competencias', 'boletin_competencias.ids', '=', 'boletin_sub_competencias.boletin_competencia_id')
            ->join('boletin_materias', 'boletin_sub_competencias.ids', '=', 'boletin_materias.boletin_sub_competencia_id')
            ->join('boletin_notas', 'boletin_materias.ids', '=', 'boletin_notas.materia_id')
            ->select('boletin_sub_competencias.*')
            ->where([['boletin_dimenciones.ids', $id], ['boletin_materias.periodo', '=', $periodo], ['boletin_notas.estudiante_id', '=', $estudiante]])
            ->groupBy('boletin_sub_competencias.nombre');
    }
    public static function boletin_competencias_boletin($id)
    {
        return boletin_dimencion::join('boletin_sub_dimenciones', 'boletin_dimenciones.ids', '=', 'boletin_sub_dimenciones.boletin_dimencion_id')
            ->join('boletin_competencias', 'boletin_sub_dimenciones.ids', '=', 'boletin_competencias.boletin_sub_dimencion_id')
            ->select('boletin_competencias.*')
            ->where('boletin_dimenciones.ids', $id);
    }
    public static function boletin_subdimenciones_boletin($id)
    {
        return boletin_dimencion::join('boletin_sub_dimenciones', 'boletin_dimenciones.ids', '=', 'boletin_sub_dimenciones.boletin_dimencion_id')
            ->select('boletin_sub_dimenciones.*')
            ->where('boletin_dimenciones.ids', $id);
    }
    public static function contar($id, $periodo)
    {
        $materia = boletin_competencia::join('boletin_sub_competencias', 'boletin_competencias.ids', '=', 'boletin_sub_competencias.boletin_competencia_id')->join('boletin_materias', 'boletin_sub_competencias.ids', '=', 'boletin_materias.boletin_sub_competencia_id')
            ->where([['boletin_competencias.ids', '=', $id], ['boletin_materias.periodo', '=', $periodo]]);

        $subc = boletin_competencia::join('boletin_sub_competencias', 'boletin_competencias.ids', '=', 'boletin_sub_competencias.boletin_competencia_id')->where('boletin_competencias.ids', $id);
        return $materia->count() + $subc->count() + 1;
    }
}
