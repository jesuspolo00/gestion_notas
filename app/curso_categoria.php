<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class curso_categoria extends Model
{
    protected $table = 'curso_categoria';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 
    ];
}
