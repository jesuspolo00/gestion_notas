<?php session_start();
 $_SESSION['menu']=2;
 $_SESSION['active']="registrar";  
 ?>

@extends('plantilla.estructura')

@section('title','inicio')
@section('pagina','boletin')
@section('subtitulo','Registrar Acudiente')
@section('script')
	
@endsection
@section('body')
	<div class="row"> 
	  <div class="col-xs-12" style="margin-bottom: 10px;">
	    <div class="dataTables_filter" id="dynamic-table_fil">
	      <form method="GET" action="{{ route('acudiente.registrar',$estudiante) }}" class="form-inline">
	         <div class="form-group">
	          <label style="" class="control-label" style="margin-bottom: 0;vertical-align: middle;">Buscar</label>
	          <input class="form-control" name="listar" style="display: inline-block;width: auto;vertical-align: middle;" type="text" placeholder="Documento">
	         <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>

	         @if($acudiente->isEmpty())
	         <p>Nota: Para seleccionar un padre ya registrado, buscar por el documento de identidad</p>
	         @endif
	         </div>
	      </form>
	      </div>
	      @if($acudiente->isNotEmpty())
	      <table class="table" style="margin-top: 20px;">
	      		<thead>
	      			<tr>
	      				<th>Nombre</th>
	      				<th>Documento</th>
	      				<th>Opciones</th>
	      			</tr>
	      		</thead>
	      		<tbody>
	      			@foreach($acudiente as $acu)
	      				<form method="POST" action="{{ route('acudiente.seleccionar',[$estudiante,$acu->id])}}">
	      					{{ csrf_field() }}
		      			<tr>
		      				<td>{{ $acu->nombre }} {{ $acu->apellido }}</td>
		      				<td>{{ $acu->documento }}</td>
		      				<td><button type="submit" class="btn btn-primary">Seleccionar</button></td>
		      			</tr>
		      			</form>
	      			@endforeach
	      		</tbody>
	      </table>
	      @endif
	    </div>
	 </div>
	<div class="row" style="margin-top: 20px;">
		<center><h3>Registrar Acudiente</h3></center>
		<form class="form-horizontal" action="{{ route('acudiente.store')}}" method="POST">
		{{ csrf_field() }}
		<input type="hidden" class="hidden" name="e" value="{{ $estudiante }}">
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Nombre</label>
		    <div class="col-sm-8 col-lg-5">
		      <input type="text" class="form-control" name="nombre" placeholder="Nombre" required>
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Apellido</label>
		    <div class="col-sm-8 col-lg-5">
		      <input type="text" class="form-control" name="apellido" placeholder="Apellido" required>
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Tipo Documento</label>
		    <div class="col-sm-8 col-lg-5">
		      	<select class="form-control" name="tdocumento" required>
				  <option value="1">C.C</option>
				  <option value="2" selected>T.I</option>
				  <option value="3">R.C</option>
				</select>
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Documento</label>
		    <div class="col-sm-8 col-lg-5">
		      <input type="number" class="form-control" name="documento" maxlength="20" placeholder="Documento" required>
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Correo</label>
		    <div class="col-sm-8 col-lg-5">
		      <input type="email" class="form-control" name="correo" placeholder="Correo" required>
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Direccion</label>
		    <div class="col-sm-8 col-lg-5">
		      <input type="text" class="form-control" name="direccion" placeholder="Direccion" required>
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Telefono</label>
		    <div class="col-sm-8 col-lg-5">
		      <input type="number" class="form-control" name="telefono" placeholder="Telefono" required>
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Genero</label>
		    <div class="col-sm-8 col-lg-5">
		      <select class="form-control" name="genero" required>
				  <option value="1">Masculino</option>
				  <option value="2">Femenino</option>				  
				</select> 
		    </div>
		  </div>
		  <div class="form-group">
		    <div class="col-sm-offset-3 col-sm-9 col-lg-6">
		      <button type="submit" class="btn btn-default">Registrar</button>
		    </div>
		  </div>
		</form>
	</div>
@endsection