	<?php session_start();
 $_SESSION['menu']=2;
 $_SESSION['active']="buscar_estudiante"; 
 ?>

@extends('plantilla.estructura')

@section('title','inicio')
@section('pagina','boletin')
@section('subtitulo','Editar Acudiente')
@section('script')
	
@endsection
@section('body')
	<div class="row"> 
	  <div class="col-xs-12" style="margin-bottom: 10px;">
	    <div class="dataTables_filter" id="dynamic-table_fil">
	      <form method="GET" action="{{ route('estudiantes.acudiente',$estudiante->id) }}" class="form-inline">
	         <div class="form-group">
	          <label style="" class="control-label" style="margin-bottom: 0;vertical-align: middle;">Buscar</label>
	          <input class="form-control" name="listar" style="display: inline-block;width: auto;vertical-align: middle;" type="text" placeholder="Documento">
	         <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>

	         @if($acudi->isEmpty())
	         <p>Nota: seleccione un acudiente ya registrado, se reemplazara el actual acudiente</p>
	         @endif
	         </div>
	      </form>
	      </div>
	      @if($acudi->isNotEmpty())
	      <table class="table" style="margin-top: 20px;">
	      		<thead>
	      			<tr>
	      				<th>Nombre</th>
	      				<th>Documento</th>
	      				<th>Opciones</th>
	      			</tr>
	      		</thead>
	      		<tbody>
	      			@foreach($acudi as $acu)
	      				<form method="POST" action="{{ route('acudiente.seleccion',[$estudiante->id,$acu->id])}}">
	      					{{ csrf_field() }}
		      			<tr>
		      				<td>{{ $acu->nombre }} {{ $acu->apellido }}</td>
		      				<td>{{ $acu->documento }}</td>
		      				<td><button type="submit" class="btn btn-primary">Seleccionar</button></td>
		      			</tr>
		      		</form>
	      			@endforeach
	      		</tbody>
	      </table>
	      @endif
	    </div>
	 </div>

	<div class="row" style="margin-top: 20px;">
		<center><h3>Editar Acudiente</h3></center>
		@if($acudiente->isNotEmpty())
		<form class="form-horizontal" action="{{ route('acudiente.update',[$estudiante->id,$acudiente[0]->id])}}" method="POST">
		@else
		<form class="form-horizontal" action="{{ route('acudiente.store')}}" method="POST">
		@endif
		{{ csrf_field() }}
		<input type="hidden" class="hidden" name="e" value="{{ $estudiante->id }}">
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Nombre</label>
		    <div class="col-sm-8 col-lg-5">
		      <input type="text" class="form-control" name="nombre" value="<?php if($acudiente->isNotEmpty()){ echo $acudiente[0]->nombre; } ?>" placeholder="Nombre" required>
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Apellido</label>
		    <div class="col-sm-8 col-lg-5">
		      <input type="text" class="form-control" name="apellido" value="<?php if($acudiente->isNotEmpty()){ echo $acudiente[0]->apellido; } ?>" placeholder="Apellido" required>
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Tipo Documento</label>
		    <div class="col-sm-8 col-lg-5">
		      	<select class="form-control" name="tdocumento" required>
		      	@if($acudiente->isNotEmpty())
			      	@if($estudiante->tipo_documento == 1)
			      		<option value="2" selected>T.I</option>
			      		<option value="1" selected>C.C</option>
			      		<option value="3" selected>R.C</optio>
			      	@elseif($estudiante->tipo_documento == 2)
			      		<option value="2" selected>T.I</option>
			      		<option value="1">C.C</option>
			      		<option value="3" selected>R.C</optio>
			      	@elseif($estudiante->tipo_documento == 3)
			      		<option value="2">T.I</option>
						 <option value="1">C.C</option>
			      		<option value="3" selected>R.C</option>
			      	@else
			      		<option value="2">T.I</option>
						  <option value="1">C.C</option>				  
						  <option value="3">R.C</option>
			      	@endif
			     @else
		      	  <option value="2">T.I</option>
				  <option value="1">C.C</option>				  
				  <option value="3">R.C</option>
				 @endif
				</select>
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Documento</label>
		    <div class="col-sm-8 col-lg-5">
		      <input type="number" class="form-control" name="documento" value="<?php if($acudiente->isNotEmpty()){ echo $acudiente[0]->documento; } ?>" maxlength="20" placeholder="Documento" required>
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Correo</label>
		    <div class="col-sm-8 col-lg-5">
		      <input type="email" class="form-control" name="correo" value="<?php if($acudiente->isNotEmpty()){ echo $acudiente[0]->correo; } ?>" placeholder="Correo" required>
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Direccion</label>
		    <div class="col-sm-8 col-lg-5">
		      <input type="text" class="form-control" name="direccion" value="<?php if($acudiente->isNotEmpty()){ echo $acudiente[0]->direccion; } ?>" placeholder="Direccion" required>
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Telefono</label>
		    <div class="col-sm-8 col-lg-5">
		      <input type="number" class="form-control" name="telefono" value="<?php if($acudiente->isNotEmpty()){ echo $acudiente[0]->telefono; } ?>" placeholder="Telefono" required>
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Genero</label>
		    <div class="col-sm-8 col-lg-5">
		      <select class="form-control" name="genero" required>
		      	@if($acudiente->isNotEmpty())
		      		@if($estudiante->genero == 1)
		      		<option value="1" selected="">Masculino</option>
		      		<option value="2">Femenino</option>
		      		@elseif($estudiante->genero == 2)
		      		<option value="1">Masculino</option>
		      		<option value="2" selected="">Femenino</option>
		      		@else
				  <option value="1">Masculino</option>
				  <option value="2">Femenino</option>
				  @endif	
				 @else
				 	<option value="1">Masculino</option>
				  <option value="2">Femenino</option>
				 @endif			  
				</select>  
		    </div>
		  </div>
		  <div class="form-group">
		    <div class="col-sm-offset-3 col-sm-9 col-lg-6">
		      <button type="submit" class="btn btn-default">Guardar</button>
		    </div>
		  </div>
		</form>
	</div>
@endsection