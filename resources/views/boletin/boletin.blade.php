<?php
use App\boletin_dimencion;
?>
<style type="text/css">
	html {
		margin:0;
		margin-top: 29px;
		margin-bottom: 29px;
		margin-left: 48px;
		margin-right: 48px;
		font-family: 'Century Gothic';
	}
	body{
		border: 2px solid  #99a3a4  ;
		padding-top: 65px; 
		padding-bottom: 30px;
	}
	.letra{
		font-size:12px;
		color: #000;
	}
	table{
		border-spacing:0;border-collapse:collapse;
		page-break-inside:auto
	}
	td{
		margin:0;
		padding: 0;
		height: 22px;
		border: 1px solid #b2babb;
	}
	tr{
		page-break-inside:avoid; page-break-after:auto;
	}

	
	

</style>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>boletin</title>
</head>
<body>
	<?php $paginacion_inicial=33;
	$paginacion=36;
	?>
	<header style="position: fixed;top: 0;">
		<div style="width: 95%; margin:auto;">			
			<table border="1" style="height: 50px;width: 100%; margin-top: 5px;">
				<tr>
					<td style="width: 30%;"><img src="{{ asset('images/slider2.png') }}" style="margin-left:20%; width: 130px;"></td>
					<td class="letra" style="width: 40%; background-color: #99a3a4; color:#000; border: 1px solid #b2babb;"><p align="center"><strong>JARDIN INFANTIL PLAY AND LEARN <br>INFORME EVALUATIVO</p></strong></td>
					<td class="letra" style="width: 30%; text-align: center; border: 1px solid #b2babb; background-color: #99a3a4; color:#000;">
						<label><strong>CODIGO: {{ $boletin[0]->codigo }}</strong></label>
						<div style="border: 1px solid #b2babb; background-color: #b2babb; color:#000;"></div>
						<label><strong>AÑO LECTIVO {{ $boletin[0]->año_lectivo }}</strong></label>
					</td>
				</tr>
			</table>
		</div>
	</header>
	<div style="width: 95%; margin:auto;">
		<div style="width: 90%; margin:auto;">
			<div class="letra" style="width: 70%;display: inline-block; color:#000;">
				<p><strong>NOMBRE DEL ALUMNO: <u>{{ $boletin[0]->nombre }}</u></strong></p>
			</div>
			<div class="letra" style="width: 30%;display: inline-block; color:#000;">
				<p><strong>NIVEL: <u>{{ $boletin[0]->nivel }}</u></strong></p>
			</div>
			<div class="letra" style="width: 100%;">
				<div style="width: 50%;display: inline-block; color:#000;">
					<p><strong>PERIODO: <u>{{ $boletin[0]->periodo }}</u></strong></p>
				</div>
				<div style="width: 20%;display: inline-block; color:#000;">
					<p><strong>FECHA: <u>{{ $boletin[0]->fecha }}</u></strong></p>
				</div>
				<div style="width: 20%;display: inline-block; color:#000;">
					<p><strong>AUSENCIA: <u>{{ $boletin[0]->ausencia }}</u></strong></p>
				</div>
			</div>
		</div>
		<header style=" bottom:0; padding-top: -30px; font-size:0.6em; position: fixed;">
			<div style="width: 90%; margin:auto;">	
				<div style="width: 20%;display: inline-block;">
					<p>LA: logro alcanzado</p>
				</div>
				<div style="width: 20%;display: inline-block;">
					<p>LD: logro en desarrollo</p>
				</div>
				<div style="width: 30%;display: inline-block;">
					<p>NE: necesita estimulo</p>
				</div>
				<div style="width: 20%;display: inline-block;">
					<p>{{ $boletin[0]->nivel }}</p>
				</div>
			</div>
		</header>
		<?php $salto=0;
		$mirar=0;
		$conteo_competencia=0;
		?>
		<table class="letra" >
			@foreach($dimencion as $dimenciones)	
			<?php 
			$materia_boletin = boletin_dimencion::boletin_materias_boletin($dimenciones->ids,$periodo, $boletin[0]->estudiante_id)->get();	    				
			$subcompetencia = boletin_dimencion::boletin_subcompetencias_boletin($dimenciones->ids,$periodo, $boletin[0]->estudiante_id)->get();
			$competencia = boletin_dimencion::boletin_competencias_boletin($dimenciones->ids)->get();
			$subdimencion = boletin_dimencion::boletin_subdimenciones_boletin($dimenciones->ids)->get();

			?>
			@if($materia_boletin->isNotEmpty())
			<tr style="margin-top: 5%;">
				<th colspan="5" style="background-color: #99a3a4;"><center>{{ $dimenciones->nombre }}</center></th>
			</tr>

			@foreach($subdimencion as $subdimenciones)
			<tr>

				<td style="width: 25%; font-weight: bolder; background-color:   #ccd1d1 
				; text-align: center;">{{$subdimenciones->nombre}}</td>
				<td style="width: 60%; font-weight: bolder; background-color:   #ccd1d1 
				; text-align: center;">Aspecto a evaluar</td>
				<td style="width: 5%; font-weight: bolder; background-color:   #ccd1d1 
				; text-align: center;">LA</td>
				<td style="width: 5%; font-weight: bolder; background-color:   #ccd1d1 
				; text-align: center;">LD</td>
				<td style="width: 5%; font-weight: bolder; background-color:   #ccd1d1 
				; text-align: center;">NE</td>
			</tr>

			@foreach($competencia as $competencias)
			<?php $salto=0; ?>
			@if($competencias->boletin_sub_dimencion_id == $subdimenciones->ids)
			<?php
			$row=boletin_dimencion::contar($competencias->ids,$periodo);
			$ultimo=$row;
			$row=$row/2;
			?>

			@foreach($subcompetencia as $subcompetencias)
			@if($subcompetencias->boletin_competencia_id == $competencias->ids)
			<tr>
				<?php $salto++;?>

				@if(intval($row)==$salto)
				<td style="text-align: center; font-weight: bolder;  border:none; border-left: 1px solid #ccd1d1;">{{$competencias->nombre}} </td>
				@else

				@if($salto==1)
				<td style="text-align: center;  border:none; border-left: 1px solid #ccd1d1;border-top: 1px solid #ccd1d1;"></td>
				@else
				<td style="text-align: center;  border:none; border-left: 1px solid #ccd1d1;"></td>
				@endif
				@endif

				<td colspan="4" style="font-weight: bolder;  background-color:   #ccd1d1 
				;">{{$subcompetencias->nombre}}</td>
			</tr>
			@foreach($materia_boletin as $mate)
			@if($mate->boletin_sub_competencia_id == $subcompetencias->ids)
			<tr>			
				<?php $salto++;?>
				@if(intval($row)==$salto)
				<td style="text-align: center; font-weight: bolder; border:none;  padding: 5px; border-left: 1px solid #ccd1d1;">{{$competencias->nombre}}</td>
				@else
				<td style="text-align: center;  padding: 5px; border:none; border-left: 1px solid #ccd1d1;"></td>
				@endif
				<td style="width: 60%;">{{$mate->nombre}}</td>
				<?php $verificar=0; ?>
				@foreach($nota as $notas)
				@if($notas->materia_id == $mate->ids)
				@for($i=1;$i<=3;$i++)		    						
				@if($notas->nota == $i)
				<td style="width: 5%; text-align:center; padding: 4px; " class="letra"><span style="font-weight: bolder; text-align:center;">X</span></td>
				@else
				<td style="width: 5%;" style="text-align: center;"></td>
				@endif
				@endfor
				<?php $verificar=1; ?>
				@else
				@continue
				@endif
				@endforeach
				@if($verificar==0)
				@for($i=1;$i<=3;$i++)
				<td style="width: 5%;" style="text-align: center;"></td>
				@endfor
				@endif
			</tr>

			@else
			@continue
			@endif
			@endforeach
			@else
			@continue
			@endif
			@endforeach
			@else
			@continue
			@endif
			@endforeach
			<?php $conteo_competencia=0;?>
			@endforeach

			@endif
			@endforeach
		</table>
	</div>
	<div style="width: 100%; margin-top: 0.5%; margin-left: auto; margin-bottom: auto; margin-right: auto;" class="letra">
		<center><span><b>OBSERVACIONES GENERALES</b></span></center>
		<br>
		<div style="margin:auto; width: 95%; text-align:justify;">
			<span style="font-weight: normal;">@if($boletin[0]->observacion !=""){{ $boletin[0]->observacion }}@else &nbsp; @endif</span>
		</div>
		<div style="width: 50%; margin:auto; margin-top: 10%;">
			<div style="float: left;">______________________<br>&nbsp;&nbsp;&nbsp;FIRMA DIRECTORA</div>
			<div style="float: right;">______________________<br>&nbsp;&nbsp;&nbsp;FIRMA PROFESORA</div>
		</div>
	</div>

	<header style=" bottom:0; padding-top: -30px; font-size:0.6em; position: fixed;">
		<div style="width: 90%; margin:auto;">	
			<div style="width: 20%;display: inline-block;">
				<p>LA: logro alcanzado</p>
			</div>
			<div style="width: 20%;display: inline-block;">
				<p>LD: logro en desarrollo</p>
			</div>
			<div style="width: 30%;display: inline-block;">
				<p>NE: necesita estimulo</p>
			</div>
			<div style="width: 20%;display: inline-block;">
				<p>{{ $boletin[0]->nivel }}</p>
			</div>
		</div>
	</header>

</body>
</html>