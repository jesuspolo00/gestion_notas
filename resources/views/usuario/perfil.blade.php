<?php session_start();
 $_SESSION['menu']=1;
 $_SESSION['active']="";  
 ?>

@extends('plantilla.estructura')
@section('head')
@endsection
@section('title','Perfil')
@section('pagina','boletin')
@section('subtitulo','Perfil de usuario')
@section('body')
		<div class="container-fluid">
<div class="col-xs-12" style="margin-bottom: 5px;">
	<div class="col-xs-12">
    <div class="row">
    	@if(Auth::User()->rol =="1" or Auth::User()->rol =="2")
                <center><h3>Modificar Datos Del Perfil</h3></center>
        <div class="col-xs-12">
        	 <form class="form-horizontal" role="form" method="POST" action="{{ route('usuario.datos_actualizar') }}">
                 {{ csrf_field() }}
                 <div class="form-group">
                    <label for="password" class="col-md-3 control-label">Usuario</label>

                    <div class="col-md-5">
                        <input type="text"  class="form-control" placeholder="Usuario" name="usuario" required value="{{ $usuario[0]->usuario }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="col-md-3 control-label">Documento</label>

                    <div class="col-md-5">
                        <input  type="text"  class="form-control" placeholder="Documento" disabled="" value="{{ $usuario[0]->documento }}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="password-confirm" class="col-md-3 control-label">Correo Electronico</label>
                    <div class="col-md-5">
                        <input type="email" class="form-control" value="{{ $usuario[0]->email }}"  placeholder="Correo Electronico" disabled="">
                    </div>
                </div>
                <div class="form-group">
		    <label class="col-sm-3 control-label">Nombre</label>
		    <div class="col-sm-8 col-lg-5">
		      <input type="text" class="form-control" name="nombre" value="{{ $usuario[0]->nombre }}" placeholder="Nombre" required>
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Apellido</label>
		    <div class="col-sm-8 col-lg-5">
		      <input type="text" class="form-control" name="apellido" value="{{ $usuario[0]->apellido }}" placeholder="Apellido" required>
		    </div>
		  </div>
		  
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Correo Personal</label>
		    <div class="col-sm-8 col-lg-5">
		      <input type="email" class="form-control" name="correo" value="{{ $usuario[0]->correo }}" placeholder="Correo" required>
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Direccion</label>
		    <div class="col-sm-8 col-lg-5">
		      <input type="text" class="form-control" name="direccion" value="{{ $usuario[0]->direccion }}" placeholder="Direccion" required>
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Telefono</label>
		    <div class="col-sm-8 col-lg-5">
		      <input type="number" class="form-control" name="telefono" value="{{ $usuario[0]->telefono }}" placeholder="Telefono" required>
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Genero</label>
		    <div class="col-sm-8 col-lg-5">
		      <select class="form-control" name="genero" required>
		      	@if($usuario->isNotEmpty())
		      		@if($usuario[0]->genero == 1)
		      		<option value="1" selected="">Masculino</option>
		      		<option value="2" >Femenino</option>
		      		@elseif($usuario[0]->genero == 2)
		      		<option value="1">Masculino</option>
		      		<option value="2" selected="">Femenino</option>
		      		@else
				  <option value="1">Masculino</option>
				  <option value="2">Femenino</option>
				  @endif	
				 @else
				 	<option value="1">Masculino</option>
				  <option value="2">Femenino</option>
				 @endif			  
				</select> 
		    </div>
		  </div>
		  <div class="col-md-offset-3">
		  <button type="submit" class="btn btn-primary">Guardar</button>
		</div>
            </form>
        <hr>
        <br>
        </div>
        @endif
        <div class="col-xs-12">
        	<center><h3>Modificar Contraseña</h3></center>
            <form class="form-horizontal" role="form" method="POST" action="{{ route('usuario.perfil_actualizar') }}">
                 {{ csrf_field() }}
                 <div class="form-group">
                    <label for="password" class="col-md-3 control-label">Contraseña Actual</label>

                    <div class="col-md-8">
                        <input type="password"  class="form-control" placeholder="Contraseña Actual" name="actual" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="col-md-3 control-label">Nueva Contraseña</label>

                    <div class="col-md-8">
                        <input id="password" type="password"  class="form-control" placeholder="Nueva Contraseña" name="password" required>
                    </div>
                </div>

                <div class="form-group">
                    <label for="password-confirm" class="col-md-3 control-label">Confirmar Contraseña</label>
                    <div class="col-md-8">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirmar Contraseña" required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-8 col-md-offset-3">
                        <button type="submit" class="btn btn-primary">
                            Actualizar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
	
	<div class="col-xs-12" style="border-top: 1px solid #ddd;">
		<form class="form-inline" style="margin-top: 15px;">
			<center>
				<h2>Cambiar Diseño</h2>
				<br>
			<div class="form-group">
			<section id="smart-styles">
				<a style="background-color:#23262F;" class="btn txt-color-white" id="dark_theme" href="javascript:void(0);">
		        <i id="skin-checked" class="fa fa-check fa-fw"></i> Dark Theme</a>
		        
		      <a style="background:#E35154;" class="btn txt-color-white" id="red_thm" href="javascript:void(0);">Red Theme</a>

		      <a style="background:#34B077;" class="btn txt-color-darken" id="green_thm" href="javascript:void(0);">Green Theme</a>

		      <a style="background:#56A5DB" class="btn txt-color-white" data-skinlogo="img/logo-pale.png" id="blue_thm" href="javascript:void(0);">Blue Theme</a>

		      <a style="background:#9C6BAD" class="btn txt-color-white" id="magento_thm" href="javascript:void(0);">Magento Theme</a>

		      <a style="background:#FFFFFF; border: 1px solid" class="btn txt-color-black" id="light_theme" href="javascript:void(0);">Light Theme</a>
		  </section>
		</div>
		  </center>	
		  <div class="form-group" style="float: right; margin-bottom: 10px;">
		  	<button type="button" class="btn btn-primary" id="guardar">Guardar</button>
		  </div>		
		</form>
	</div>
</div>
</div>
@endsection
@section('script')
	<script type="text/javascript">
		$( "#guardar" ).click(function () {
		     $.ajaxSetup({
		          headers: {
		          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		          }
		       });
		     var clases = $('body').attr('class');
		      $.ajax({
		          url: 'perfil/color/'+clases,
		          type: 'GET',                          
		          success: function (data) {
	          		if(data){
	          			alert('Diseño Guardado');
	          		}else{
	          			alert('Error al Actualizar el diseño');
	          		}	
		          }            
		      });
		   
		  })
	</script>
@endsection