<?php session_start();
 $_SESSION['menu']=7;
 $_SESSION['active']="usuarios";  
 ?>

@extends('plantilla.estructura')
@section('head')
@endsection
@section('title','inicio')
@section('pagina','boletin')
@section('subtitulo','Registrar Usuario')
@section('script')
@endsection
@section('body')
<div class="container">
    <div class="row">
                <center><h3>Editar Usuario</h3></center>
        <div class="col-xs-7" style="border-right: 1px solid #ddd;">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('usuario.update',$usuario->id) }}">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="name" class="col-md-3 control-label">Usuario</label>

                            <div class="col-md-9">
                                <input id="name" type="text" class="form-control" placeholder="Nombre de Usuario" name="name" value="{{ $usuario->usuario }}" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-md-3 control-label">Correo Electronico</label>

                            <div class="col-md-9">
                                <input id="email" type="email" class="form-control" placeholder="Correo Electronico" name="email" value="{{ $usuario->email }}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Documento</label>

                            <div class="col-md-9">
                                <input type="number" class="form-control" placeholder="Documento" name="documento" value="{{ $usuario->documento }}" required>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="password-confirm" class="col-md-3 control-label">Tipo</label>

                            <div class="col-md-9">
                                <select class="form-control" name="rol">
                                    @if($usuario->rol==1)
                                      <option value="1" selected="">Padre</option>
                                      <option value="2">Docente</option>
                                      <option value="3">Recaudo</option>
                                      <option value="4">Secretaria</option>
                                      <option value="5">Administrador</option>
                                    @elseif($usuario->rol==2)
                                        <option value="1">Padre</option>
                                      <option value="2" selected="">Docente</option>
                                      <option value="3">Recaudo</option>
                                      <option value="4">Secretaria</option>
                                      <option value="5">Administrador</option>
                                    @elseif($usuario->rol==3)
                                    <option value="1">Padre</option>
                                      <option value="2">Docente</option>
                                      <option value="3" selected="">Recaudo</option>
                                      <option value="4">Secretaria</option>
                                      <option value="5">Administrador</option>
                                    @elseif($usuario->rol==4)
                                    <option value="1">Padre</option>
                                      <option value="2">Docente</option>
                                      <option value="3">Recaudo</option>
                                      <option value="4" selected="">Secretaria</option>
                                      <option value="5">Administrador</option>
                                    @elseif($usuario->rol==5)
                                    <option value="1">Padre</option>
                                      <option value="2">Docente</option>
                                      <option value="3">Recaudo</option>
                                      <option value="4">Secretaria</option>
                                      <option value="5" selected="">Administrador</option>
                                    @else
                                    <option value="1">Padre</option>
                                      <option value="2">Docente</option>
                                      <option value="3">Recaudo</option>
                                      <option value="4">Secretaria</option>
                                      <option value="5">Administrador</option>
                                    @endif                        
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-primary">
                                    Guardar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-xs-5">
            <form class="form-horizontal" role="form" method="POST" action="{{ route('usuario.confirmar',$usuario->id) }}">
                 {{ csrf_field() }}
                <div class="form-group">
                    <label for="password" class="col-md-3 control-label">Contraseña</label>

                    <div class="col-md-8">
                        <input id="password" type="password"  class="form-control" placeholder="Contraseña" name="password" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="password-confirm" class="col-md-3 control-label">Confirmar Contraseña</label>
                    <div class="col-md-8">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirmar Contraseña" required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-8 col-md-offset-3">
                        <button type="submit" class="btn btn-primary">
                            Actualizar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection