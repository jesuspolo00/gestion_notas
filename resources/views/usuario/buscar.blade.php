<?php session_start();
 $_SESSION['menu']=7;
 $_SESSION['active']="usuarios";   
 ?>

@extends('plantilla.estructura')

@section('title','inicio')
@section('pagina','boletin')
@section('subtitulo','Buscar usuarios')
@section('script')
	<script src="{{ asset('plugins/data-tables/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('plugins/data-tables/DT_bootstrap.js') }}"></script>
	<script src="{{ asset('plugins/data-tables/dynamic_table_init.js') }}"></script>
	<script src="{{ asset('plugins/edit-table/edit-table.js') }}"></script>
  
@endsection
@section('body')
  
	<div class="row">    
  <div class="col-xs-12" style="margin-bottom: 10px;">
    <div class="col-xs-8">
      <div class="dataTables_filter" id="dynamic-table_fil">
        <form method="GET" action="{{ route('usuario') }}" class="form-inline">
           <div class="form-group">
            <label class="control-label" style="margin-bottom: 0;vertical-align: middle;">Buscar</label>
            <input class="form-control" name="buscar" placeholder="Buscar" style="display: inline-block;width: auto;vertical-align: middle;" type="text">            
           <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
           </div>
        </form>
        </div>
      </div>
      <div class="col-xs-4">
          <a href="{{ url('register') }}" style="float: right;" class="btn btn-info" title="Editar Usuario">Nuevo Usuario</a>
      </div>
    </div>
  </div>

	<div class="table-responsive">
      <table  class="display table table-bordered table-striped" >
        <thead>
          <tr>                      
            <th>Usuario</th>
            <th style="width: 14%" >Correo electronico</th>
            <th style="width: 14%">Documento</th>
            <th>Cargo</th>                      
            <th>Opciones</th>
          </tr>
        </thead>
        <tbody>
          @foreach($usuario as $usuarios)
              <tr class="gradeX">
                <td>{{ $usuarios->usuario }}</td>
                <td>{{ $usuarios->email }}</td>
                <td>{{ $usuarios->documento }}</td>
                <td>
                 @if($usuarios->rol==1)
                    Acudiente
                 @elseif($usuarios->rol==2)
                    Docente
                 @elseif($usuarios->rol==3)
                    Contador
                 @elseif($usuarios->rol==4)
                    Secretaria
                 @elseif($usuarios->rol==5)
                    Administrador
                 @endif 
                </td>
                <td>
                  <center>
                    <a href="{{ route('usuario.editar',$usuarios->id) }}" class="btn btn-success" title="Editar Usuario"><span class="glyphicon glyphicon-edit"></span></a>
                    <a href="{{ route('usuario.eliminar',$usuarios->id) }}" onclick="return confirm('Estas seguro que quieres eliminar a este usuario?');" class="btn btn-danger" title="Eliminar Usuario"><span class="glyphicon glyphicon-trash"></span></a>
               
                  </center>
                </td>
              </tr>
          @endforeach
          
        </tbody>
        
      </table>
      {{ $usuario->links() }}
    </div><!--/table-responsive-->
	
@endsection