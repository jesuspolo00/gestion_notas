<?php session_start();
 $_SESSION['menu']=7;
 $_SESSION['active']="general";  
 ?>

@extends('plantilla.estructura')
@section('title','Dimenciones')
@section('pagina','boletin')
@section('subtitulo','Agregar Dimenciones')
@section('body')

	<div class="row">
		<div class="container">
			<div class="col-xs-12">
	    			@foreach($categoria as $categorias)
	    			<form class="form-horizontal" action="{{ route('nivelCat.editar',$categorias->id)}}" method="POST">
	    			{{ csrf_field() }}
			    		<div class="form-group">
						    <label class="col-sm-2 control-label">Categorias</label>
						    <div class="col-sm-7">
						      <input type="text" class="form-control" name="nombre" id="input{{$categorias->id}}" value="{{ $categorias->nombre }}" required="" disabled>
						    </div>
						    <div class="col-xs-3">
						    	<button type="button" id="editcurso{{ $categorias->id }}" class="btn btn-default" onclick="editar({{$categorias->id}})"><span class="glyphicon glyphicon-edit"></span></button>
							    <button id="cursado{{$categorias->id}}" type="submit" class="btn btn-default hidden"><span class="glyphicon glyphicon-ok"></span></button>
							    <a href="{{ route('nivelCat.eliminar',$categorias->id)}}" class="btn btn-default"><span class="glyphicon glyphicon-trash"></span></a>
						    </div>
						 </div>
					</form>
					 @endforeach				
				
				<form class="form-horizontal" action="{{ route('nivelCat.agregar') }}" method="POST">
				{{ csrf_field() }}	
				  <div id="contenedormateria" class="col-xs-12">
					  <div id="dimencion1" class="form-group">
					    <label class="col-sm-2 control-label">Categoria 1</label>
					    <div class="col-sm-5">
					      <input type="text" class="form-control" required="" name="categoria[]" placeholder="Ingrese Una Categoria">
					    </div>
					    <div id="botones1" class="col-xs-3">
					    <button type="button" class="btn btn-default" onclick="quitarCategoria();">-</button>
					    <button type="button" class="btn btn-default" onclick="agregarCategoria();">+</button>
					    </div>
					  </div>
				  </div>
				  <div class="form-group">
				    <div class="col-sm-offset-3 col-xs-offset-1 col-sm-9 col-lg-6">
				    	<a href="{{ route('configuracion.index','nivel') }}" class="btn btn-default" >Volver</a>
				      <button type="submit" class="btn btn-default">Guardar</button>
				    </div>
				  </div>
				</form>
			
			</div>
		</div>
	</div>

@endsection
@section('script')
<script type="text/javascript">	
	var numero=1;
	function agregarCategoria(){
		$( "#botones1" ).remove();
		var ni =$("#ni").html();
		numero++;
		$('#contenedormateria').append('<div id="dimencion'+numero+'" class="form-group">'
					    +'<label class="col-sm-2 control-label">Categoria '+numero+'</label>'
					    +'<div class="col-sm-5">'
					    +'<input type="text" class="form-control" name="categoria[]" placeholder="Ingrese Una Categoria">'
					    +'</div>'
					    +'<div id="botones1" class="col-xs-3">'
					    +'<button type="button" class="btn btn-default" onclick="quitarCategoria();">-</button>'
					    +'<button type="button" class="btn btn-default" onclick="agregarCategoria();">+</button>'
					    +'</div>'
					 +'</div>');
	}
	function quitarCategoria(){
		if(numero<=1){

		}else{			
		$("#dimencion"+numero).remove();	
		numero--;		
		$("#dimencion"+numero).append('<div id="botones1" class="col-xs-3">'
					    +'<button type="button" class="btn btn-default" onclick="quitarCategoria();">-</button>'
					    +'<button type="button" class="btn btn-default" onclick="agregarCategoria();">+</button>'
					    +'</div>');
		}
		
	}
	function editar(id){		
		$("#input"+id).removeAttr('disabled'); 
		$("#editcurso"+id).addClass('hidden');
		$("#cursado"+id).removeClass('hidden');
	}
</script>
@endsection