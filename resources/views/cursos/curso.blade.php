<?php session_start();
 $_SESSION['menu']=3;
 $_SESSION['active']="cursos";  
 ?>

@extends('plantilla.estructura')

@section('title','inicio')
@section('pagina','boletin')
@section('subtitulo','Perfil Nivel')
@section('body')
	<center><h1>{{ $curso->nombre }}</h1></center>
	<hr>
	  <!-- Nav tabs -->
	  <ul class="nav nav-tabs" id="mytab" role="tablist">
	  	<li role="presentation" class="active"><a href="#informacion" aria-controls="informacion" role="tab" data-toggle="tab">Informacion</a></li>
	    <li role="presentation"><a href="#lista" aria-controls="lista" role="tab" data-toggle="tab">Lista Estudiantes</a></li>
	    <li role="presentation"><a href="#horario" aria-controls="horario" role="tab" data-toggle="tab">Horario</a></li>
	    @if($curso->activo==0)
	    	<li role="presentation"><a href="#periodo" aria-controls="periodo" role="tab" data-toggle="tab">Cerrar Periodo</a></li>
	    @endif
	  </ul>

	  <!-- Tab panes -->
	  <div class="tab-content">
	  		<!-- informacion -->
		    <div role="tabpanel" class="tab-pane active" id="informacion">
		@if(Auth::User()->validar('4'))
		    	<a href="{{ route('cursos.detalle',$curso->id) }}" style="float: right; margin-bottom: 10px;" class="btn btn-default"><span class="glyphicon glyphicon-edit"></span></a>
@endif
		    	<div class="row">
		    		<div class="col-xs-12">
			    		<form class="form-horizontal">
			    			
			    			<div class="col-sm-6">
					    		<div class="form-group">
						    		<label class="col-sm-3 control-label">Nombre del curso:</label>			    		
						    		<div class="col-sm-9">
					    				<input type="text" class="form-control" disabled value="{{ $curso->nombre }}">
					    			</div>
					    		</div>					    		
				    		</div>				    		
				    		<div class="col-sm-6">
					    		<div class="form-group">
						    		<label class="col-sm-3 control-label">Director de grupo:</label>			    		
						    		<div class="col-sm-9">
					    				<input type="text" class="form-control" disabled value="{{ $detalle[0]->director_grupo }}">
					    			</div>
					    		</div>					    		
				    		</div>	
				    		
				    		<div class="col-sm-6">
				    			<div class="form-group">
						    		<label class="col-sm-3 control-label">Tipo de Calificacion:</label>			    		
						    		<div class="col-sm-9">
					    				<input type="text" class="form-control" disabled value="{{ $detalle[0]->tipo_calificacion }}">
					    			</div>
					    		</div>
					    	</div>
					    	<div class="col-sm-6">
				    			<div class="form-group">
						    		<label class="col-sm-3 control-label">Numero de estudiantes:</label>			    		
						    		<div class="col-sm-9">
					    				<input type="text" class="form-control" disabled value="{{ $detalle[0]->numero_estudiantes }}">
					    			</div>
					    		</div>
					    	</div>
					    	<div class="col-sm-6">
				    			<div class="form-group">
						    		<label class="col-sm-3 control-label">Nivel de formacion:</label>			    		
						    		<div class="col-sm-9">
					    				<input type="text" class="form-control" disabled value="{{ $detalle[0]->nivel_formacion }}">
					    			</div>
					    		</div>
					    	</div>
			    		</form>
		    		</div>
				</div>					  
		    </div>
		  <!-- Lista Estudiantes -->
		    <div role="tabpanel" class="tab-pane" id="lista">		    	
				<div class="table-responsive">
	                <table  class="display table table-bordered table-striped" >
	                  <thead>
	                    <tr>                      
	                      <th>Nombre</th>
	                      <th style="width: 14%" >Tipo Documento</th>
	                      <th style="width: 14%">Documento</th>                   
	                      <th>Opciones</th>
	                    </tr>
	                  </thead>
	                  <tbody>
	                    @foreach($estudiante as $estudiantes)
	                        <tr class="gradeX">
	                          <td>{{ $estudiantes->nombre." ".$estudiantes->apellido }}</td>
	                          <td>
	                          @if($estudiantes->tipo_documento==1)
	                          C.C
	                          @elseif($estudiantes->tipo_documento==2)
	                          T.I
	                          @elseif($estudiantes->tipo_documento==3)
	                          R.C
	                          @endif
	                          </td>
	                          <td>{{ $estudiantes->documento }}</td>
	                          <td>
	                            <center>
	                              <a href="{{ route('estudiantes.perfil',$estudiantes->id) }}" class="btn btn-info" title="Ver Perfil"><span class="glyphicon glyphicon-eye-open"></span></a>
	                              @if(Auth::User()->validar('4'))
	                              <a href="{{ route('estudiantes.editar',$estudiantes->id) }}" class="btn btn-success" title="Editar Estudiante"><span class="glyphicon glyphicon-edit"></span></a>
	                              <a href="{{ route('estudiantes.eliminar',$estudiantes->id) }}" onclick="return confirm('Estas seguro que quieres eliminar a este usuario?');" class="btn btn-danger" title="Eliminar Estudiante"><span class="glyphicon glyphicon-trash"></span></a>
	                              @endif
	                            </center>
	                          </td>
	                        </tr>
	                    @endforeach
	                  </tbody>
	                </table>
	            </div><!--/table-responsive-->					  
		    </div>
		    
		    <!-- Horario -->
		    <div role="tabpanel" class="tab-pane" id="horario">
		    	<div class="table-responsive" style="margin-top: 15px;">
                <table  class="display table table-bordered table-striped" >
                  <thead>
                    <tr>
                      <th>Hora</th>
                      <th>Lunes</th>
                      <th>Martes</th>
                      <th>Miercoles</th>
                      <th>Jueves</th>
                      <th>Viernes</th>
                      <th>Sabados</th>
                    </tr>
                  </thead>
                  <tbody>               
                	@foreach($hora as $horas)
                		<tr class="gradeX">
                      	<td style="text-align: center;">{{ $horas->hora}}</td>
	                	@foreach($dia as $dias)
	                		<td style="text-align: center;">
                  			@foreach($horario as $horarios)
                  				@if($horarios->dia_id==$dias->id and $horarios->hora_id==$horas->id)
                  					<label>{{ $horarios->materia }}</label>
                  				@endif
							@endforeach
							</td>
	                	@endforeach
	                	</tr>  
	              	@endforeach                      
                  </tbody>
                </table>
              </div>
		  </div>
		  @if($curso->activo==0)
			  <div role="tabpanel" class="tab-pane" id="periodo">
			  		<div class="col-xs-12">
			  			<form class="form-inline" action="{{route('nivel.cerrar',$curso->id)}}" method="POST">
			  				{{ csrf_field() }}
			  			Nota: Una ves cerrado el curso no se podra modificar ni subir notas en el periodo 
			  			<button type="submit" class="btn btn-info">Cerrar Periodo</button>
			  			</form>
			  		</div>
			  </div>
		  @endif
		</div>	
@endsection
@section('script')
	<script type="text/javascript">
		$( document ).ready(function() {
	  		var tab = "<?php if(isset($tab)){echo $tab;} ?>";
	  		if(tab != ""){
	  			$("#mytab a[href='#"+tab+"']").tab('show');
	  		}
		});
	</script>
@endsection
