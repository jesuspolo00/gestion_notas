<?php session_start();
 $_SESSION['menu']=3;
 $_SESSION['active']="cursos";  
 ?>

@extends('plantilla.estructura')

@section('title','inicio')
@section('pagina','boletin')
@section('subtitulo','Buscar Nivel')
@section('script')
	<script src="{{ asset('plugins/data-tables/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('plugins/data-tables/DT_bootstrap.js') }}"></script>
	<script src="{{ asset('plugins/data-tables/dynamic_table_init.js') }}"></script>
	<script src="{{ asset('plugins/edit-table/edit-table.js') }}"></script>
@endsection
@section('body')
  @if(Auth::User()->validar('4'))
	<div class="row"> 
  <div class="col-xs-12 col-sm-10" style="margin-bottom: 10px;">
    <div class="dataTables_filter" id="dynamic-table_fil">
      <form method="GET" action="{{ route('cursos.buscar') }}" class="form-inline">
         <div class="form-group">
          <label style="" class="control-label" style="margin-bottom: 0;vertical-align: middle;">Buscar</label>
          <input class="form-control" name="listar" placeholder="Nombre del curso" style="display: inline-block;width: auto;vertical-align: middle;" type="text">
         <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
         </div>
      </form>
      </div>
    </div>
  </div>
  @endif
	<div class="table-responsive">
    <table  class="display table table-bordered table-striped" >
      <thead>
        <tr>
          <th>Nivel</th>
          <th style="width: 14%" >Dir Grupo</th>
          <th style="width: 14%">Nivel Formacion</th>
          <th class="hidden-phone">Opciones</th>
        </tr>
      </thead>
      <tbody>
        @foreach($curso as $cursos)
            <tr class="gradeX">
              <td>{{ $cursos->nombre}}</td>                         
              <td>{{ $cursos->director_grupo }}</td>
              <td>{{ $cursos->nivel_formacion }}</td>
              <td>
                <center>
                  <a href="{{ route('cursos.perfil',$cursos->id) }}" class="btn btn-info" title="Ver Perfil"><span class="glyphicon glyphicon-eye-open"></span></a>
                </center>
              </td>
            </tr>
        @endforeach
      </tbody>
    </table>
    {{ $curso->links() }}
  </div><!--/table-responsive-->
@endsection