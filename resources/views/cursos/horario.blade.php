<?php session_start();
 $_SESSION['menu']=3;
 $_SESSION['active']="horario";  
 ?>

@extends('plantilla.estructura')

@section('title','inicio')
@section('pagina','boletin')
@section('subtitulo','Horario')
@section('script')
	
@endsection
@section('body')

	<div class="col-xs-12" style="padding-bottom: 10px">
		<form class="form-inline" method="get" action="{{ route('cursos.horario') }}">
				  <div class="form-group">
				    <label>curso</label>
				    <select class="form-control" name="curso">
				    @foreach($curso as $cursos)
				    	@if($selected!="")
				    		@if($selected['curso']==$cursos->id)
				    		<option selected="" value="{{$cursos->id}}">{{ $cursos->nombre }}</option>
				    		@endif
				    	@endif
					  <option value="{{$cursos->id}}">{{ $cursos->nombre }}</option>					  
					  @endforeach
					</select>
				  </div>
				  <div class="form-group">
				    <label>periodo</label>
				    <select class="form-control" name="periodo">
				   		@if($selected!="")
				    		<option selected="">{{ $selected['periodo'] }}</option>
				    	@endif
					  <option>1</option>
					  <option>2</option>
					  <option>3</option>
					  <option>4</option>
					</select>
				  </div>
				  <div class="form-group">
				    <label>año</label>
				    <select class="form-control" name="año">
				    @if($selected!="")
				    		<option selected="">{{ $selected['año'] }}</option>
				    @endif
				    @for($i = date('Y'); $i >= 1900; $i--)
					    <option>{{ $i }}</option>
					@endfor
					</select>
				  </div>	  
				  <button type="submit" name="boton" value="buscar" class="btn btn-default">Buscar</button>
		</form>
		<!-- **tabla** -->
			<div class="table-responsive" style="margin-top: 15px;">
                <table  class="display table table-bordered table-striped" >
                  <thead>
                    <tr>
                      <th>Hora</th>
                      <th>Lunes</th>
                      <th>Martes</th>
                      <th>Miercoles</th>
                      <th>Jueves</th>
                      <th>Viernes</th>
                      <th>Sabados</th>
                    </tr>
                  </thead>
                  <tbody>
               
                	@foreach($hora as $horas)
                		<tr class="gradeX">
                      	<td style="text-align: center;">{{ $horas->hora}}</td>
	                	@foreach($dia as $dias)
	                		<td style="text-align: center;">
                  			@foreach($horario as $horarios)
                  				@if($horarios->dia_id==$dias->id and $horarios->hora_id==$horas->id)
                  					<label>{{ $horarios->materia }}</label>
                  				@endif
							@endforeach
							</td>
	                	@endforeach
	                	</tr>  
	              	@endforeach
                      
                  </tbody>

                </table>
              </div><!--/table-responsive-->
	</div>
@endsection