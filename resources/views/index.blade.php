<?php session_start();
 $_SESSION['menu']=1;
 $_SESSION['active']="";  
 ?>

@extends('plantilla.estructura')
@section('head')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
@endsection
@section('title','boletines')
@section('pagina')
@section('subtitulo','inicio')
@section('body')
        <div class="col-xs-12" style="margin-bottom: 20px;">
            <div class="container" style="width: 70%;">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
              <!-- Indicators -->
              <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
              </ol>

              <!-- Wrapper for slides -->
              <div class="carousel-inner" role="listbox">
                
                <div class="item ">
                  <img src="{{ asset('images/slide_3.jpg') }}" style="height: 300px;">
                  <div class="carousel-caption">
                    
                  </div>
                </div>
                <div class="item active">
                  <img src="{{ asset('images/slide_banner.jpg') }}" style="height: 300px;">
                  <div class="carousel-caption">
                    
                  </div>
                </div>
              </div>

              <!-- Controls -->
              <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
            </div>
        </div>
    @if(Auth::User()->validar('4'))
        <div class="col-xs-12">
            <div class="container" style="width: 80%">
            <div class="col-xs-5">
                <div class="list-group">
                  <div class="list-group-item">
                    <form method="POST" action="{{ route('finalizar.periodo') }}">
                        {{ csrf_field() }}
                        <h4 class="list-group-item-heading">Finalizar Periodo</h4>
                        <p class="list-group-item-text">Nota: se restablecera todos los cursos para su uso</p>
                        <button class="btn btn-info" style="margin-top: 5px;">Finalizar Periodo</button>
                    </form>
                  </div>
                </div>
            </div>
            <div class="col-xs-5" style="float: right;">
                <div class="list-group">
                  <div class="list-group-item">
                    <form method="POST" action="{{ route('finalizar.año') }}">
                        {{ csrf_field() }}
                        <h4 class="list-group-item-heading">Finalizar Año Escolar</h4>
                        <p class="list-group-item-text">Nota: finalizar el ultimo periodo antes de finalizar el año</p>
                        <button class="btn btn-info" style="margin-top: 5px;">Finalizar Año</button>
                    </form>
                  </div>
                </div>
            </div>
            </div>
        </div>
        <label>&nbsp;</label>
    @endif
@endsection