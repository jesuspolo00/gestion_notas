<?php session_start();
 $_SESSION['menu']=2;
 $_SESSION['active']="buscar_estudiante";  
 ?>

@extends('plantilla.estructura')

@section('title','inicio')
@section('pagina','boletin')
@section('subtitulo','Registrar Estudiante')
@section('script')
	
@endsection
@section('body')
	<div class="row">
		<form class="form-horizontal" action="{{ route('estudiantes.actualizar',$estudiante->id)}}" method="POST" enctype="multipart/form-data">
		{{ csrf_field() }}
		{{ method_field('PUT') }}
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Nombre</label>
		    <div class="col-sm-8 col-lg-5">
		      <input type="text" class="form-control" name="nombre" placeholder="Nombre" value="{{ $estudiante->nombre }}"required>
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Apellido</label>
		    <div class="col-sm-8 col-lg-5">
		      <input type="text" class="form-control" name="apellido" value="{{ $estudiante->apellido }}" placeholder="Apellido" required>
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Tipo Documento</label>
		    <div class="col-sm-8 col-lg-5">
		      	<select class="form-control" name="tdocumento" required>
		      	@if($estudiante->tipo_documento == 1)
		      		<option value="1" selected>C.C</option>
		      	@elseif($estudiante->tipo_documento == 2)
		      		<option value="2" selected>T.I</option>
		      	@elseif($estudiante->tipo_documento == 3)
		      		<option value="3" selected>R.C</option>
		      	@endif
		      	  <option value="2">T.I</option>
				  <option value="1">C.C</option>				  
				  <option value="3">R.C</option>
				</select>
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Documento</label>
		    <div class="col-sm-8 col-lg-5">
		      <input type="number" class="form-control" name="documento" value="{{ $estudiante->documento }}" placeholder="Documento" required>
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Genero</label>
		    <div class="col-sm-8 col-lg-5">
		      <select class="form-control" name="genero" required>
		      		@if($estudiante->genero == 1)
		      		<option value="1" selected="">Masculino</option>
		      		@elseif($estudiante->genero == 2)
		      		<option value="2" selected="">Femenino</option>
		      		@endif
				  <option value="1">Masculino</option>
				  <option value="2">Femenino</option>				  
				</select> 
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Tipo Sangre</label>
		    <div class="col-sm-8 col-lg-5">
		      <select class="form-control" name="tipo" required>
		      	@if($estudiante->tipo_sangre == 1)
		      		<option value="1" selected=""><var>A</var><sup>+</sup></option>
		      	@elseif($estudiante->tipo_sangre == 2)
		      	<option value="2" selected=""><var>A</var><sup>-</sup></option>
		      	@elseif($estudiante->tipo_sangre == 3)
		      	<option value="3" selected=""><var>B</var><sup>+</sup></option>
		      	@elseif($estudiante->tipo_sangre == 4)
		      	<option value="4" selected=""><var>B</var><sup>-</sup></option>
		      	@elseif($estudiante->tipo_sangre == 5)
		      	<option value="5" selected=""><var>AB</var><sup>+</sup></option>
		      	@elseif($estudiante->tipo_sangre == 6)
		      	<option value="6" selected=""><var>AB</var><sup>-</sup></option>
		      	@elseif($estudiante->tipo_sangre == 7)
		      	<option value="7" selected=""><var>O</var><sup>+</sup></option>
		      	@elseif($estudiante->tipo_sangre == 8)
		      	<option value="8" selected=""><var>O</var><sup>-</sup></option>
		      	@endif
				  <option value="1"><var>A</var><sup>+</sup></option>
				  <option value="2"><var>A</var><sup>-</sup></option>
				  <option value="3"><var>B</var><sup>+</sup></option>
				  <option value="4"><var>B</var><sup>-</sup></option>
				  <option value="5"><var>AB</var><sup>+</sup></option>
				  <option value="6"><var>AB</var><sup>-</sup></option>
				  <option value="7"><var>O</var><sup>+</sup></option>
				  <option value="8"><var>O</var><sup>-</sup></option>
				</select>
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Foto</label>
		    <div class="col-sm-8 col-lg-5">
		      <img width="100px" src="{{ asset($estudiante->foto) }}" title="No tiene foto">
		      <input type="file" class="form-control" name="foto" placeholder="Foto" required>
		    </div>
		  </div>		  
		  <div class="form-group">
		    <div class="col-sm-offset-3 col-sm-9 col-lg-6">
		      <button type="submit" class="btn btn-default">Guardar</button>
		    </div>
		  </div>
		</form>
	</div>
@endsection