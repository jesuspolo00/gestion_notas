<?php session_start();
 $_SESSION['menu']=2;
 $_SESSION['active']="buscar_estudiante";  
 ?>

@extends('plantilla.estructura')

@section('title','inicio')
@section('pagina','boletin')
@section('subtitulo','Buscar Estudiantes')
@section('script')
	<script src="{{ asset('plugins/data-tables/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('plugins/data-tables/DT_bootstrap.js') }}"></script>
	<script src="{{ asset('plugins/data-tables/dynamic_table_init.js') }}"></script>
	<script src="{{ asset('plugins/edit-table/edit-table.js') }}"></script>
  
@endsection
@section('body')
  @if(Auth::User()->validar('4.2'))
	<div class="row">    
  <div class="col-xs-12 col-sm-10" style="margin-bottom: 10px;">
    <div class="dataTables_filter" id="dynamic-table_fil">
      <form method="GET" action="{{ route('estudiantes.buscar') }}" class="form-inline">
         <div class="form-group">
          <label class="control-label" style="margin-bottom: 0;vertical-align: middle;">Buscar</label>
          <input class="form-control" name="resultado" placeholder="Buscar" style="display: inline-block;width: auto;vertical-align: middle;" type="text">            
         <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
         </div>
      </form>
      </div>
    </div>
  </div>
  @endif
	<div class="table-responsive">
                <table  class="display table table-bordered table-striped" >
                  <thead>
                    <tr>                      
                      <th>Nombre</th>
                      <th style="width: 14%" >Tipo Documento</th>
                      <th style="width: 14%">Documento</th>
                      <th>Genero</th>                      
                      <th>Opciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($estudiante as $estudiantes)
                        <tr class="gradeX">
                          <td>{{ $estudiantes->nombre." ".$estudiantes->apellido }}</td>
                          <td>
                          @if($estudiantes->tipo_documento==1)
                          C.C
                          @elseif($estudiantes->tipo_documento==2)
                          T.I
                          @elseif($estudiantes->tipo_documento==3)
                          R.C
                          @endif
                          </td>
                          <td>{{ $estudiantes->documento }}</td>
                          <td>
                            @if($estudiantes->genero==1)
                            Masculino
                            @elseif($estudiantes->genero==2)
                            Femenino
                            @endif
                          </td>
                          <td>
                            <center>
                              <a href="{{ route('estudiantes.perfil',$estudiantes->id) }}" class="btn btn-info" title="Ver Perfil"><span class="glyphicon glyphicon-eye-open"></span></a>
                              @if(Auth::User()->validar('4'))
                              <a href="{{ route('estudiantes.editar',$estudiantes->id) }}" class="btn btn-success" title="Editar Estudiante"><span class="glyphicon glyphicon-edit"></span></a>
                              <a href="{{ route('estudiantes.eliminar',$estudiantes->id) }}" onclick="return confirm('Estas seguro que quieres eliminar a este usuario?');" class="btn btn-danger" title="Eliminar Estudiante"><span class="glyphicon glyphicon-trash"></span></a>
                              @endif
                            </center>
                          </td>
                        </tr>
                    @endforeach
                    
                  </tbody>
                  
                </table>
                {{ $estudiante->links() }}
              </div><!--/table-responsive-->
	
@endsection