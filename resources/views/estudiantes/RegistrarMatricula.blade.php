<?php session_start();
 $_SESSION['menu']=2;
 $_SESSION['active']="registrar";  
 ?>

@extends('plantilla.estructura')

@section('title','inicio')
@section('pagina','boletin')
@section('subtitulo','Matricular Estudiante')
@section('script')
	
@endsection
@section('body')
	<div class="row">		
		<form class="form-horizontal" action="{{ route('matricula.store')}}" method="POST">
		{{ csrf_field() }}
		<input type="hidden" class="hidden" name="e" value="{{ $estudiante}}">
		  <div class="col-sm-12">
		  		<center><h3>Registrar Matricula</h3></center>
		  </div>		
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Curso:</label>
		    <div class="col-sm-8 col-lg-5">
		      	<select class="form-control" name="curso" required="">
		      	@foreach($curso as $cursos)
				  <option value="{{ $cursos->id }}">{{ $cursos->nombre }}</option>				  
				@endforeach
				</select>
		    </div>
		  </div>
		  <div class="form-group">
		    <div class="col-sm-offset-3 col-sm-9 col-lg-6">
              <input type="submit" value="Matricular" class="btn btn-success">
		    </div>
		  </div>
		</form>
	</div>
@endsection