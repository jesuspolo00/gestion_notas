<?php session_start();
 use App\Dimencion;
 $_SESSION['menu']=2;
 $_SESSION['active']="buscar_estudiante"; 
 $meses = ['enero','febrero','marzo','abril','mayo','junio','julio','agosto','septiembre','octubre','noviembre','diciembre'];
 ?>

@extends('plantilla.estructura')

@section('title','inicio')
@section('pagina','boletin')
@section('subtitulo','Perfil Estudiante')
@section('script')
@endsection
@section('body')		
		<center><div style="width: 150px;height: 150px;border: 1px solid #ddd;">
			<img src="{{ asset($estudiante->foto) }}" style="width: 100%; height: 100% ; position: relative; ">
		</div></center>
		<center><h1 style="margin-bottom: 1px;">{{ $estudiante->nombre}} {{ $estudiante->apellido }}</h1></center>
		<center><h3><small>{{ $curso[0]->curso}}</small></h3></center>
		<hr>
	  <!-- Nav tabs -->
	  <ul class="nav nav-tabs" role="tablist">
	  	<li role="presentation" class="active"><a href="#informacion" aria-controls="informacion" role="tab" data-toggle="tab">Informacion</a></li>
	  	@if(Auth::User()->validar('4.2'))
		  	@if($curso[0]->activo==0)
		    	<li role="presentation"><a href="#materias" aria-controls="materias" role="tab" data-toggle="tab">Ausencias</a></li>
		    @endif
	    @endif
	    @if(Auth::User()->validar('1'))
	    	<li role="presentation"><a href="#horario" aria-controls="horario" role="tab" data-toggle="tab">Horario</a></li>
	    @endif
	    <li role="presentation"><a href="#pagos" aria-controls="pagos" role="tab" data-toggle="tab">Pagos</a></li>
	    @if(Auth::User()->validar('4.2'))
		    @if($curso[0]->activo==0)
		    	<li role="presentation"><a href="#calificar" aria-controls="calificar" role="tab" data-toggle="tab">Calificar</a></li>
		    @endif
	    @endif
	    @if(Auth::User()->validar('1'))
		    	<li role="presentation"><a href="#boletin" aria-controls="boletin" role="tab" data-toggle="tab">boletin</a></li>
	    @endif
	  </ul>

	  <!-- Tab panes -->
	  <div class="tab-content">
	  		<!-- informacion -->
		    <div role="tabpanel" class="tab-pane active" id="informacion">		    	
		    	<div class="row">
		    		@if(Auth::User()->validar('4'))
		    		<a href="{{ route('estudiantes.acudiente',$estudiante->id) }}" style="float: right; margin-bottom: 10px;" title="Ver Acudiente" class="btn btn-default"><span class="glyphicon glyphicon-eye-open"></span></a>
		    		@endif
		    		<div class="col-xs-12">
			    		<form class="form-horizontal">
			    			
			    			<div class="col-sm-6">
					    		<div class="form-group">
						    		<label class="col-sm-3 control-label">Nombre:</label>			    		
						    		<div class="col-sm-9">
					    				<input type="text" class="form-control" disabled value="{{$estudiante->nombre }}">
					    			</div>
					    		</div>					    		
				    	
					    		<div class="form-group">
						    		<label class="col-sm-3 control-label">Apellido:</label>			    		
						    		<div class="col-sm-9">
					    				<input type="text" class="form-control" disabled value="{{$estudiante->apellido }}">
					    			</div>
					    		</div>					    		
				    
				    			<div class="form-group">
						    		<label class="col-sm-3 control-label">Tipo Documento:</label>			    		
						    		<div class="col-sm-9">
						    		@if($estudiante->tipo_documento == 1)
							      		<input type="text" class="form-control" disabled value="Cedula de Ciudadania">
							      	@elseif($estudiante->tipo_documento == 2)
							      		<input type="text" class="form-control" disabled value="Tarjeta de Identidad">
							      	@elseif($estudiante->tipo_documento == 3)
							      		<input type="text" class="form-control" disabled value="Registro Civil">
							      	@endif
					    				
					    			</div>
					    		</div>
				    
				    			<div class="form-group">
						    		<label class="col-sm-3 control-label">Documento:</label>			    		
						    		<div class="col-sm-9">
					    				<input type="text" class="form-control" disabled value="{{$estudiante->documento }}">
					    			</div>
					    		</div>
					    	</div>	
					    	<div class="col-sm-6">
				    			<div class="form-group">
						    		<label class="col-sm-3 control-label">Genero:</label>			    		
						    		<div class="col-sm-9">
							    		@if($estudiante->genero == 1)
							      			<input type="text" class="form-control" disabled value="Masculino">
							      		@elseif($estudiante->genero == 2)
							      			<input type="text" class="form-control" disabled value="Femenino">
							      		@endif
					    				
					    			</div>
					    		</div>
					    	
				    			<div class="form-group">
						    		<label class="col-sm-3 control-label">Tipo Sangre:</label>		    		
						    		<div class="col-sm-9">
						    			@if($estudiante->tipo_sangre == 1)
								      		<input type="text" class="form-control" disabled value="A+">
								      	@elseif($estudiante->tipo_sangre == 2)
								      		<input type="text" class="form-control" disabled value="A-">		
								      	@elseif($estudiante->tipo_sangre == 3)
								      		<input type="text" class="form-control" disabled value="B+">
								      	@elseif($estudiante->tipo_sangre == 4)
								      		<input type="text" class="form-control" disabled value="B-">
								      	@elseif($estudiante->tipo_sangre == 5)
								      		<input type="text" class="form-control" disabled value="AB+">
								      	@elseif($estudiante->tipo_sangre == 6)
								      		<input type="text" class="form-control" disabled value="AB-">
								      	@elseif($estudiante->tipo_sangre == 7)
								      		<input type="text" class="form-control" disabled value="O+">
								      	@elseif($estudiante->tipo_sangre == 8)
								      		<input type="text" class="form-control" disabled value="O-">
								      	@endif
					    				
					    			</div>
					    		</div>
					    	</div>
			    		</form>
		    		</div>
				</div>					  
		    </div>   		
		    @if(Auth::User()->validar('4.2'))
			@if($curso[0]->activo==0)    			
		  <!-- asignacion -->
		    <div role="tabpanel" class="tab-pane" id="materias">		    	
		    	<div class="row">
		    		<div class="col-xs-12">
			    		<form class="form-horizontal" method="POST" action="{{route('estudiantes.ausencia',$estudiante->id)}}">
			    			{{ csrf_field() }}
			    			<div class="col-sm-6">
				    			<div class="form-group">
						    		<label class="col-sm-4 control-label">Numero de ausencias:</label>			    		
						    		<div class="col-sm-8">
					    				<input type="text" class="form-control" name="ausencia" placeholder="Ausencias" required="" value="{{$curso[0]->ausencia }}">
					    			</div>
					    		</div>
					    		<div class="form-group">
						    		<label class="col-sm-4 control-label">Observacion general:</label>			    		
						    		<div class="col-sm-8">
					    				<textarea class="form-control" maxlength="1200" name="observacion" placeholder="observacion general, se admiten 1200 caracteres como maximo" required="" value="">{{$curso[0]->observacion }}</textarea>
					    			</div>
					    		</div>
					    		<div class="col-xs-offset-3">
					    		<button type="submit" class="btn btn-primary">Guardar</button>
					    		</div>
					    	</div>
			    		</form>			    		
			    	</div>
			    </div>					  
		    </div>	
		    @endif	 
		    @endif  
		    @if(Auth::User()->validar('1'))
		    <!-- Horario -->
		    <div role="tabpanel" class="tab-pane" id="horario">
		    	<div class="table-responsive" style="margin-top: 15px;">
                <table  class="display table table-bordered table-striped" >
                  <thead>
                    <tr>
                      <th>Hora</th>
                      <th>Lunes</th>
                      <th>Martes</th>
                      <th>Miercoles</th>
                      <th>Jueves</th>
                      <th>Viernes</th>
                      <th>Sabados</th>
                    </tr>
                  </thead>
                  <tbody>               
                	@foreach($hora as $horas)
                		<tr class="gradeX">
                      	<td style="text-align: center;">{{ $horas->hora}}</td>
	                	@foreach($dia as $dias)
	                		<td style="text-align: center;">
                  			@foreach($horario as $horarios)
                  				@if($horarios->dia_id==$dias->id and $horarios->hora_id==$horas->id)
                  					<label>{{ $horarios->materia }}</label>
                  				@endif
							@endforeach
							</td>
	                	@endforeach
	                	</tr>  
	              	@endforeach                      
                  </tbody>
                </table>
              </div>
            </div> 
            @endif  
		    <!-- pagos -->
		    <div role="tabpanel" class="tab-pane" id="pagos">
		    	<div class="col-xs-12">
          	<table class="table table-bordered table-hover">
          		<thead>
          			<tr>
          				<th>Mes</th>
          				<th>Pago</th>
          			</tr>
          		</thead>
            <?php $conteo=0; ?>
	            <tbody>
	            @foreach($meses as $mes)
	                <?php $verificar=0; ?>
	                
	                @foreach($pago as $pagos)
	                  @if($pagos->mes==$mes)
	                      	<tr class="success">
	                        	<td>{{ $mes }}</td>
	                        	<td>Si</td>
	                        </tr>
	                      <?php $verificar=1; ?>
	                  @endif
	                @endforeach
	                @if($verificar==0)
	                    <tr>
	                      <td>{{ $mes }}</td>
	                      <td>No</td>
	                    </tr>
	                @endif
	                <?php $conteo++; ?>
	            @endforeach
	            </tbody>
             </table>
        </div>
		    </div>
		    @if(Auth::User()->validar('4.2'))
		    @if($curso[0]->activo==0)
		    <!-- calificar -->
		    <div role="tabpanel" class="tab-pane" id="calificar">
		    	<div class="container">
		    		<table class="table table-bordered">
		    		<form method="POST" action="{{ route('estudiantes.calificar',[$curso[0]->id,$estudiante->id] )}}">
		    		@foreach($dimencion as $dimenciones)	
		    			<?php 
		    				$materia_boletin = Dimencion::materias_boletin($dimenciones->id)->get();	    				
					        $subcompetencia = Dimencion::subcompetencias_boletin($dimenciones->id)->get();
					        $competencia = Dimencion::competencias_boletin($dimenciones->id)->get();
					        $subdimencion = Dimencion::subdimenciones_boletin($dimenciones->id)->get();

		    			 ?>
		    			 @if($materia_boletin->isNotEmpty())
		    			<thead>
		    				<th colspan="5" style="background-color: #538cd5;"><center>{{ $dimenciones->nombre }}</center></th>
		    			</thead>
		    		{{ csrf_field() }}
		    			<tbody>
		    				@foreach($subdimencion as $subdimenciones)
			    				<tr>
			    					<td style="width: 25%;background-color: #c5d8f1; text-align: center;">{{$subdimenciones->nombre}}</td>
			    					<td style="width: 60%;background-color: #c5d8f1; text-align: center;">aspecto a evaluar</td>
			    					<td style="width: 5%;background-color: #c5d8f1;">LA</td>
			    					<td style="width: 5%;background-color: #c5d8f1;">LD</td>
			    					<td style="width: 5%;background-color: #c5d8f1;">NE</td>
			    				</tr>
			    				@foreach($competencia as $competencias)
			    					@if($competencias->sub_dimencion_id == $subdimenciones->id)
			    					<?php
			    						$row=Dimencion::contar($competencias->id);

			    					?>
					    				<tr>
					    					<td rowspan="{{$row}}" class="text-center">{{$competencias->nombre}}</td>
					    				</tr>
					    				@foreach($subcompetencia as $subcompetencias)
					    					@if($subcompetencias->competencia_id == $competencias->id)
							    				<tr>
							    					<td colspan="4" style="background-color: #c5d8f1;">{{$subcompetencias->nombre}}</td>
							    				</tr>
						    					@foreach($materia_boletin as $mate)
								    				@if($mate->sub_competencia_id == $subcompetencias->id)
									    				<tr>
									    					<td style="width: 60%;">{{$mate->nombre}}</td>
									    					<?php $verificar=0; ?>
									    					@foreach($nota as $notas)
									    						@if($notas->materia_id == $mate->id)
											    					@for($i=1;$i<=3;$i++)		    						@if($notas->nota == $i)
											    							<td style="width: 5%;" style="text-align: center;"><input type="radio" style="border:none;" name="{{ $mate->id}}" value="{{$i}}-{{$mate->id}}" checked="" step="any"></td>
											    						@else
											    							<td style="width: 5%;" style="text-align: center;"><input type="radio" style="border:none;" name="{{ $mate->id}}" value="{{$i}}-{{$mate->id}}" step="any"></td>
											    						@endif
											    					@endfor
											    					<?php $verificar=1; ?>
											    				@else
											    					@continue
											    				@endif
									    					@endforeach
									    				@if($verificar==0)
									    					@for($i=1;$i<=3;$i++)
									    						<td style="width: 5%;" style="text-align: center;"><input type="radio" style="border:none;" name="{{ $mate->id}}" value="{{$i}}-{{$mate->id}}" step="any"></td>
									    					@endfor
									    				@endif
									    				</tr>
								    				@else
								    					@continue
								    				@endif
							    				@endforeach
							    			@else
							    				@continue
							    			@endif
					    				@endforeach
					    			@else
					    				@continue
					    			@endif
			    				@endforeach
		    				@endforeach
		    			</tbody>
		    			@endif
		    			@endforeach
		    		</table>
		    			<button type="submit" class="btn btn-default">Guardar</button>
		    		</form>
		    	</div>
		    </div>
		    @endif
		    @endif
		    @if(Auth::User()->validar('1'))

		    <?php 
		    	$periodo=['I TRIMESTRE (Agosto-Octubre)','II TRIMESTRE (Noviembre a Febrero)','III TRIMESTRE (Marzo a Junio)'];
		    	
		        $cont=0;
		     ?>
			    <div role="tabpanel" class="tab-pane" id="boletin">

			    	<table class="table">
			    		<thead>
				    		<tr>
				    			<th class="text-center">Periodo I</th>
				    			<th class="text-center">Periodo II</th>
				    			<th class="text-center">Periodo III</th>
				    		</tr>
				    	</thead>
				    	<tbody>
				    		<tr>
@if($boletin->isNotEmpty())
					    			@foreach($validar as $val)
						    			
					    				@if($val==1)
					    					<?php $per=0; ?>
					    					@foreach($boletin as $bol)
					    						
					    						@if($bol->periodo==$periodo[$cont])
					    							<td class="text-center"><a href="{{ route('estudiantes.boletin',$bol->id) }}" class="btn btn-primary">Ver</a></td>
					    							<?php $per=1; ?>
					    						@endif
					    					@endforeach
					    					@if($per==0)
					    						<td class="text-center"><span><strong>No Disponible</strong></span></td>
					    					@endif
					    				@else
					    					<td class="text-center"><span><strong>No Disponible</strong></span></td>
					    				@endif
						    			<?php $cont++; ?>
					    			@endforeach
					    		@else
					    			<td class="text-center"><span><strong>No Disponible</strong></span></td>
					    			<td class="text-center"><span><strong>No Disponible</strong></span></td>
					    			<td class="text-center"><span><strong>No Disponible</strong></span></td>
				    			@endif
				    		</tr>
				    	</tbody>
			    	</table>
			   
			    </div>
		    @endif
		</div>
	
@endsection
