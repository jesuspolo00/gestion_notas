<?php session_start();
 use App\boletin_dimencion;
 $_SESSION['menu']=2;
 $_SESSION['active']="buscar_estudiante"; 
 $meses = ['enero','febrero','marzo','abril','mayo','junio','julio','agosto','septiembre','octubre','noviembre','diciembre'];
 ?>

@extends('plantilla.estructura')

@section('title','inicio')
@section('pagina','boletin')
@section('subtitulo','Perfil Estudiante')
@section('script')
@endsection
@section('body')		
		<center><div style="width: 150px;height: 150px;border: 1px solid #ddd;">
			<img src="{{ asset('images/perfil.jpg') }}" style="max-width: 100%; position: relative; ">
		</div></center>
		<center><h1 style="margin-bottom: 1px;">{{ $estudiante->nombre}} {{ $estudiante->apellido }}</h1></center>
		<center><h3><small>{{ $curso[0]->nombre}}</small></h3></center>
		<hr style="margin-top: 0px; margin-bottom: 0px;">
	  		<div class="container-fluid">
	  			<h4 class="text-center"><small>{{ $per_letra}}</small></h4>
	  		</div>
		<hr style="margin-top: 0px; margin-bottom: 0px;">
	  	<div class="container">
		    @if(Auth::User()->validar('1'))
		   

		    <!-- calificar -->
		    <div role="tabpanel" class="tab-pane" id="calificar">
		    	<div class="container">
		    		<table class="table table-bordered">
		    		@foreach($dimencion as $dimenciones)	
		    			<?php 
		    				$materia_boletin = boletin_dimencion::boletin_materias_boletin($dimenciones->ids,$periodo)->get();	    				
					        $subcompetencia = boletin_dimencion::boletin_subcompetencias_boletin($dimenciones->ids)->get();
					        $competencia = boletin_dimencion::boletin_competencias_boletin($dimenciones->ids)->get();
					        $subdimencion = boletin_dimencion::boletin_subdimenciones_boletin($dimenciones->ids)->get();
		    			 ?>
		    			 @if($materia_boletin->isNotEmpty())
		    			<thead>
		    				<th colspan="5" style="background-color: #538cd5;"><center>{{ $dimenciones->nombre }}</center></th>
		    			</thead>
		    		{{ csrf_field() }}
		    			<tbody>
		    				@foreach($subdimencion as $subdimenciones)
			    				<tr>
			    					<td style="width: 25%;background-color: #c5d8f1; text-align: center;">{{$subdimenciones->nombre}}</td>
			    					<td style="width: 60%;background-color: #c5d8f1; text-align: center;">aspecto a evaluar</td>
			    					<td style="width: 5%;background-color: #c5d8f1;">LA</td>
			    					<td style="width: 5%;background-color: #c5d8f1;">LD</td>
			    					<td style="width: 5%;background-color: #c5d8f1;">NE</td>
			    				</tr>
			    				@foreach($competencia as $competencias)
			    					@if($competencias->boletin_sub_dimencion_id == $subdimenciones->ids)
			    					<?php
			    						$row=boletin_dimencion::contar($competencias->ids,$periodo);

			    					?>
					    				<tr>
					    					<td rowspan="{{$row}}" class="text-center">{{$competencias->nombre}}</td>
					    				</tr>
					    				@foreach($subcompetencia as $subcompetencias)
					    					@if($subcompetencias->boletin_competencia_id == $competencias->ids)
							    				<tr>
							    					<td colspan="4" style="background-color: #c5d8f1;">{{$subcompetencias->nombre}}</td>
							    				</tr>
						    					@foreach($materia_boletin as $mate)
								    				@if($mate->boletin_sub_competencia_id == $subcompetencias->ids)
									    				<tr>
									    					<td style="width: 60%;">{{$mate->nombre}}</td>
									    					<?php $verificar=0; ?>
									    					@foreach($nota as $notas)

									    						@if($notas->materia_id == $mate->ids)
											    					@for($i=1;$i<=3;$i++)		    						@if($notas->nota == $i)
											    							<td style="width: 5%;" class="letra"><span style="padding: 12px;">X</span></td>
											    						@else
											    							<td style="width: 5%;" style="text-align: center;"></td>
											    						@endif
											    					@endfor
											    					<?php $verificar=1; ?>
											    				@else
											    					@continue
											    				@endif
									    					@endforeach
									    				@if($verificar==0)
									    					@for($i=1;$i<=3;$i++)
									    						<td style="width: 5%;" style="text-align: center;"></td>
									    					@endfor
									    				@endif
									    				</tr>
								    				@else
								    					@continue
								    				@endif
							    				@endforeach
							    			@else
							    				@continue
							    			@endif
					    				@endforeach
					    			@else
					    				@continue
					    			@endif
			    				@endforeach
		    				@endforeach
		    			</tbody>
		    			@endif
		    			@endforeach
		    		</table>
		    			
		    	</div>
		    </div>
		   
		    @endif
		</div>   
	
	
@endsection