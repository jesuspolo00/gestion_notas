<?php session_start();
 $_SESSION['menu']=2;
 $_SESSION['active']="registrar";  
 ?>

@extends('plantilla.estructura')

@section('title','inicio')
@section('pagina','boletin')
@section('subtitulo','Registrar Estudiante')
@section('script')
	
@endsection
@section('body')
		<a href="{{ route('estudiantes.incompletos') }}" style="float: right; margin-bottom: 10px;" title="Terminar Registro" class="btn btn-default"><span class="glyphicon glyphicon-info-sign"></span></a>
	<div class="row" style="margin-top: 40px;">

		<form class="form-horizontal" action="{{ route('estudiantes.store')}}" method="POST" enctype="multipart/form-data">
		{{ csrf_field() }}
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Nombre</label>
		    <div class="col-sm-8 col-lg-5">
		      <input type="text" class="form-control" name="nombre" placeholder="Nombre" required>
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Apellido</label>
		    <div class="col-sm-8 col-lg-5">
		      <input type="text" class="form-control" name="apellido" placeholder="Apellido" required>
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Tipo Documento</label>
		    <div class="col-sm-8 col-lg-5">
		      	<select class="form-control" name="tdocumento" required>
		      	  <option value="2" selected>T.I</option>
				  <option value="1">C.C</option>				  
				  <option value="3">R.C</option>
				</select>
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Documento</label>
		    <div class="col-sm-8 col-lg-5">
		      <input type="number" class="form-control" name="documento" maxlength="20" placeholder="Documento" required>
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Genero</label>
		    <div class="col-sm-8 col-lg-5">
		      <select class="form-control" name="genero" required>
				  <option value="1">Masculino</option>
				  <option value="2">Femenino</option>				  
				</select> 
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Tipo Sangre</label>
		    <div class="col-sm-8 col-lg-5">
		      <select class="form-control" name="tipo" required>
				  <option value="1"><var>A</var><sup>+</sup></option>
				  <option value="2"><var>A</var><sup>-</sup></option>
				  <option value="3"><var>B</var><sup>+</sup></option>
				  <option value="4"><var>B</var><sup>-</sup></option>
				  <option value="5"><var>AB</var><sup>+</sup></option>
				  <option value="6"><var>AB</var><sup>-</sup></option>
				  <option value="7"><var>O</var><sup>+</sup></option>
				  <option value="8"><var>O</var><sup>-</sup></option>
				</select>
		    </div>
		  </div>
          <div class="form-group">
		    <label class="col-sm-3 control-label">Foto</label>
		    <div class="col-sm-8 col-lg-5">
		      <input type="file" name="foto" class="form-control">
		    </div>
		  </div>

		  <div class="form-group">
		    <div class="col-sm-offset-3 col-sm-9 col-lg-6">
		      <button type="submit" class="btn btn-default">Registrar</button>
		    </div>
		  </div>
		</form>
	</div>
@endsection