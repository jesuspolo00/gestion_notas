<?php session_start();
 $_SESSION['menu']=7;
 $_SESSION['active']="general";  
 ?>

@extends('plantilla.estructura')

@section('title','Configuracion')
@section('pagina','boletin')
@section('subtitulo','configuracion')

@section('body')
	<div>

	  <!-- Nav tabs -->
	  <ul class="nav nav-tabs" id="mytab" role="tablist">
	    <li role="presentation" class="active"><a href="#index" aria-controls="index" role="tab" data-toggle="tab">Configuracion</a></li>
	    <li role="presentation"><a href="#nivel" aria-controls="nivel" role="tab" data-toggle="tab">Nivel</a></li>
	    <li role="presentation"><a href="#boletin" aria-controls="boletin" role="tab" data-toggle="tab">Boletin</a></li>
	    <li role="presentation"><a href="#horario" aria-controls="horario" role="tab" data-toggle="tab">Horario</a></li>
	    <li role="presentation"><a href="#habilitar" aria-controls="habilitar" role="tab" data-toggle="tab">Habilitar Nivel</a></li>
	    <li role="presentation"><a href="#generar" aria-controls="generar" role="tab" data-toggle="tab">Generar Boletines</a></li>
	  </ul>

	  <!-- Tab panes -->
	  <div class="tab-content">
	  <!-- configuracion -->
	    <div role="tabpanel" class="tab-pane active" id="index">
	    	<div class="row">
				<form class="form-horizontal" action="{{ route('configuracion.colegio')}}" enctype="multipart/form-data" method="POST">
				{{ csrf_field() }}
				  <div class="form-group">
				    <label class="col-sm-3 control-label">Nombre Del Colegio</label>
				    <div class="col-sm-8 col-lg-5">
				      <input type="text" class="form-control" name="nombre" value="<?php 
				      	if($colegio != null){ echo $colegio->nombre; } ?>" placeholder="Nombre Del Colegio" required="">
				    </div>
				  </div>
				  <div class="form-group">
				    <label class="col-sm-3 control-label">Sigla Del Colegio</label>
				    <div class="col-sm-8 col-lg-5">
				      <input type="text" class="form-control" name="sigla" value="<?php if($colegio != null){ echo $colegio->sigla;} ?>" placeholder="Sigla Del Colegio">
				    </div>
				  </div>				 
				  <div class="form-group">
				    <label class="col-sm-3 control-label">Año Escolar</label>
				    <div class="col-sm-4 col-lg-2">
				    <?php  if($colegio != null){ 
				    		if($colegio->año_escolar !=""){ ?>
				    		<select class="form-control" name="año" id="año" readonly>
						    <?php  echo "<option selected>$colegio->año_escolar</option>"; ?>
							</select>
				    		<?php }else{ ?>
				    			<select class="form-control" name="año" id="año" required="">
							    @for($i = date('Y'); $i >= 1900; $i--)
								    <option>{{ $i }}</option>
								@endfor
								</select>
					    	<?php } ?>
					<?php }else{ ?>
						<select class="form-control" name="año" id="año" required="">
					    @for($i = date('Y'); $i >= 1900; $i--)
						    <option>{{ $i }}</option>
						@endfor
						</select>
					<?php } ?>
				    </div>
				    <div class="col-sm-4 col-lg-2">
				    <input type="text" class="form-control" id="año1" disabled="">
				    </div>
				  </div>
				  <div class="form-group">
				    <label class="col-sm-3 control-label">Periodo Escolar</label>
				    <div class="col-sm-8 col-lg-5">
				    	<?php  if($colegio != null){ 
				    		if($colegio->periodo_escolar !=""){ ?>
				      			<select class="form-control" name="periodo" readonly>
						      		<option value="<?php echo $colegio->periodo_escolar; ?>"><?php echo $colegio->periodo_escolar; ?></option>
								</select>
						<?php }else{ ?>
								<select class="form-control" name="periodo" required="">
								  <option value="1">1</option>
								  <option value="2">2</option>
								  <option value="3">3</option>
								</select>
						<?php } ?>
					<?php }else{ ?>
							<select class="form-control" name="periodo" required="">
						  <option value="1">1</option>
						  <option value="2">2</option>
						  <option value="3">3</option>
						</select>
					<?php } ?>
				    </div>
				  </div>
				  <div class="form-group">
				    <div class="col-sm-offset-3 col-sm-9 col-lg-6">
				      <button type="submit" class="btn btn-default">Guardar</button>
				    </div>
				  </div>
				</form>
			</div>
	    </div>
	    
	    <!-- cursos -->
	    <div role="tabpanel" class="tab-pane" id="nivel">
	    	<div class="row">
	    		<div class="col-xs-12">
	    			<a href="{{ route('nivel.categoria') }}" style="float: right;" class="btn btn-default">Categoria</a>
	    		</div>
	    		@foreach($curso as $cursos)
	    			<form class="form-horizontal" action="{{ route('cursos.editar',$cursos->id)}}" method="POST">
	    			{{ csrf_field() }}
			    		<div class="form-group">
						    <label class="col-sm-2 control-label">Nivel</label>
						    <div class="col-sm-5">
						      <input type="text" class="form-control" name="nombre" id="input_curso{{$cursos->id}}" value="{{ $cursos->nombre }}" disabled required="">
						    </div>
						    <div class="col-sm-2">
						    	<select class="form-control" name="nivel" disabled="" id="nivel{{$cursos->id}}">
						    		@foreach($categoria as $categorias)
						    			@if($cursos->id_categoria == $categorias->id)
						    				<option value="{{ $categorias->id}}" selected="">{{ $categorias->nombre }}</option>
						    			@else
						    				<option value="{{ $categorias->id }}">{{ $categorias->nombre }}</option>
						    			@endif
						    		@endforeach
						    	</select>
						    </div>
						    <div class="col-xs-3">
						    	<a href="{{ route('cursos.detalle',$cursos->id) }}" class="btn btn-default" title="Configurar Curso"><span class="glyphicon glyphicon-cog"></span></a>
						    	<button type="button" id="editcurso{{ $cursos->id }}" class="btn btn-default" onclick="editar_curso({{$cursos->id}})"><span class="glyphicon glyphicon-edit"></span></button>
							    <button id="cursado{{$cursos->id}}" type="submit" class="btn btn-default hidden"><span class="glyphicon glyphicon-ok"></span></button>
							    <a href="{{ route('cursos.eliminar',$cursos->id)}}" class="btn btn-default"><span class="glyphicon glyphicon-trash"></span></a>
						    </div>
						 </div>
					</form>
					 @endforeach
				<form class="form-horizontal" action="{{ route('configuracion.cursos')}}" method="POST">
				{{ csrf_field() }}
				  <div id="contenedor" class="col-xs-12">
					  <div id="curso1" class="form-group">
					    <label class="col-sm-2 control-label">Nivel 1</label>
					    <div class="col-sm-5">
					      <input type="text" class="form-control" required="" name="curso[]" placeholder="Ingrese Un Nivel">
					    </div>
					    <div class="col-sm-2">
					      <select id="ni" class="form-control" name="categoria[]">
					      		@foreach($categoria as $cate)
					      			<option value="{{$cate->id}}">{{$cate->nombre}}</option>
					      		@endforeach
					      </select>
					    </div>
					    <div id="botones" class="col-xs-3">
					    <button type="button" class="btn btn-default" onclick="quitarCurso();">-</button>
					    <button type="button" class="btn btn-default" onclick="agregarCurso();">+</button>
					    </div>
					  </div>
				  </div>

				  <div class="form-group">
				    <div class="col-sm-offset-3 col-xs-offset-1 col-sm-9 col-lg-6">
				      <button type="submit" class="btn btn-default">Guardar</button>
				    </div>
				  </div>
				</form>
			</div>
	    </div>
	    <!-- horario -->
	    <div role="tabpanel" class="tab-pane" id="horario">
	    	<div class="col-sm-offset-2">
	    	
		    	<form class="form-inline" method="get" action="{{ route('configuracion.index','horario') }}">
		    		{{ csrf_field() }}
				  <div class="form-group">
				    <label>curso</label>
				    <select class="form-control" name="curso">
				    @foreach($curso as $cursos)
				    	@if($selected!="")
				    		@if($selected['curso']==$cursos->id)
				    		<option selected="" value="{{$cursos->id}}">{{ $cursos->nombre }}</option>
				    		@endif
				    	@endif
					  <option value="{{$cursos->id}}">{{ $cursos->nombre }}</option>					  
					  @endforeach
					</select>
				  </div>
				  <div class="form-group">
				    <label>periodo</label>
				    <select class="form-control" name="periodo">
					    @if($selected!="")
				    		<option selected="">{{ $selected['periodo'] }}</option>
				    	@endif
					  <option>1</option>
					  <option>2</option>
					  <option>3</option>
					  <option>4</option>
					</select>
				  </div>
				  <div class="form-group">
				    <label>año</label>
				    <select class="form-control" name="año">
				    @if($selected!="")
				    		<option selected="">{{ $selected['año'] }}</option>
				    @endif
				    @for($i = date('Y'); $i >= 1900; $i--)
					    <option>{{ $i }}</option>
					@endfor
					</select>
				  </div>	  
				  <button type="submit" name="boton" value="buscar" class="btn btn-default">Buscar</button>	
				  <button type="submit" name="boton" value="guardar" class="btn btn-default">Guardar</button>
				<a href="{{ route('horario') }}" style="float: right;" class="btn btn-default"><span class="glyphicon glyphicon-cog"></span></a>
			</div>
			
			<!-- **tabla** -->
			<div class="table-responsive" style="margin-top: 15px;">
                <table  class="display table table-bordered table-striped" >
                  <thead>
                    <tr>
                      <th>Hora</th>
                      <th>Lunes</th>
                      <th>Martes</th>
                      <th>Miercoles</th>
                      <th>Jueves</th>
                      <th>Viernes</th>
                      <th>Sabados</th>
                    </tr>
                  </thead>
                  <tbody>
               
                	@foreach($hora as $horas)
                		<tr class="gradeX">
                      	<td>{{ $horas->hora}}</td>
	                	@foreach($dia as $dias)
	                		<?php $veri=0; ?>
	                		<td>
	                      		<select class="form-control" name="{{$horas->id}}-{{$dias->id}}" >
	                      			@foreach($horario as $horarios)
	                      				@if($horarios->dia_id==$dias->id and $horarios->hora_id==$horas->id)
	                      					<option selected value="{{ $horarios->materia }}">{{ $horarios->materia}}</option>
	                      					<?php $veri=1; ?>
	                      				@endif
									@endforeach
									<?php if($veri==0){
										echo "<option selected=''></option>";
									}
									?>									
									@foreach($materia as $materias)
									  <option value="{{ $materias->nombre }}">{{ $materias->nombre }}</option>	
									@endforeach
								</select>
							</td>
	                	@endforeach
	                	</tr>  
	              	@endforeach
                      
                  </tbody>

                </table>
              </div><!--/table-responsive-->
			</form> 
	    </div>
	    <!-- boletin -->
	    <div role="tabpanel" class="tab-pane" id="boletin">
				<form class="form-horizontal" action="{{route('configuracion.boletin')}}"  method="POST">
				{{ csrf_field() }}
				  <div class="form-group">
				    <label class="col-sm-3 control-label">Codigo Del Boletin</label>
				    <div class="col-sm-8 col-lg-5">
				      <input type="text" class="form-control" name="codigo" required="" value="<?php 
				      	if($colegio != null){ echo $colegio->boletin_codigo; } ?>" placeholder="Codigo Del Boletin">
				    </div>
				  </div>
				  <div class="form-group">
				    <label class="col-sm-3 control-label">Año Lectivo</label>
				    <div class="col-sm-8 col-lg-5">
				      <input type="text" class="form-control" name="lectivo" required="" value="<?php 
				      	if($colegio != null){ echo $colegio->boletin_añolectivo; } ?>" placeholder="2017 - 2018">
				    </div>
				  </div>
				  <div class="form-group">
				    <label class="col-sm-3 control-label">Fecha De Entrega</label>
				    <div class="col-sm-8 col-lg-5">
				      <input type="date" class="form-control" name="fecha" required="" value="<?php 
				      	if($colegio != null){ echo $colegio->boletin_fecha; } ?>" placeholder="Fecha De Entrega del Boletin">
				    </div>
				  </div>				 
				  
				  <div class="form-group">
				    <div class="col-sm-offset-3 col-sm-9 col-lg-6">
				      <button type="submit" class="btn btn-default">Guardar</button>
				    </div>
				  </div>
				</form>
			</div>
			<div role="tabpanel" class="tab-pane" id="habilitar">
	  			<div class="col-xs-12">
	    			<form class="form-horizontal" action="{{ route('configuracion.habilitar')}}" method="POST">
	  				@foreach($curso as $cursos)
	    			{{ csrf_field() }}
			    		<div class="form-group">
						    <label class="col-xs-3 control-label">{{ $cursos->nombre }}</label>
						    @if($cursos->activo==0)
						    <div class="col-xs-1 col-xs-offset-1">
							    <div class="radio">
							    	<label class="control-label">
							      	<input type="radio" name="c{{$cursos->id}}" checked value="0" required="">Si
							      	</label>
							     </div>
							</div>
							<div class="col-xs-3">
							    <div class="radio">
						    	<label class="control-label">
							      <input type="radio" name="c{{$cursos->id}}" value="1" required="">No
							      </label>
								</div>
						    </div>
						    @elseif($cursos->activo==1)
						    	<div class="col-xs-1 col-xs-offset-1">
							    <div class="radio">
							    	<label class="control-label">
							      	<input type="radio" name="c{{$cursos->id}}" value="0" required="">Si
							      	</label>
							     </div>
							</div>
							<div class="col-xs-3">
							    <div class="radio">
						    	<label class="control-label">
							      <input type="radio" name="c{{$cursos->id}}" checked value="1" required="">No
							      </label>
								</div>
						    </div>
						   @else
							    <div class="col-xs-1 col-xs-offset-1">
								    <div class="radio">
								    	<label class="control-label">
								      	<input type="radio" disabled="" name="c{{$cursos->id}}" value="0" required="">Si
								      	</label>
								     </div>
								</div>
								<div class="col-xs-3">
								    <div class="radio">
							    	<label class="control-label">
								      <input type="radio" disabled="" name="c{{$cursos->id}}" required="" value="1">No
								      </label>
									</div>
							    </div>
						 @endif
							 </div>
					 @endforeach
					 	<div class="col-xs-offset-5">	
						 	<div class="form-group">
							 	<button type="submit" class="btn btn-primary">Guardar</button>
							 </div>
						 </div>
					</form>
	  			</div>
	  		</div>
	  		<div role="tabpanel" class="tab-pane" id="generar">
	  			<div class="row"> 
				  <div class="col-xs-12 col-sm-10" style="margin-bottom: 10px;">
				    <div class="dataTables_filter" id="dynamic-table_fil">
				      <form method="GET" action="{{ route('configuracion.index','generar') }}" class="form-inline">
				         <div class="form-group">
				          <label style="" class="control-label" style="margin-bottom: 0;vertical-align: middle;">Buscar</label>
				          <select class="form-control" name="generar">
				          		<option value="">Seleccione una opcion..</option>
				          		<option value="1">Notas entregadas</option>
				          		<option value="0">Notas NO entregadas</option>
				          		<option value="2">Boletines generados</option>
				          </select>
				         <button type="submit" name="boletin" value="boletin" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
				         </div>
				      </form>
				      </div>
				    </div>
				  </div>
					<div class="table-responsive">
				    <table  class="display table table-bordered table-striped" >
				      <thead>
				        <tr>
				          <th width="50%">Nivel</th>
				          <th style="width: 25%" >Estado</th>
				          <th></th>
				        </tr>
				      </thead>
				      <tbody>
				        @foreach($generar as $generados)
				        	
				        		
					            <tr class="gradeX">
					              <td>{{ $generados->nombre}}</td>                         
					              @if($generados->activo==0)
					              	<td>Notas NO entregadas</td>
					              	<td></td>
					              @elseif($generados->activo==1)
					            	<form action="{{route('boletin.generar',$generados->id)}}" method="POST">
					            		{{ csrf_field() }}
						            	<td>Notas entregadas</td>
						            	<td>
							                <center>
							                  <button type="submit"  class="btn btn-primary" title="Generar Boletin">Generar boletin</button>
							                </center>
							              </td>
						            </form>
					              @elseif($generados->activo==2)
					              	<td>Boletines generados</td>
					              	<td></td>
					              @endif
					              
					            </tr>
				            
				        @endforeach
				      </tbody>
				    </table>
				   
				  </div><!--/table-responsive-->
	  		</div>	

	   	</div>
	  </div>
	</div>
	
@endsection
@section('script')
	<script type="text/javascript"> 
	$( document ).ready(function() {
  		var tab = "<?php if(isset($tab)){echo $tab;} ?>";
  		if(tab != ""){
  			$("#mytab a[href='#"+tab+"']").tab('show');
  		}
  		$( "#año" ).change(function () {
		var str = "";
		$( "#año option:selected" ).each(function() {
			str += $(this).val();
		});
		str=parseInt(str)+1;
		$("#año1").val(str);
		
		})
		.change();
	});
	var cont=1;
	var numero=1;
	function agregarCurso(){
		$( "#botones" ).remove();
		var ni =$("#ni").html();
		cont++;
		$('#contenedor').append('<div id="curso'+cont+'" class="form-group">'
					    +'<label class="col-sm-2 control-label">Nivel '+cont+'</label>'
					    +'<div class="col-sm-5">'
					    +'<input type="text" class="form-control" name="curso[]" placeholder="Ingrese Un Nivel">'
					    +'</div>'
					    +'<div class="col-sm-2">'
					      +'<select id="ni" class="form-control" name="categoria[]">'+ni
					      +'</select>'
					    +'</div>'
					    +'<div id="botones" class="col-xs-3">'
					    +'<button type="button" class="btn btn-default" onclick="quitarCurso();">-</button>'
					    +'<button type="button" class="btn btn-default" onclick="agregarCurso();">+</button>'
					    +'</div>'
					 +'</div>');
	}
	function quitarCurso(){
		if(cont<=1){

		}else{			
		$("#curso"+cont).remove();	
		cont--;		
		$("#curso"+cont).append('<div id="botones" class="col-xs-3">'
					    +'<button type="button" class="btn btn-default" onclick="quitarCurso();">-</button>'
					    +'<button type="button" class="btn btn-default" onclick="agregarCurso();">+</button>'
					    +'</div>');
		}
		
	}
	function agregarMateria(){
		$( "#botones1" ).remove();
		numero++;
		$('#contenedormateria').append('<div id="materia'+numero+'" class="form-group">'
					    +'<label class="col-sm-2 control-label">Materia '+numero+'</label>'
					    +'<div class="col-sm-5">'
					    +'<input type="text" class="form-control" name="materia1[]" placeholder="Ingrese Una Materia">'
					    +'</div>'
					     +'<div class="col-sm-2">'
					    +'	<select class="form-control" name="tipo_materia1[]">'
					    +'		<option value="cualitativo" selected="">Cualitativo</option>'
					    +'		<option value="cuantitativo">Cuantitativo</option>'
					    +'	</select>'
					    +'</div>'
					    +'<div id="botones1" class="col-xs-3">'
					    +'<button type="button" class="btn btn-default" onclick="quitarMateria();">-</button>'
					    +'<button type="button" class="btn btn-default" onclick="agregarMateria();">+</button>'
					    +'</div>'
					 +'</div>');
	}
	function quitarMateria(){
		if(numero<=1){

		}else{			
		$("#materia"+numero).remove();	
		numero--;		
		$("#materia"+numero).append('<div id="botones1" class="col-xs-3">'
					    +'<button type="button" class="btn btn-default" onclick="quitarMateria();">-</button>'
					    +'<button type="button" class="btn btn-default" onclick="agregarMateria();">+</button>'
					    +'</div>');
		}
		
	}
	function editar(id){		
		$("#input"+id).removeAttr('disabled'); 
		$("#edit"+id).addClass('hidden');
		$("#editado"+id).removeClass('hidden');
	}
	function editar_curso(id){		
		$("#input_curso"+id).removeAttr('disabled'); 
		$("#editcurso"+id).addClass('hidden');
		$("#cursado"+id).removeClass('hidden');
	}



	</script>
@endsection