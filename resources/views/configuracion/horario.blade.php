<?php session_start();
 $_SESSION['menu']=7;
 $_SESSION['active']="general";  
 ?>

@extends('plantilla.estructura')

@section('title','Horario')
@section('pagina','boletin')
@section('subtitulo','Horario')
@section('body')
  <form method="POST" action="{{ route('horario.store') }}">
  {{ csrf_field() }}
    <div class="row">
        <div class="col-xs-4">
          <center>
            @foreach($hora as $horas)
              @if($horas->id >= 1 and $horas->id <= 8)
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" name="h{{ $horas->id }}" <?php if($horas->disponible == 1){ echo "checked='checked'"; } ?>>
                      </label>
                      <label>{{ $horas->hora }}</label>
                    </div>
              @endif
            @endforeach
          </center>
        </div>
        <div class="col-xs-4" style="border-left: 1px solid #ddd; border-right: 1px solid #ddd;">
          <center>
            @foreach($hora as $horas)
              @if($horas->id >= 9 and $horas->id <= 16)
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" name="h{{ $horas->id }}" <?php if($horas->disponible == 1){ echo "checked='checked'"; } ?>>
                      </label>
                      <label>{{ $horas->hora }}</label>
                    </div>
              @endif
            @endforeach
          </center>
        </div>
        <div class="col-xs-4">
          <center>
            @foreach($hora as $horas)
              @if($horas->id >= 17 and $horas->id <= 24)
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" name="h{{ $horas->id }}" <?php if($horas->disponible == 1){ echo "checked='checked'"; } ?>>
                      </label>
                      <label>{{ $horas->hora }}</label>
                    </div>
              @endif
            @endforeach
          </center>
        </div>
        <div class="form-group col-xs-offset-2">
          <button type="subtmit" class="btn btn-default">Guardar</button>
          <a href="{{ route('configuracion.index','horario') }}" class="btn btn-default">Volver</a>
        </div>
    </div>
  </form>
	
@endsection