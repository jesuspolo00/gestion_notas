<?php session_start();
 $_SESSION['menu']=7;
 $_SESSION['active']="matricula";  
 ?>

@extends('plantilla.estructura')

@section('title','inicio')
@section('pagina','boletin')
@section('subtitulo','Promover estudiantes')
@section('script')
	<script src="{{ asset('plugins/data-tables/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('plugins/data-tables/DT_bootstrap.js') }}"></script>
	<script src="{{ asset('plugins/data-tables/dynamic_table_init.js') }}"></script>
	<script src="{{ asset('plugins/edit-table/edit-table.js') }}"></script>
@endsection
@section('body')
  @if(Auth::User()->validar('4'))
	<div class="row"> 
  <div class="col-xs-12 col-sm-10" style="margin-bottom: 10px;">
    <div class="dataTables_filter" id="dynamic-table_fil">
      <form method="GET" action="{{ route('configurar.matricula') }}" class="form-inline">
         <div class="form-group">
          <label style="" class="control-label" style="margin-bottom: 0;vertical-align: middle;">Buscar</label>
          <select class="form-control" name="nivel">
              @foreach($curso as $cursos)
                  @if($old !="" and $old==$cursos->id)
                    <option value="{{ $cursos->id }}" selected="">{{ $cursos->nombre }}</option> 
                  @else
                    <option value="{{ $cursos->id }}">{{ $cursos->nombre }}</option> 
                  @endif
              @endforeach
          </select>
         <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
         </div>
      </form>
      </div>
    </div>
  </div>
  @endif
  @if($old!="")
    @if($estudiante->isNotEmpty())
  	<div class="table-responsive">
      <form method="POST" action="{{ route('matricula.habilitar',$old) }}" class="form-inline">
        {{ csrf_field() }}
      <table class="display table table-bordered table-striped table-hover" >
        <thead>
          <tr>
            <th style="width: 3%;"></th>
            <th>Nombre</th>
            <th>Documento</th>
            <th>Nivel</th>
          </tr>
        </thead>
        <tbody>

            @foreach($estudiante as $estudiantes)
              <tr class="gradeX">
                <td><input type="checkbox" name="{{ $estudiantes->matricula }}" ></td>
                <td>{{ $estudiantes->estudiante }} {{ $estudiantes->apellido }}</td>                         
                <td>
                  @if($estudiantes->tipo_documento==1)
                    C.C
                    @elseif($estudiantes->tipo_documento==2)
                    T.I
                    @elseif($estudiantes->tipo_documento==3)
                    R.C
                    @endif
                  {{ $estudiantes->documento }}</td>
                <td>{{ $estudiantes->nombre }}</td>
              </tr>
            @endforeach
        </tbody>
      </table>
        <div style="float: right; margin-bottom: 5px;">
          <div class="form-group" >
            <select class="form-control" name="promover">
                @foreach($curso as $cursos)
                    @if($old!=$cursos->id)
                      <option value="{{ $cursos->id }}">{{ $cursos->nombre }}</option>
                    @endif
                @endforeach
            </select>
          </div>
            <button type="submit" class="btn btn-primary">Promover</button>
        </div>
      </form>
    </div><!--/table-responsive-->
    @else
      <div class="alert alert-danger" role="alert">
        <p>No Hay Resultados Disponibles!</p>
      </div>
    @endif
  @else
    <div class="alert alert-danger" role="alert">
      <p>No Hay Resultados Disponibles!</p>
    </div>
  @endif
@endsection