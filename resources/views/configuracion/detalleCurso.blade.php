<?php session_start();
 $_SESSION['menu']=7;
 $_SESSION['active']="general";  
 ?>

@extends('plantilla.estructura')

@section('title','inicio')
@section('pagina','boletin')
@section('subtitulo','Configuracion Del Curso')
@section('body')
	<center><h2>{{ $curso->nombre }}</h2></center>
	<!-- Nav tabs -->
  
  <!-- Tab panes -->
  
    
    	<div class="row">		
		<form class="form-horizontal" action="{{ route('detalle.registrar',$curso->id) }}" method="POST">
		{{ csrf_field() }}

		  <div class="form-group">
		    <label class="col-sm-3 control-label">Director De Grupo</label>
		    <div class="col-sm-8 col-lg-5">
		      <input type="text" class="form-control" name="director" required="" value="<?php if($detalle->isNotEmpty()){ echo $detalle[0]->director_grupo; } ?>" placeholder="Director De Grupo">
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Tipo Calificacion</label>
		    <div class="col-sm-8 col-lg-5">
		      	<select class="form-control" name="tcalificacion" required="">
		      	  @if($detalle->isEmpty())
		      	  <option value="cualitativo" selected>Cualitativo</option>
				  	<option value="cuantitativo">Cuantitativo</option>
				  @else
			      	  @if($detalle[0]->tipo_calificacion == 'cualitativo')
			      	  <option value="cualitativo" selected>Cualitativo</option>
					  <option value="cuantitativo">Cuantitativo</option>
					  @elseif($detalle[0]->tipo_calificacion == 'cuantitativo')
					  	<option value="cualitativo">Cualitativo</option>
					    <option value="cuantitativo" selected>Cuantitativo</option>
					  	@endif
				  @endif	
				</select>
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Numero De Estudiantes</label>
		    <div class="col-sm-8 col-lg-5">
		      <input type="number" class="form-control" name="numero" required="" value="<?php if($detalle->isNotEmpty()){ echo $detalle[0]->numero_estudiantes; }?>" placeholder="Numero De Estudiantes">
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Nivel De Formacion</label>
		    <div class="col-sm-8 col-lg-5">
		      <input type="text" class="form-control" name="nivel" required="" value="<?php if($detalle->isNotEmpty()){ echo $detalle[0]->nivel_formacion; } ?>" placeholder="Nivel De Formacion" required>
		    </div>
		  </div>
		  <div class="form-group">
		    <div class="col-sm-offset-3 col-sm-9 col-lg-6">
		      <button type="submit" class="btn btn-default">Guardar</button>
		    </div>
		  </div>
		</form>
	</div>
    
	
@endsection
@section('script')
	<script type="text/javascript">
	var numero = 1;
	function agregarMateria(){
		$( "#botones1" ).remove();
		numero++;
		var select = $("#select1").html();
		$('#contenedormateria').append('<div id="materia'+numero+'" class="form-group">'
					    +'<label class="col-sm-3 control-label">Materia '+numero+'</label>'
					    +'<div class="col-sm-5">'
					    +'<select class="form-control" name="materia[]">'
					    +select
					    +'</select>'
					    +'</div>'
					    +'<div id="botones1" class="col-xs-3">'
					    +'<button type="button" class="btn btn-default" onclick="quitarMateria();">-</button>'
					    +'<button type="button" class="btn btn-default" onclick="agregarMateria();">+</button>'
					    +'</div>'
					 +'</div>');
	}
	function quitarMateria(){
		if(numero<=1){

		}else{			
		$("#materia"+numero).remove();	
		numero--;		
		$("#materia"+numero).append('<div id="botones1" class="col-xs-3">'
					    +'<button type="button" class="btn btn-default" onclick="quitarMateria();">-</button>'
					    +'<button type="button" class="btn btn-default" onclick="agregarMateria();">+</button>'
					    +'</div>');
		}
		
	}
	</script>
@endsection