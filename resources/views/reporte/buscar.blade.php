<?php session_start();
 $_SESSION['menu']=9;
 $_SESSION['active']="reporte_buscar";  
 ?>

@extends('plantilla.estructura')

@section('title','inicio')
@section('pagina','boletin')
@section('subtitulo','Buscar Boletin')
@section('script')
	<script src="{{ asset('plugins/data-tables/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('plugins/data-tables/DT_bootstrap.js') }}"></script>
	<script src="{{ asset('plugins/data-tables/dynamic_table_init.js') }}"></script>
	<script src="{{ asset('plugins/edit-table/edit-table.js') }}"></script>
  
@endsection
@section('body')
	<div class="row"> 
  <div class="col-xs-12 col-sm-10" style="margin-bottom: 10px;">
    <div class="dataTables_filter" id="dynamic-table_fil">
      <form method="GET" action="{{ route('reportes.buscar') }}" class="form-inline">
         <div class="form-group">
          <input class="form-control" name="estudiante" style="display: inline-block;width: auto;vertical-align: middle;" placeholder="nombre del estudiante" type="text">
          <select class="form-control" name="curso">
              <option value="">Seleccione una opcion..</option>
              @foreach($curso as $cursos)
                <option value="{{$cursos->nombre}}">{{$cursos->nombre}}</option>
              @endforeach
          </select>
          <select class="form-control" name="periodo">
              <option value="">Seleccione una opcion..</option>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
          </select>
          <select class="form-control" name="año">
              <option value="">Seleccione una opcion..</option>
              @foreach($año as $años)
                <option value="{{ $años->año }}-{{ $años->año+1 }}">{{ $años->año }} - {{ $años->año+1 }}</option>
              @endforeach
          </select>
         <button type="submit" name="buscar" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
         </div>
      </form>
      </div>
    </div>
  </div>
	<div class="table-responsive">
    <table class="display table table-bordered table-striped">
      <thead>
        <tr>                      
          <th>Nombre</th>
          <th>nivel</th>
          <th>periodo</th>
          <th>año</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
          @foreach($boletin as $boletines)
              <tr>
                  <td>{{ $boletines->nombre }}</td>
                  <td>{{ $boletines->nivel }}</td>
                  <td>{{ $boletines->periodo }}</td>
                  <td>{{ $boletines->año_lectivo }}</td>
                  <td><a href="{{ route('boletin',$boletines->id) }}" class="btn btn-primary" target="_black">Descargar</a></td>
              </tr>
          @endforeach
      </tbody>
      {{ $boletin->links() }}
    </table>
  </div><!--/table-responsive-->
@endsection