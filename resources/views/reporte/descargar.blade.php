<?php session_start();
 $_SESSION['menu']=9;
 $_SESSION['active']="reporte_descargar";  
 ?>

@extends('plantilla.estructura')

@section('title','inicio')
@section('pagina','boletin')
@section('subtitulo','Boletin Por Cursos')
@section('script')
	<script src="{{ asset('plugins/data-tables/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('plugins/data-tables/DT_bootstrap.js') }}"></script>
	<script src="{{ asset('plugins/data-tables/dynamic_table_init.js') }}"></script>
	<script src="{{ asset('plugins/edit-table/edit-table.js') }}"></script>
@endsection
@section('body')
	<div class="row">
    
  <div class="col-xs-12 col-sm-10" style="margin-bottom: 10px;">
    <div class="dataTables_filter" id="dynamic-table_fil">
      <h5>Descarga los boletines por cursos <b>nota:</b> solo aparecen los cursos ya con boletines generados</h5>
      </div>
    </div>
  </div>
	<div class="table-responsive">
    <table class="display table table-bordered table-striped">
      <thead>
        <tr>                      
          <th>Nombre</th>
          <th style="width: 30%" ></th>
        </tr>
      </thead>
      <tbody>
        @foreach($curso as $cursos)
            <tr class="gradeX">
              <td>{{ $cursos->nombre }}</td>
              <td><a href="{{ route('boletin.descargar',$cursos->id) }}" class="btn btn-primary" target="_black">Descargar</a></td>
            </tr>
        @endforeach
      </tbody>
    </table>
    {{ $curso->links() }}
  </div><!--/table-responsive-->
	
@endsection