<!DOCTYPE html>
<html>
  <head>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.5, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>@yield('title')</title>
    <link rel="shortcut icon" href="{{ asset('images/logo.ico') }}">
    <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
    <link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet" type="text/css" />
    @yield('head')

  </head>
  @if(Auth::User()->color != "")
  <body class="{{ Auth::User()->color }}">
  @else
  <body class="fixed_header atm-spmenu-push light_theme red_thm left_nav_fixed">
  @endif
  <div class="wrapper">
    <!--\\\\\\\ wrapper Start \\\\\\-->
    <div class="header_bar">
      <!--\\\\\\\ header Start \\\\\\-->
      <div class="brand">
        <!--\\\\\\\ brand Start \\\\\\-->
        
        <div class="logo" style="display:block"><span class="theme_color"><img src="{{ asset('images/logo.png') }}" style="height: 30px;"></span> 
@if(Auth::User()->rol =="1")
          Acudiente 
          @elseif(Auth::User()->rol =="2")
          Docente
          @elseif(Auth::User()->rol =="3")
          Recaudo
          @elseif(Auth::User()->rol =="4")
          Secretaria
          @elseif(Auth::User()->rol =="5")
          Admin
          @endif
</div>
        
      </div>
      <!--\\\\\\\ brand end \\\\\\-->
      <div class="header_top_bar">
        <!--\\\\\\\ header top bar start \\\\\\-->
        
        <a href="javascript:void(0);" class="menutoggle"> <i class="fa fa-bars"></i> </a>
        <!--<div class="menu_center"><center>@yield('pagina')</center></div>-->

        <form action="{{ route('logout') }}" method="POST">
        {{ csrf_field() }}
        <div class="top_right_bar hidden-xs">
          <div class="top_right">
            <div class="top_right_menu">
                <div class="user_admin dropdown"> <a href="javascript:void(0);" data-toggle="dropdown"><span class="user_adminname">{{Auth::User()->usuario}}</span> <b class="caret"></b> </a>
                
                <ul class="dropdown-menu">
                  <div class="top_pointer"></div>
                  <li> <a href="{{ route('usuario.perfil') }}"><i class="fa fa-user"></i> Perfil</a> </li>
                  <li> <a href="#ayuda" data-toggle="modal"><i class="fa fa-question-circle"></i> Ayuda</a> </li>
                  <li> <button type="submit" style="text-decoration: none;border: none;background-color: #fff;padding-left: 20px;"><i class="fa fa-power-off"></i> Cerrar Session</button> </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
         </form>
      </div>
      <!--\\\\\\\ header top bar end \\\\\\-->
    </div>
    <!--Modal Ayuda-->
    <div class="modal fade" id="ayuda">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <div style="width: 100%; height: 100%;">
        <img src="{{ asset('images/ayuda.png') }}" style="width: 100%;height: 40%;">  
        </div>      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog --><!-- /.modal -->
    </div>
    <!--\\\\\\\ header end \\\\\\-->
    <div class="inner">
      <!--\\\\\\\ inner start \\\\\\-->
      <div class="left_nav">
        <!--\\\\\\\left_nav start \\\\\\-->
        <form method="GET" action="{{ route('estudiantes.buscar') }}">
        <div class="search_bar"> <i class="fa fa-search"></i>
          <input type="text" name="resultado" class="search" placeholder="Buscar Estudiantes" />
        </div>
        </form>
        <div class="left_nav_slidebar">
          <ul>
          <!-- menu 1 -->
            @if($_SESSION['menu'] == 1)
              <li class="left_nav_active theme_border"><a href="{{ url('/') }}"><i class="fa fa-home"></i> Inicio <span class="left_nav_pointer"></span> <span class="plus"><i class="fa fa-plus"></i></span> </a>
            
            @else
              <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Inicio <span class="left_nav_pointer"></span> <span class="plus"><i class="fa fa-plus"></i></span> </a>
                
            @endif          
          @if(Auth::User()->validar('4.2.1'))
              </li>
                <!-- menu 2 -->
                 @if($_SESSION['menu'] == 2)
                  <li class="left_nav_active theme_border"><a href="javascript:void(0);"><i class="fa fa-address-book" aria-hidden="true"></i> Estudiantes <span class="left_nav_pointer"></span> <span class="plus"><i class="fa fa-plus"></i></span> </a>
                  <ul class="opened" style="display:block">
                @else
                  <li><a href="javascript:void(0);" class="left_nav_sub_active"><i class="fa fa-address-book" aria-hidden="true"></i> Estudiantes <span class="left_nav_pointer"></span> <span class="plus"><i class="fa fa-plus"></i></span> </a>
                    <ul>
                @endif  
                  @if($_SESSION['active'] =='buscar_estudiante')
                    <li> <a href="{{route('estudiantes.buscar')}}"> <span>&nbsp;</span> <i class="fa fa-circle "></i> <b class="theme_color">Buscar Estudiante</b> </a> </li>
                  @else
                    <li> <a href="{{route('estudiantes.buscar')}}"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Buscar Estudiante</b> </a> </li>
                  @endif
                  @if(Auth::User()->validar('4'))          
                      @if($_SESSION['active'] =='registrar')
                        <li> <a href="{{route('estudiantes.registrar')}}"> <span>&nbsp;</span> <i class="fa fa-circle "></i> <b class="theme_color">Registrar Estudiante</b> </a> </li>
                      @else
                        <li> <a href="{{route('estudiantes.registrar')}}"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Registrar Estudiante</b> </a> </li>
                      @endif  
                  @endif
                  </ul>
                </li>
            @endif
            @if(Auth::User()->validar('2.4'))
                <!-- menu 3 -->
                @if($_SESSION['menu'] == 3)
                  <li class="left_nav_active theme_border"><a href="javascript:void(0);"><i class="fa fa-graduation-cap"></i> Nivel <span class="left_nav_pointer"></span> <span class="plus"><i class="fa fa-plus"></i></span> </a>
                  <ul class="opened" style="display:block">
                @else
                  <li><a href="javascript:void(0);"><i class="fa fa-graduation-cap"></i> Nivel <span class="left_nav_pointer"></span> <span class="plus"><i class="fa fa-plus"></i></span> </a>
                    <ul>
                @endif  
                @if($_SESSION['active'] =='cursos')
                    <li> <a href="{{route('cursos.buscar')}}"> <span>&nbsp;</span> <i class="fa fa-circle "></i> <b class="theme_color">Buscar Nivel</b> </a> </li>
                @else
                  <li> <a href="{{route('cursos.buscar')}}"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Buscar Nivel</b> </a> </li>
                @endif   
                @if(Auth::User()->validar('4'))            
                    @if($_SESSION['active'] =='horario')
                        <li> <a href="{{ route('cursos.horario')}}"> <span>&nbsp;</span> <i class="fa fa-circle "></i> <b class="theme_color">Ver Horario</b> </a> </li>
                    @else
                      <li> <a href="{{ route('cursos.horario')}}"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Ver Horario</b> </a> </li>
                    @endif  
                @endif
                  </ul>
                </li>
            @endif

            @if(Auth::User()->validar('4'))
                <!-- menu 4 -->
                @if($_SESSION['menu'] == 4)
                  <li class="left_nav_active theme_border"><a href="javascript:void(0);"><i class="fa fa-book"></i> Dimenciones <span class="left_nav_pointer"></span> <span class="plus"><i class="fa fa-plus"></i></span> </a>
                  <ul class="opened" style="display:block">
                @else
                  <li><a href="javascript:void(0);"><i class="fa fa-book"></i> Dimenciones <span class="left_nav_pointer"></span> <span class="plus"><i class="fa fa-plus"></i></span> </a>
                    <ul>
                @endif  
                @if($_SESSION['active'] =='dimenciones')
                    <li> <a href="{{route('dimencion')}}"> <span>&nbsp;</span> <i class="fa fa-circle "></i> <b class="theme_color">Dimenciones</b> </a> </li>
                @else
                  <li> <a href="{{route('dimencion')}}"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Dimenciones</b> </a> </li>
                @endif              
                @if($_SESSION['active'] =='subdimenciones')
                    <li> <a href="{{route('subdimencion')}}"> <span>&nbsp;</span> <i class="fa fa-circle "></i> <b class="theme_color">Sub Dimencion</b> </a> </li>
                @else
                  <li> <a href="{{route('subdimencion')}}"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Sub Dimencion</b> </a> </li>
                @endif
                @if($_SESSION['active'] =='competencia')
                    <li> <a href="{{route('competencias')}}"> <span>&nbsp;</span> <i class="fa fa-circle "></i> <b class="theme_color">Competencias</b> </a> </li>
                @else
                  <li> <a href="{{route('competencias')}}"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Competencias</b> </a> </li>
                @endif
                @if($_SESSION['active'] =='subcompetencia')
                    <li> <a href="{{route('subcompetencias')}}"> <span>&nbsp;</span> <i class="fa fa-circle "></i> <b class="theme_color">Sub Competencias</b> </a> </li>
                @else
                  <li> <a href="{{route('subcompetencias')}}"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Sub Competencias</b> </a> </li>
                @endif
                @if($_SESSION['active'] =='materias')
                    <li> <a href="{{route('materias')}}"> <span>&nbsp;</span> <i class="fa fa-circle "></i> <b class="theme_color">Materias</b> </a> </li>
                @else
                  <li> <a href="{{route('materias')}}"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Materias</b> </a> </li>
                @endif
                  </ul>
                </li>
            @endif

            @if(Auth::User()->validar('4'))
                <!-- menu 5 -->
                @if($_SESSION['menu'] == 5)
                  <li class="left_nav_active theme_border"><a href="javascript:void(0);"><i class="fa fa-male"></i> Profesores <span class="left_nav_pointer"></span> <span class="plus"><i class="fa fa-plus"></i></span> </a>
                  <ul class="opened" style="display:block">
                @else
                  <li><a href="javascript:void(0);"><i class="fa fa-male"></i> Profesores <span class="left_nav_pointer"></span> <span class="plus"><i class="fa fa-plus"></i></span> </a>
                    <ul>
                @endif  
                @if($_SESSION['active'] =='profesor')
                    <li> <a href="{{route('profesores.buscar')}}"> <span>&nbsp;</span> <i class="fa fa-circle "></i> <b class="theme_color">Buscar Profesores</b> </a> </li>
                @else
                  <li> <a href="{{route('profesores.buscar')}}"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Buscar Profesores</b> </a> </li>
                @endif  
                @if($_SESSION['active'] =='Rprofesor')
                    <li> <a href="{{route('profesores.registrar')}}"> <span>&nbsp;</span> <i class="fa fa-circle "></i> <b class="theme_color">Registrar Profesores</b> </a> </li>
                @else
                  <li> <a href="{{route('profesores.registrar')}}"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Registrar Profesores</b> </a> </li>
                @endif 
                </ul>
              </li>
            @endif    
            @if(Auth::User()->validar('3'))
            <!-- menu 6 -->
            @if($_SESSION['menu'] == 6)
              <li class="left_nav_active theme_border"><a href="javascript:void(0);"><i class="glyphicon glyphicon-usd"></i> Pagos <span class="left_nav_pointer"></span> <span class="plus"><i class="fa fa-plus"></i></span> </a>
              <ul class="opened" style="display:block">
            @else
              <li><a href="javascript:void(0);"><i class="glyphicon glyphicon-usd"></i> Pagos <span class="left_nav_pointer"></span> <span class="plus"><i class="fa fa-plus"></i></span> </a>
                <ul>
            @endif  
            @if($_SESSION['active'] =='pagos')
                <li> <a href="{{ route('pagos.index')}}"> <span>&nbsp;</span> <i class="fa fa-circle "></i> <b class="theme_color">Administrar Pagos</b> </a> </li>
            @else
              <li> <a href="{{ route('pagos.index')}}"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Administrar Pagos</b> </a> </li>
            @endif   
                
              </ul>
            </li>
            @endif

            @if(Auth::User()->validar('4'))
            <!-- menu 7 -->
            @if($_SESSION['menu'] == 9)
              <li class="left_nav_active theme_border"><a href="javascript:void(0);"><i class="fa fa-file-text"></i> Reportes <span class="left_nav_pointer"></span> <span class="plus"><i class="fa fa-plus"></i></span> </a>
              <ul class="opened" style="display:block">
            @else
              <li><a href="javascript:void(0);"><i class="fa fa-file-text"></i> Reportes <span class="left_nav_pointer"></span> <span class="plus"><i class="fa fa-plus"></i></span> </a>
                <ul>
            @endif
            @if($_SESSION['active'] =='reporte_buscar')
                <li> <a href="{{ route('reportes.buscar') }}"> <span>&nbsp;</span> <i class="fa fa-circle "></i> <b class="theme_color">Buscar Boletin</b> </a> </li>
            @else
              <li> <a href="{{ route('reportes.buscar') }}"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Buscar boletin</b> </a> </li>
            @endif    
             @if($_SESSION['active'] =='reporte_descargar')
                <li> <a href="{{ route('reportes.descargar') }}"> <span>&nbsp;</span> <i class="fa fa-circle "></i> <b class="theme_color">Descargar Boletines</b> </a> </li>
            @else
              <li> <a href="{{ route('reportes.descargar') }}"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Descargar Boletines</b> </a> </li>
            @endif   
              </ul>
            </li>
            @endif            

            @if(Auth::User()->validar('4'))
            <!-- menu 7 -->
            @if($_SESSION['menu'] == 7)
              <li class="left_nav_active theme_border"><a href="javascript:void(0);"><i class="fa fa-th"></i> Configuracion <span class="left_nav_pointer"></span> <span class="plus"><i class="fa fa-plus"></i></span> </a>
              <ul class="opened" style="display:block">
            @else
              <li><a href="javascript:void(0);"><i class="fa fa-th"></i> Configuracion <span class="left_nav_pointer"></span> <span class="plus"><i class="fa fa-plus"></i></span> </a>
                <ul>
            @endif
            @if($_SESSION['active'] =='general')
                <li> <a href="{{ route('configuracion.index','index')}}"> <span>&nbsp;</span> <i class="fa fa-circle "></i> <b class="theme_color">Configuracion</b> </a> </li>
            @else
              <li> <a href="{{ route('configuracion.index','index')}}"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Configuracion</b> </a> </li>
            @endif
            @if(Auth::User()->validar('5'))
              @if($_SESSION['active'] =='historial')
                  <li> <a href="{{ route('configurar.historial') }}"> <span>&nbsp;</span> <i class="fa fa-circle "></i> <b class="theme_color">Configurar Historial</b> </a> </li>
              @else
                <li> <a href="{{ route('configurar.historial') }}"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Configurar Historial</b> </a> </li>
              @endif
            @endif 
            @if($_SESSION['active'] =='matricula')
                <li> <a href="{{ route('configurar.matricula') }}"> <span>&nbsp;</span> <i class="fa fa-circle "></i> <b class="theme_color">Configurar Matriculas</b> </a> </li>
            @else
              <li> <a href="{{ route('configurar.matricula') }}"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Configurar Matriculas</b> </a> </li>
            @endif  
            @if(Auth::User()->validar('5'))
              @if($_SESSION['active'] =='usuarios')
                  <li> <a href="{{ route('usuario') }}"> <span>&nbsp;</span> <i class="fa fa-circle "></i> <b class="theme_color">Configurar Usuarios</b> </a> </li>
              @else
                <li> <a <a href="{{ route('usuario') }}"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Configurar Usuarios</b> </a> </li>
              @endif  
            @endif
             </ul>
            </li>
            @endif
            <!-- menu 8 -->
            @if($_SESSION['menu'] == 8)
              <li class="left_nav_active theme_border visible-xs"><a href="javascript:void(0);"><i class="fa fa-th"></i>{{ Auth::User()->usuario }}<span class="left_nav_pointer"></span> <span class="plus"><i class="fa fa-plus"></i></span> </a>
              <ul class="opened" style="display:block">
            @else
              <li class="visible-xs"><a href="javascript:void(0);"><i class="fa fa-th"></i>{{ Auth::User()->usuario }}<span class="left_nav_pointer"></span> <span class="plus"><i class="fa fa-plus"></i></span> </a>
                <ul>
            @endif  
            @if($_SESSION['active'] =='perfil')
                <li> <a href="{{ route('usuario.perfil') }}"> <span>&nbsp;</span> <i class="fa fa-circle "></i> <b class="theme_color">Perfil</b> </a> </li>
            @else
              <li> <a href="{{ route('usuario.perfil') }}"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Perfil</b> </a> </li>
            @endif  
            @if($_SESSION['active'] =='ayuda')
                <li> <a href="datatable.html"> <span>&nbsp;</span> <i class="fa fa-circle "></i> <b class="theme_color">Ayuda</b> </a> </li>
            @else
              <li> <a href="datatable.html"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Ayuda</b> </a> </li>
            @endif 
            @if($_SESSION['active'] =='salir')
                <li> <a href="datatable.html"> <span>&nbsp;</span> <i class="fa fa-circle "></i> <b class="theme_color">Cerrar Sesion</b> </a> </li>
            @else
              <li> <a href="datatable.html"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Cerrar Sesion</b> </a> </li>
            @endif  
              
              </ul>
            </li>

          </ul>
        </div>
      </div>
      <!--\\\\\\\left_nav end \\\\\\-->
      <div class="contentpanel">
        <!--\\\\\\\ contentpanel start\\\\\\-->
        <div class="pull-left breadcrumb_admin clear_both">
          <div class="pull-left page_title theme_color">
            <h1><img src="{{ asset('images/logo.png') }}" style="height: 30px;"></h1>
            <h2 class="">@yield('subtitulo')</h2>
          </div>
         
        </div>
        <div class="container clear_both padding_fix">
          <!--\\\\\\\ container  start \\\\\\-->
          <div class="container" style="background-color: #FFF; padding-top: 10px;">
          @yield('body')
          @include('flash::message')
          @include('errors.request')
          </div>
        </div>
        <!--\\\\\\\ container  end \\\\\\-->
      </div>
      <!--\\\\\\\ content panel end \\\\\\-->
    </div>
    <!--\\\\\\\ inner end\\\\\\-->
  </div>
  <!--\\\\\\\ wrapper end\\\\\\-->

  <script src="{{ asset('js/jquery-2.1.0.js') }}"></script>
  <script src="{{ asset('js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('js/jquery.slimscroll.min.js') }}"></script>
  <script src="{{ asset('js/jPushMenu.js') }}"></script> 
  <script src="{{ asset('js/side-chats.js') }}"></script>
  <script src="{{ asset('js/common-script.js') }}"></script>
  @yield('script')
  </body>
</html>
