<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" type="text/css" href="{{ asset('css/login.css')}}">
  <title>Boletin</title>
<link rel="shortcut icon" href="{{ asset('images/logo.ico') }}">
</head>
<body>
    <div class="container">
  <div class="row">
    
<!-- Mixins-->
<!-- Pen Title-->
<div class="pen-title">
  <h1>Sistema De Boletines</h1>
</div>
<div class="container">
  <div class="card"></div>
  <div class="card">
    <h1 class="title">Login</h1>
     <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
    {{ csrf_field() }}
      <div class="input-container">
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        <input id="email" type="text" class="form-control" id="username" name="email" value="{{ old('email') }}" required>
        <label for="username" id="Username" class="control-label">Usuario</label>
        <div class="bar"></div>
        
    </div>
        
      </div>
      <div class="input-container">
        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <input id="Password" type="password" class="form-control" name="password" required>
             <label for="password" id="Password" class="control-label">Contraseña</label>
            
        <div class="bar"></div>
      </div>
      </div>
      <div class="button-container">
        <button type="submit"><span>Entrar</span></button>
      </div>
      <center>
       @if ($errors->has('email'))
            <span class="help-block" style="color: #ec2652">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
        @if ($errors->has('password'))
                <span class="help-block" style="color: #ec2652">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </center>
    </form>   
  </div>
</div>
  </div>
</div>
</body>
</html>

