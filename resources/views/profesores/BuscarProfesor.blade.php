<?php session_start();
 $_SESSION['menu']=5;
 $_SESSION['active']="profesor";  
 ?>

@extends('plantilla.estructura')

@section('title','inicio')
@section('pagina','boletin')
@section('subtitulo','Buscar Profesores')
@section('script')
	<script src="{{ asset('plugins/data-tables/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('plugins/data-tables/DT_bootstrap.js') }}"></script>
	<script src="{{ asset('plugins/data-tables/dynamic_table_init.js') }}"></script>
	<script src="{{ asset('plugins/edit-table/edit-table.js') }}"></script>
@endsection
@section('body')
	<div class="row">
    
  <div class="col-xs-12 col-sm-10" style="margin-bottom: 10px;">
    <div class="dataTables_filter" id="dynamic-table_fil">
      <form method="GET" action="{{ route('profesores.buscar')}}" class="form-inline">
         <div class="form-group">
          <label style="" class="control-label" style="margin-bottom: 0;vertical-align: middle;">Buscar</label>
          <input class="form-control" name="listar" placeholder="Buscar" style="display: inline-block;width: auto;vertical-align: middle;" type="text">            
         <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
         </div>
      </form>
      </div>
    </div>
  </div>
	<div class="table-responsive">
                <table  class="display table table-bordered table-striped" >
                  <thead>
                    <tr>                      
                      <th>Nombre</th>
                      <th style="width: 14%" >Tipo Documento</th>
                      <th style="width: 14%">Documento</th>
                      <th>Correo</th>                      
                      <th class="hidden-phone">Opciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($profesor as $profesores)
                        <tr class="gradeX">
                          <td>{{ $profesores->nombre." ".$profesores->apellido }}</td>
                          <td>
                          @if($profesores->tipo_documento==1)
                          C.C
                          @elseif($profesores->tipo_documento==2)
                          T.I
                          @elseif($profesores->tipo_documento==3)
                          R.C
                          @endif
                          </td>
                          <td>{{ $profesores->documento }}</td>
                          <td>{{ $profesores->correo }}</td>
                          <td>
                            <center>
                              <a href="{{ route('profesores.editar',$profesores->id) }}" class="btn btn-success" title="Editar Estudiante"><span class="glyphicon glyphicon-edit"></span></a>
                              <a href="{{ route('profesores.eliminar',$profesores->id) }}" onclick="return confirm('Estas seguro que quieres eliminar a este usuario?');" class="btn btn-danger" title="Eliminar Estudiante"><span class="glyphicon glyphicon-trash"></span></a>
                            </center>
                          </td>
                        </tr>
                    @endforeach
                  </tbody>
                  
                </table>
                {{ $profesor->links() }}
              </div><!--/table-responsive-->
	
@endsection