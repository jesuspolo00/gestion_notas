<?php session_start();
 $_SESSION['menu']=5;
 $_SESSION['active']="profesor";  
 ?>

@extends('plantilla.estructura')

@section('title','inicio')
@section('pagina','boletin')
@section('subtitulo','Editar Profesor')
@section('body')
	<div class="row">
		<form class="form-horizontal" action="{{ route('profesores.actualizar',$profesor->id)}}" method="POST">
		{{ csrf_field() }}
		{{ method_field('PUT') }}
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Nombre</label>
		    <div class="col-sm-8 col-lg-5">
		      <input type="text" class="form-control" name="nombre" placeholder="Nombre" value="{{ $profesor->nombre }}"required>
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Apellido</label>
		    <div class="col-sm-8 col-lg-5">
		      <input type="text" class="form-control" name="apellido" value="{{ $profesor->apellido }}" placeholder="Apellido" required>
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Tipo Documento</label>
		    <div class="col-sm-8 col-lg-5">
		      	<select class="form-control" name="tdocumento" required>
		      	@if($profesor->tipo_documento == 1)
		      		<option value="1" selected>C.C</option>
		      	@elseif($profesor->tipo_documento == 2)
		      		<option value="2" selected>T.I</option>
		      	@elseif($profesor->tipo_documento == 3)
		      		<option value="3" selected>R.C</option>
		      	@endif
		      	  <option value="2">T.I</option>
				  <option value="1">C.C</option>				  
				  <option value="3">R.C</option>
				</select>
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Documento</label>
		    <div class="col-sm-8 col-lg-5">
		      <input type="number" class="form-control" name="documento" value="{{ $profesor->documento }}" placeholder="Documento" required>
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Correo</label>
		    <div class="col-sm-8 col-lg-5">
		      <input type="email" class="form-control" name="correo" value="{{ $profesor->correo }}" placeholder="Correo" required>
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Direccion</label>
		    <div class="col-sm-8 col-lg-5">
		      <input type="text" class="form-control" name="direccion" value="{{ $profesor->direccion }}" placeholder="Direccion" required>
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Telefono</label>
		    <div class="col-sm-8 col-lg-5">
		      <input type="number" class="form-control" name="telefono" value="{{ $profesor->telefono }}" placeholder="Telefono" required>
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Genero</label>
		    <div class="col-sm-8 col-lg-5">
		      <select class="form-control" name="genero" required>
		      		@if($profesor->genero == 1)
		      		<option value="1" selected="">Masculino</option>
		      		@elseif($profesor->genero == 2)
		      		<option value="2" selected="">Femenino</option>
		      		@endif
				  <option value="1">Masculino</option>
				  <option value="2">Femenino</option>				  
				</select> 
		    </div>
		  </div>
		   <div class="form-group">
		    <label class="col-sm-3 control-label">Curso</label>
		    <div class="col-sm-8 col-lg-5">
		      	<select class="form-control" name="curso" required="" id="curso">
				  @foreach($salon as $salones)
				  		@if($curso[0]->id==$salones->id)
				  			<option value="{{ $salones->id }}" selected="">{{ $salones->nombre }}</option>
				  		@else
				  			<option value="{{ $salones->id }}">{{ $salones->nombre }}</option>
				  		@endif	  
					@endforeach
				</select>
		    </div>
		  </div>
		  @foreach($salon as $cur)
		  <input value="{{ $cur->id_categoria }}" type="hidden" id="d{{ $cur->id }}" class="form-control hidden" disabled="">
		  @endforeach
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Dimenciones</label>
		    <div class="col-sm-8 col-lg-5">
		      	<select class="form-control" name="dimencion[]" id="dimencion" multiple required="">
				</select>
		    </div>
		  </div>
		  <div class="form-group">
		    <div class="col-sm-offset-3 col-sm-9 col-lg-6">
		      <button type="submit" class="btn btn-default">Guardar</button>
		    </div>
		  </div>
		</form>
	</div>
@endsection
@section('script')
	<script type="text/javascript">
		$( document ).ready(function() {
		    console.log( "ready!" );
		});
		$("#curso").change(function () {
		    var id = "";
		    $( "#curso option:selected" ).each(function() {
		      id = $(this).val();
		    });
		    if(id != ""){
		    var id_categoria=$('#d'+id).val();
			    if(id_categoria!=""){

			    
			     $.ajaxSetup({
			          headers: {
			          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			          }
			       });

			      $.ajax({
			          url: '../<?php echo $profesor->id;?>/ajaxeditar/'+id_categoria,
			          type: 'GET',                           
			          success: function (data) {
			             $('#dimencion').html(data);
			          	 $("#dimencion").removeAttr('disabled');
			          }            
			      });
			  }else{
			  	$('#d'+id).val('');
		    	$('#dimencion').html("");
			  }
		    }else{
		    	$('#d'+id).val('');
		    	$('#dimencion').html("");
		    }
		  })
		.change();
	</script>
@endsection