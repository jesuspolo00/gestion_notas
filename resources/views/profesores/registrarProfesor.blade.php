<?php session_start();
 $_SESSION['menu']=5;
 $_SESSION['active']="Rprofesor";  
 ?>

@extends('plantilla.estructura')

@section('title','inicio')
@section('pagina','boletin')
@section('subtitulo','Registrar Profesor')
@section('body')
	<div class="row">
		<form class="form-horizontal" action="{{ route('profesores.store') }}" method="POST">
		{{ csrf_field() }}
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Nombre</label>
		    <div class="col-sm-8 col-lg-5">
		      <input type="text" class="form-control" name="nombre" placeholder="Nombre" required="">
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Apellido</label>
		    <div class="col-sm-8 col-lg-5">
		      <input type="text" class="form-control" name="apellido" placeholder="Apellido" required="">
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Tipo Documento</label>
		    <div class="col-sm-8 col-lg-5">
		      	<select class="form-control" name="tdocumento" required>
		      	  <option value="2" selected>T.I</option>
				  <option value="1">C.C</option>				  
				  <option value="3">R.C</option>
				</select>
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Documento</label>
		    <div class="col-sm-8 col-lg-5">
		      <input type="number" class="form-control" name="documento" placeholder="Documento" required="">
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Correo</label>
		    <div class="col-sm-8 col-lg-5">
		      <input type="email" class="form-control" name="correo" placeholder="Correo" required="">
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Direccion</label>
		    <div class="col-sm-8 col-lg-5">
		      <input type="text" class="form-control" name="direccion" placeholder="Direccion" required="">
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Telefono</label>
		    <div class="col-sm-8 col-lg-5">
		      <input type="number" class="form-control" name="telefono" placeholder="Telefono" required="">
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Genero</label>
		    <div class="col-sm-8 col-lg-5">
		     <select class="form-control" name="genero" required>
				  <option value="1">Masculino</option>
				  <option value="2">Femenino</option>				  
				</select>
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Curso</label>
		    <div class="col-sm-8 col-lg-5">
		      	<select class="form-control" name="curso" required="" id="curso">
		      		<option value="">Seleccione una opcion..</option>
				  @foreach($curso as $cursos)
				  <option value="{{ $cursos->id }}">{{ $cursos->nombre }}</option>			  
					@endforeach
				</select>
		    </div>
		  </div>		  
		  @foreach($curso as $cur)
		  <input value="{{ $cur->id_categoria }}" type="hidden" id="d{{ $cur->id }}" class="form-control hidden" disabled="">
		  @endforeach
		  <div class="form-group">
		    <label class="col-sm-3 control-label">Dimenciones</label>
		    <div class="col-sm-8 col-lg-5">
		      	<select class="form-control" name="dimencion[]" id="dimencion" multiple required="">
				</select>
		    </div>
		  </div>
		  
		  <div class="form-group">
		    <div class="col-sm-offset-3 col-sm-9 col-lg-6">
		      <button type="submit" class="btn btn-default">Registrar</button>
		    </div>
		  </div>
		</form>
	</div>
@endsection
@section('script')
	<script type="text/javascript">
		$("#curso").change(function () {
		    var id = "";
		    $( "#curso option:selected" ).each(function() {
		      id = $(this).val();
		    });
		    if(id != ""){
		    var id_categoria=$('#d'+id).val();
			    if(id_categoria!=""){

			    
			     $.ajaxSetup({
			          headers: {
			          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			          }
			       });

			      $.ajax({
			          url: 'ajax/'+id_categoria,
			          type: 'GET',                           
			          success: function (data) {
			             $('#dimencion').html(data);
			          	 $("#dimencion").removeAttr('disabled');
			          }            
			      });
			  }else{
			  	$('#d'+id).val('');
		    	$('#dimencion').html("");
			  }
		    }else{
		    	$('#d'+id).val('');
		    	$('#dimencion').html("");
		    }
		  })
	</script>
@endsection