<?php session_start();
 $_SESSION['menu']=7;
 $_SESSION['active']="historial";  
 ?>

@extends('plantilla.estructura')
@section('head')
@endsection
@section('title','inicio')
@section('pagina','boletin')
@section('subtitulo','Historial de boletines')
@section('script')
	<script src="{{ asset('plugins/data-tables/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('plugins/data-tables/DT_bootstrap.js') }}"></script>
	<script src="{{ asset('plugins/data-tables/dynamic_table_init.js') }}"></script>
	<script src="{{ asset('plugins/edit-table/edit-table.js') }}"></script>
  
@endsection
@section('body')
      <div class="container">
          <a href="{{ route('configurar.historial') }}" style="float: right;" class="btn btn-info" title="Editar Usuario">Historial</a>
     </div>
     <br>
	<div class="table-responsive">
      <table  class="display table table-bordered table-striped table-hover" >
        <thead>
          <tr>                      
            <th>Responsable</th>
            <th style="width: 14%">Movimiento</th>
            <th style="width: 14%">Curso</th>
            <th>Periodo</th>                      
            <th>Año</th>
            <th>Estudiante</th>
          </tr>
        </thead>
        <tbody>
          @foreach($historial as $historia)
              <tr class="gradeX">
                <td>{{ $historia->usuario }}</td>
                <td>{{ $historia->movimiento }}</td>
                <td>{{ $historia->curso }}</td>
                <td>{{ $historia->periodo }}</td>
                <td>{{ $historia->año }}</td>
                <td>{{ $historia->estudiante }}</td>
              </tr>
          @endforeach
          
        </tbody>
      </table>
        {{ $historial->links() }}
    </div><!--/table-responsive-->
	
@endsection