<?php session_start();
 $_SESSION['menu']=7;
 $_SESSION['active']="historial";  
 ?>

@extends('plantilla.estructura')
@section('head')
@endsection
@section('title','inicio')
@section('pagina','boletin')
@section('subtitulo','Historial')
@section('script')
	<script src="{{ asset('plugins/data-tables/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('plugins/data-tables/DT_bootstrap.js') }}"></script>
	<script src="{{ asset('plugins/data-tables/dynamic_table_init.js') }}"></script>
	<script src="{{ asset('plugins/edit-table/edit-table.js') }}"></script>
  
@endsection
@section('body')
  
	<div class="row">    
  <div class="col-xs-12" style="margin-bottom: 10px;">
    <div class="col-xs-9">
      <div class="dataTables_filter" id="dynamic-table_fil">
        <form method="GET" action="{{ route('configurar.historial') }}" class="form-inline">
         <div class="form-group">
          <input class="form-control" name="buscar" style="display: inline-block;width: auto;vertical-align: middle;" placeholder="Buscar" type="text">
          <select class="form-control" name="movimiento">
              <option value="">Seleccione una opcion..</option>
              @foreach($movimiento as $movimientos)
              	<option>{{ $movimientos->movimiento }}</option>
              @endforeach
          </select>
          <select class="form-control" name="lugar">
              <option value="">Seleccione una opcion..</option>
              @foreach($lugar as $lugares)
              	<option>{{ $lugares->lugar }}</option>
              @endforeach
          </select>
         <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
         </div>
      </form>
        </div>
      </div>
      <div class="col-xs-3">
          <a href="{{ route('configurar.historial.boletin') }}" style="float: right;" class="btn btn-info" title="Editar Usuario">Historial de boletines</a>
      </div>
    </div>
  </div>

	<div class="table-responsive">
      <table  class="display table table-bordered table-striped table-hover" >
        <thead>
          <tr>                      
            <th>Responsable</th>
            <th style="width: 14%" >Movimiento</th>
            <th style="width: 14%">Descripcion</th>
            <th>Observacion</th>                      
            <th>Afectado</th>
          </tr>
        </thead>
        <tbody>
          @foreach($historial as $historia)
              <tr class="gradeX">
                <td>{{ $historia->usuario }}</td>
                <td>{{ $historia->movimiento }}</td>
                <td>{{ $historia->lugar }}</td>
                <td>{{ $historia->observacion }}</td>
                <td>{{ $historia->afectado }}</td>
              </tr>
          @endforeach
          
        </tbody>
      </table>
        {{ $historial->links() }}
    </div><!--/table-responsive-->
	
@endsection