<?php session_start();
 $_SESSION['menu']=6;
 $_SESSION['active']="pagos";  
 ?>

@extends('plantilla.estructura')

@section('title','inicio')
@section('pagina','boletin')
@section('subtitulo','ADMINISTRAR PAGOS')
@section('script')
	<script src="{{ asset('plugins/data-tables/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('plugins/data-tables/DT_bootstrap.js') }}"></script>
	<script src="{{ asset('plugins/data-tables/dynamic_table_init.js') }}"></script>
	<script src="{{ asset('plugins/edit-table/edit-table.js') }}"></script>
@endsection
@section('body')
	<div class="row">
    
  <div class="col-xs-12 col-sm-10" style="margin-bottom: 10px;">
    <div class="dataTables_filter" id="dynamic-table_fil">
      <form method="GET" action="{{ route('pagos.index') }}" class="form-inline">
         <div class="form-group">
          <label style="" class="control-label" style="margin-bottom: 0;vertical-align: middle;">Buscar</label>
          <select class="form-control" name="buscar">
              @foreach($curso as $cursos)
                <option value="{{ $cursos->id }}">{{ $cursos->nombre }}</option>
              @endforeach
          </select>           
         <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
         </div>
      </form>
      </div>
    </div>
  </div>
	<div class="table-responsive">
      <table  class="display table table-bordered table-striped" >
        <thead>
          <tr>                      
            <th>Nombre</th>
            <th style="width: 14%" >Tipo Documento</th>
            <th style="width: 14%">Documento</th>
            <th>Genero</th>                      
            <th>Opciones</th>
          </tr>
        </thead>
        <tbody>
          @foreach($estudiante as $estudiantes)
              <tr class="gradeX">
                <td>{{ $estudiantes->nombre." ".$estudiantes->apellido }}</td>
                <td>
                @if($estudiantes->tipo_documento==1)
                C.C
                @elseif($estudiantes->tipo_documento==2)
                T.I
                @elseif($estudiantes->tipo_documento==3)
                R.C
                @endif
                </td>
                <td>{{ $estudiantes->documento }}</td>
                <td>
                  @if($estudiantes->genero==1)
                  Masculino
                  @elseif($estudiantes->genero==2)
                  Femenino
                  @endif
                </td>
                <td>
                  <center>
                    <a href="{{ route('pagos.guardar',$estudiantes->id) }}" class="btn btn-info" title="Ver Pagos"><span class="glyphicon glyphicon-eye-open"></span></a> 
                  </center>
                </td>
              </tr>
          @endforeach
          
        </tbody>
        
      </table>
    </div><!--/table-responsive-->
	
@endsection