<?php session_start();
 $_SESSION['menu']=6;
 $_SESSION['active']="pagos";  
 $meses = ['enero','febrero','marzo','abril','mayo','junio','julio','agosto','septiembre','octubre','noviembre','diciembre'];
 //$pago->all();
 ?>

@extends('plantilla.estructura')

@section('title','Pagos')
@section('pagina','boletin')
@section('subtitulo','Pagos del estudiante')
@section('body')
  <form method="POST" action="{{ route('pagos.store',$estudiante->id) }}">
  {{ csrf_field() }}
    <div class="row">
        <div class="col-xs-12">
          <center><h3>Pagos</h3></center>
          <h5>{{ $estudiante->nombre }} {{ $estudiante->apellido }}</h5>
          <div class="col-sm-4 col-sm-offset-3">
            <?php $conteo=0; ?>
            @foreach($meses as $mes)
                <?php $verificar=0; ?>
                @if($conteo==6)
                  </div>
                  <div class="col-sm-3">
                @endif
                @foreach($pago as $pagos)
                  @if($pagos->mes==$mes)
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" name="{{ $mes }}" checked="">
                        {{ $mes }}</label>
                      </div>
                      <?php $verificar=1; ?>
                  @endif
                @endforeach
                @if($verificar==0)
                  <div class="checkbox">
                      <label>
                        <input type="checkbox" name="{{ $mes }}">
                      {{ $mes }}</label>
                    </div>
                @endif
                <?php $conteo++; ?>
            @endforeach
             
          
              
          </div>
        </div>
        <center>
        <div class="form-group">
          <button type="submit" class="btn btn-default">Guardar</button>
          <a href="{{ route('pagos.index') }}" class="btn btn-default">Volver</a>
        </div>
        </center>
    </div>
  </form>
	
@endsection
