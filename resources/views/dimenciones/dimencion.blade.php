<?php session_start();
 $_SESSION['menu']=4;
 $_SESSION['active']="dimenciones";  
 ?>

@extends('plantilla.estructura')
@section('title','Dimenciones')
@section('pagina','boletin')
@section('subtitulo','Agregar Dimenciones')
@section('body')

	<div class="row">
		<div class="container">
			<div class="col-xs-12">
	    			@foreach($dimencion as $dimenciones)
	    			<form class="form-horizontal" method="POST">
	    			{{ csrf_field() }}
			    		<div class="form-group">
						    <label class="col-sm-2 control-label">Dimencion</label>
						    <div class="col-sm-5">
						      <input type="text" class="form-control" name="nombre" id="input{{$dimenciones->id}}" value="{{ $dimenciones->nombre }}" required="" disabled>
						    </div>
						    <div class="col-sm-2">
						    	<select class="form-control" name="nivel" disabled="" id="nivel{{$dimenciones->id}}">
						    		@foreach($curso as $cursos)
						    			@if($dimenciones->nivel == $cursos->id)
						    				<option value="{{ $dimenciones->nivel }}" selected="">{{ $cursos->nombre }}</option>
						    			@else
						    				<option value="{{ $cursos->id }}">{{ $cursos->nombre }}</option>
						    			@endif
						    		@endforeach
						    	</select>
						    </div>
						    <div class="col-xs-3">
							    <a href="{{ route('dimencion.eliminar',$dimenciones->id)}}" class="btn btn-default"><span class="glyphicon glyphicon-trash"></span></a>
						    </div>
						 </div>
					</form>
					 @endforeach				
				
				<form class="form-horizontal" action="{{ route('dimencion.agregar') }}" method="POST">
				{{ csrf_field() }}	
				  <div id="contenedormateria" class="col-xs-12">
					  <div id="dimencion1" class="form-group">
					    <label class="col-sm-2 control-label">Dimencion 1</label>
					    <div class="col-sm-5">
					      <input type="text" class="form-control" required="" name="dimencion[]" placeholder="Ingrese Una Dimencion">
					    </div>
					    <div class="col-sm-2">
					      <select id="ni" class="form-control" name="nivel[]">
					      		@foreach($curso as $cursos)
					      			<option value="{{$cursos->id}}">{{$cursos->nombre}}</option>
					      		@endforeach
					      </select>
					    </div>
					    <div id="botones1" class="col-xs-3">
					    <button type="button" class="btn btn-default" onclick="quitarDimencion();">-</button>
					    <button type="button" class="btn btn-default" onclick="agregarDimencion();">+</button>
					    </div>
					  </div>
				  </div>
				  <div class="form-group">
				    <div class="col-sm-offset-3 col-xs-offset-1 col-sm-9 col-lg-6">
				      <button type="submit" class="btn btn-default">Guardar</button>
				    </div>
				  </div>
				</form>
			
			</div>
		</div>
	</div>

@endsection
@section('script')
<script type="text/javascript">	
	var numero=1;
	function agregarDimencion(){
		$( "#botones1" ).remove();
		var ni =$("#ni").html();
		numero++;
		$('#contenedormateria').append('<div id="dimencion'+numero+'" class="form-group">'
					    +'<label class="col-sm-2 control-label">Dimencion '+numero+'</label>'
					    +'<div class="col-sm-5">'
					    +'<input type="text" class="form-control" name="dimencion[]" placeholder="Ingrese Una Dimencion">'
					    +'</div>'
					    +'<div class="col-sm-2">'
					      +'<select id="ni" class="form-control" name="nivel[]">'+ni
					      +'</select>'
					    +'</div>'
					    +'<div id="botones1" class="col-xs-3">'
					    +'<button type="button" class="btn btn-default" onclick="quitarDimencion();">-</button>'
					    +'<button type="button" class="btn btn-default" onclick="agregarDimencion();">+</button>'
					    +'</div>'
					 +'</div>');
	}
	function quitarDimencion(){
		if(numero<=1){

		}else{			
		$("#dimencion"+numero).remove();	
		numero--;		
		$("#dimencion"+numero).append('<div id="botones1" class="col-xs-3">'
					    +'<button type="button" class="btn btn-default" onclick="quitarDimencion();">-</button>'
					    +'<button type="button" class="btn btn-default" onclick="agregarDimencion();">+</button>'
					    +'</div>');
		}
		
	}
	function editar(id){		
		$("#nivel"+id).removeAttr('disabled'); 
		$("#input"+id).removeAttr('disabled'); 
		$("#edit"+id).addClass('hidden');
		$("#editado"+id).removeClass('hidden');
	}
</script>
@endsection