<?php session_start();
 $_SESSION['menu']=4;
 $_SESSION['active']="competencia";  
 ?>

@extends('plantilla.estructura')
@section('title','Dimenciones')
@section('pagina','boletin')
@section('subtitulo','Agregar Competencias')
@section('body')

  <div class="row">
    <div class="container">
      <div class="col-xs-12" style="margin-bottom: 20px;">
        <form action="{{ route('competencias')}}" method="GET" class="form-inline">
          <select class="form-control" id="nivel" required="">
            <option value="">Seleccione Una Opcion...</option>
            @foreach($curso as $cursos)
              @foreach($dimencion as $dimenciones)
                @if($dimenciones->nivel == $cursos->id)
                  <option value="{{ $dimenciones->nivel }}">{{ $cursos->nombre }}</option>
                @endif
              @endforeach
            @endforeach
          </select>
          <select class="form-control" id="sel" name="buscar" required="">
              <option value="">Seleccione Una Opcion...</option>
          </select>
          <select class="form-control" name="subdimencion" id="subdimencion" required="">
            <option value="">Seleccione Una Opcion...</option>
          </select>
          <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
        </form>
      </div>
      
      <div class="col-xs-12">
            @foreach($competencia as $dimenciones)
            <form class="form-horizontal" method="POST">
            {{ csrf_field() }}
              <div class="form-group">
                <label class="col-sm-3 control-label">Competencias</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" name="nombre" id="input{{$dimenciones->id}}" value="{{ $dimenciones->nombre }}" disabled required="">
                </div>
                <div class="col-xs-3">
                  <a href="{{ route('competencias.eliminar',$dimenciones->id)}}" class="btn btn-default"><span class="glyphicon glyphicon-trash"></span></a>
                </div>
             </div>
          </form>
           @endforeach    
      @if($verificar != "")
        <form class="form-horizontal" action="{{ route('competencias.agregar',$verificar->id)}}" method="POST">
        {{ csrf_field() }}  
          <div id="contenedormateria" class="col-xs-12">
            <div id="materia1" class="form-group">
              <label class="col-sm-3 control-label">Competencias 1</label>
              <div class="col-sm-6">
                <input type="text" class="form-control" name="competencia[]" required="" placeholder="Ingrese Una Competencia">
              </div>
              <div id="botones1" class="col-xs-3">
              <button type="button" class="btn btn-default" onclick="quitarCompetencia();">-</button>
              <button type="button" class="btn btn-default" onclick="agregarCompetencia();">+</button>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-3 col-xs-offset-1 col-sm-9 col-lg-6">
              <button type="submit" class="btn btn-default">Guardar</button>
            </div>
          </div>
        </form>
      @endif
      </div>
      
    </div>
  </div>

@endsection
@section('script')
<script type="text/javascript"> 
  var numero=1;
  function agregarCompetencia(){
    $( "#botones1" ).remove();
    numero++;
    $('#contenedormateria').append('<div id="materia'+numero+'" class="form-group">'
              +'<label class="col-sm-3 control-label">Competencias '+numero+'</label>'
              +'<div class="col-sm-6">'
              +'<input type="text" class="form-control" name="competencia[]" placeholder="Ingrese Una Competencia">'
              +'</div>'
              +'<div id="botones1" class="col-xs-3">'
              +'<button type="button" class="btn btn-default" onclick="quitarCompetencia();">-</button>'
              +'<button type="button" class="btn btn-default" onclick="agregarCompetencia();">+</button>'
              +'</div>'
           +'</div>');
  }
  function quitarCompetencia(){
    if(numero<=1){

    }else{      
    $("#materia"+numero).remove();  
    numero--;   
    $("#materia"+numero).append('<div id="botones1" class="col-xs-3">'
              +'<button type="button" class="btn btn-default" onclick="quitarCompetencia();">-</button>'
              +'<button type="button" class="btn btn-default" onclick="agregarCompetencia();">+</button>'
              +'</div>');
    }
    
  }
  function editar(id){    
    $("#input"+id).removeAttr('disabled'); 
    $("#edit"+id).addClass('hidden');
    $("#editado"+id).removeClass('hidden');
  }
  $( "#nivel" ).change(function () {
    var id = "";
    $( "#nivel option:selected" ).each(function() {
      id = $( this ).val();
    });
    if(id != ""){
     $.ajaxSetup({
          headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
       });

      $.ajax({
          url: 'subdimencion/listar/'+id,
          type: 'GET',                           
          success: function (data) {
             $('#sel').html(data);
          }            
      });
    }else{
        $('#sel').html('<option value="">Seleccione Una Opcion...</option>');
    }
  })
  $( "#sel" ).change(function () {
    var id = "";
    $( "#sel option:selected" ).each(function() {
      id = $( this ).val();
    });
    if(id != ""){
     $.ajaxSetup({
          headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
       });

      $.ajax({
          url: 'competencias/listar/'+id,
          type: 'GET',                           
          success: function (data) {
             $('#subdimencion').html(data);
          }            
      });
    }else{
        $('#subdimencion').html('<option value="">Seleccione Una Opcion...</option>');
    }
  })
         
 
</script>
@endsection