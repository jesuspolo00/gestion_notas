@if($dimencion->IsNotEmpty())
<option value="">Seleccione Una Opcion...</option>
@foreach($dimencion as $dimen)
  <option value="{{ $dimen->id }}">{{ $dimen->nombre }}</option>
@endforeach
@else
	<option value="">Seleccione Una Opcion...</option>
@endif