@if($subdimencion->IsNotEmpty())
<option value="">Seleccione Una Opcion...</option>
@foreach($subdimencion as $dimen)
  <option value="{{ $dimen->id }}">{{ $dimen->nombre }}</option>
@endforeach
@else
	<option value="">Seleccione Una Opcion...</option>
@endif