@if($subcompetencias->IsNotEmpty())
<option value="">Seleccione Una Opcion...</option>
@foreach($subcompetencias as $comp)
  <option value="{{ $comp->id }}">{{ $comp->nombre }}</option>
@endforeach
@else
	<option value="">Seleccione Una Opcion...</option>
@endif